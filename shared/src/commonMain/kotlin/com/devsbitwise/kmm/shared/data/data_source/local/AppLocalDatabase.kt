/*
 * Copyright (c) 2023 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.kmm.shared.data.data_source.local


import androidx.room.ConstructedBy
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.RoomDatabaseConstructor
import androidx.room.TypeConverters
import com.devsbitwise.messari.data.data_source.local.MessariTypeConverter
import com.devsbitwise.messari.data.data_source.local.dao.assets.MessariAssetDao
import com.devsbitwise.messari.data.data_source.local.dao.exchanges.MessariExchangeDao
import com.devsbitwise.messari.data.data_source.local.dao.marketcap.MessariMarketCapDao
import com.devsbitwise.messari.data.data_source.local.dao.price.MessariPriceDao
import com.devsbitwise.messari.data.data_source.local.entities.assets.MessariAssetDataEntity
import com.devsbitwise.messari.data.data_source.local.entities.assets.MessariAssetMinDataEntity
import com.devsbitwise.messari.data.data_source.local.entities.exchanges.MessariExchangeDataEntity
import com.devsbitwise.messari.data.data_source.local.entities.marketcap.MessariMarketCapDataEntity
import com.devsbitwise.messari.data.data_source.local.entities.price.MessariPriceDataEntity
import com.devsbitwise.wordpress.data.data_source.local.dao.posts.WordPressPostDao
import com.devsbitwise.wordpress.data.data_source.local.entities.posts.WordPressPostDataEntity
import com.devsbitwise.youtube.data.data_source.local.YouTubeTypeConverter
import com.devsbitwise.youtube.data.data_source.local.dao.videos.YouTubeVideoDao
import com.devsbitwise.youtube.data.data_source.local.entities.videos.YouTubeVideoDataEntity


// The Room compiler generates the `actual` implementations
@Suppress("NO_ACTUAL_FOR_EXPECT", "EXPECT_ACTUAL_CLASSIFIERS_ARE_IN_BETA_WARNING")
expect object AppDatabaseConstructor : RoomDatabaseConstructor<AppLocalDatabase>

@Database(
    entities = [
        MessariAssetDataEntity::class,
        MessariAssetMinDataEntity::class,
        MessariExchangeDataEntity::class,
        MessariPriceDataEntity::class,
        MessariMarketCapDataEntity::class,
        WordPressPostDataEntity::class,
        YouTubeVideoDataEntity::class
    ],
    version = 10,
    exportSchema = false
)
@ConstructedBy(AppDatabaseConstructor::class)
@TypeConverters(MessariTypeConverter::class, YouTubeTypeConverter::class)
abstract class AppLocalDatabase : RoomDatabase() {

    abstract val messariAssetDao: MessariAssetDao

    abstract val messariExchangeDao: MessariExchangeDao

    abstract val messariPriceDao: MessariPriceDao

    abstract val messariMarketCapDao: MessariMarketCapDao

    abstract val wordPressPostDao: WordPressPostDao

    abstract val youTubeVideoDao: YouTubeVideoDao

}