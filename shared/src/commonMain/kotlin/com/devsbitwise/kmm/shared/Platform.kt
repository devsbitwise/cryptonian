package com.devsbitwise.kmm.shared

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform