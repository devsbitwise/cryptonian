enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
pluginManagement {
    repositories {
        google {
            mavenContent {
                includeGroupAndSubgroups("androidx")
                includeGroupAndSubgroups("com.android")
                includeGroupAndSubgroups("com.google")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google {
            mavenContent {
                includeGroupAndSubgroups("androidx")
                includeGroupAndSubgroups("com.android")
                includeGroupAndSubgroups("com.google")
            }
        }
        mavenCentral()
        maven {
            url = uri("https://jitpack.io")
        }
    }
    versionCatalogs {
        create("material") {
            from(files("gradle/material.versions.toml"))
        }
        create("admob") {
            from(files("gradle/admob.versions.toml"))
        }
        create("firebase") {
            from(files("gradle/firebase.versions.toml"))
        }
        create("gms") {
            from(files("gradle/gms.versions.toml"))
        }
    }
}

rootProject.name = "Cryptonian"

// Android modules
include(":androidApp")
include(":AndroidCommon")
include(":AndroidMaterial")
include(":AndroidFirebase")
include(":AndroidAdMob")
include(":HtmlTextView")

// KMM/KMP modules
include(":shared")
include(":Kommon")
include(":Messari")
include(":YouTube")
include(":WordPress")