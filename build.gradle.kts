/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import com.android.build.gradle.BaseExtension

// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    alias(libs.plugins.android.application).apply(false)
    alias(libs.plugins.android.kotlin).apply(false)
    alias(libs.plugins.kotlin.multiplatform).apply(false)
    alias(libs.plugins.kotlin.serialization).apply(false)
    alias(libs.plugins.ksp).apply(false)
    alias(gms.plugins.google.services).apply(false)
    alias(firebase.plugins.crashlytics).apply(false)
    alias(libs.plugins.test.aggregation.results)
    alias(libs.plugins.test.aggregation.coverage)
}

apply(plugin = "android-reporting")

// Since changing the report directory of instrumented tests result using android-reporting plugin seems impossible at this point,
// we will instead change the reporting directory of unit tests and code coverage to align and standardize the plugins' reporting
tasks {
    testAggregatedReport {
        destinationDirectory.set(file("$rootDir/build/unitTest-results"))
    }
    jacocoAggregatedReport {
        reporting.baseDir = layout.projectDirectory.dir("$rootDir/build/codeCoverage-results").asFile
    }
    testAggregation {
        coverage {
            exclude(
                "**/databinding/**", // View Binding generated class
                "**/*Dao_Impl*", // Room DAO generated class
                "**/data/data_source/remote/services/**", // HTTP client classes
                "com/devsbitwise/cryptonian/data/data_source/components/FCMService.*", // Firebase Cloud Messaging (FCM)
                "com/devsbitwise/cryptonian/data/data_source/remote/responses/notifications/**", // DTOs for Firebase Cloud Messaging (FCM)
                "com/devsbitwise/android/admob/**",
                "com/devsbitwise/android/material/**",
                "com/devsbitwise/android/firebase/**",
                "org/sufficientlysecure/htmltextview/**"
            )
        }
    }
}

allprojects {
    plugins.withId("com.android.base") {
        the<BaseExtension>().buildTypes {
            configureEach { aggregateTestCoverage = isDebuggable } // Only debuggable variant when executing and aggregating unit tests result to avoid duplicate report
        }
    }
}

// Overwrite the default Gradle clean task
tasks.register<Delete>("clean") {
    description = "Clean root directory build folder"
    group = "Android Gradle Task"
    delete(rootProject.layout.buildDirectory)
}