/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import com.google.firebase.crashlytics.buildtools.gradle.CrashlyticsExtension

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.android.kotlin) // This should be put before any Kotlin dependent plugin
    alias(libs.plugins.kotlin.serialization)
    alias(gms.plugins.google.services) // This should be put before any Google Play services dependent plugin like Firebase
    alias(firebase.plugins.crashlytics)
    alias(libs.plugins.play.publisher)
}

android {
    namespace = "com.devsbitwise.cryptonian"
    compileSdk = 35

    defaultConfig {
        applicationId = "com.devsbitwise.cryptonian"
        minSdk = 21
        targetSdk = 34
        versionCode = Integer.parseInt(project.property("BUILD_NUMBER") as String)
        versionName = "3.5.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    signingConfigs {
        getByName("debug") {
            storeFile = file(project.property("DEBUG_KEYSTORE_FILE") as String)
            storePassword = project.property("DEBUG_KEYSTORE_PASSWORD") as String
            keyAlias = project.property("DEBUG_KEYSTORE_KEY_ALIAS") as String
            keyPassword = project.property("DEBUG_KEYSTORE_KEY_PASSWORD") as String
        }
        create("release") {
            storeFile = file(project.property("CRYPTONIAN_KEYSTORE_FILE") as String)
            storePassword = project.property("CRYPTONIAN_KEYSTORE_PASSWORD") as String
            keyAlias = project.property("CRYPTONIAN_KEYSTORE_KEY_ALIAS") as String
            keyPassword = project.property("CRYPTONIAN_KEYSTORE_KEY_PASSWORD") as String
        }
    }

    buildTypes {
        debug {
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )

            configure<CrashlyticsExtension> {
                // No need during development, enabling it also prevents Gradle to work offline
                mappingFileUploadEnabled = false
            }

            signingConfig = signingConfigs.getByName("debug")
        }
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )

            signingConfig = signingConfigs.getByName("release")
        }
    }
    compileOptions {
        // Flag to support the new language APIs
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_17.toString()
    }
    buildFeatures {
        viewBinding = true
        buildConfig = true
    }
    lint {
        checkDependencies = true // Analyze each modules/subprojects during lint and merge the result into single report
    }
}

play {
    updatePriority.set(1)
    serviceAccountCredentials.set(file(project.property("PLAY_API_KEY_FILE") as String))
    artifactDir.set(file("build/outputs/bundle/release"))
}

dependencies {

    testImplementation(libs.junit)
    testImplementation(libs.google.truth)
    testImplementation(libs.koin.test) // To perform dependency injection using Koin for unit test

    androidTestImplementation(libs.androidx.runner) // Adding @SmallTest, @MediumTest, @LargeTest and enable this module to run instrumented test
    androidTestImplementation(libs.androidx.test.core)
    androidTestImplementation(libs.androidx.test.core.ktx) // Adding launchActivity extension
    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(libs.androidx.junit.ext)
    androidTestImplementation(libs.androidx.junit.ext.ktx)
    androidTestImplementation(libs.androidx.truth.ext)
    androidTestImplementation(libs.google.truth)

    // Java 8 backward compatibility support
    coreLibraryDesugaring(libs.desugar.jdk.libs)

    // Android KTX
    implementation(libs.navigation.ui.ktx)
    implementation(libs.navigation.runtime.ktx)
    implementation(libs.navigation.fragment.ktx)

    // UI
    implementation(libs.core.splashscreen)

    // UI (third party)
    implementation(libs.youtube.player)
    implementation(libs.mp.android.chart)

    // Git Submodule dependencies
    implementation(projects.androidCommon)
    implementation(projects.androidMaterial)
    implementation(projects.androidFirebase)
    implementation(projects.androidAdMob)
    implementation(projects.htmlTextView)

    implementation(projects.shared)

}