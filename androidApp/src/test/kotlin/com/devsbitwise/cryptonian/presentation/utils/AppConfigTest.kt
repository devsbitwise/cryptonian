/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.utils

import com.devsbitwise.kommon.di.commonModule
import com.devsbitwise.kommon.extensions.fromJsonString
import com.devsbitwise.kommon.extensions.toJsonString
import com.google.common.truth.Truth.assertThat
import kotlinx.serialization.json.Json
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.test.KoinTest
import org.koin.test.inject

class AppConfigTest: KoinTest {

    private val json: Json by inject()

    @Before
    fun setup() {

        startKoin {
            modules(commonModule)
        }

    }

    @Test
    fun `Fully initialized app remote config returns true`() {

        val map = HashMap<String, String>()
        map["cryptoImageBaseUrl"] = "https://asset-images.abc.io/images/"
        map["interstitialUnit"] = "ca-app-pub-1234567890/09876543221"
        map["bannerUnit"] = "ca-app-pub-1234567890/09876543221"
        map["nativeUnit"] = "ca-app-pub-1234567890/09876543221"
        map["videoKey"] = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
        map["videoChannels"] = "[{\"id\":\"UCjemQfjaXAzA-95RKoy9n_g\",\"name\":\"Discover Crypto\"},{\"id\":\"UCqK_GSMbpiV8spgD3ZGloSw\",\"name\":\"Coin Bureau\"}]"
        map["newsCategories"] = "[{\"id\":8852,\"name\":\"Binance News\"},{\"id\":27,\"name\":\"Bitcoin\"},{\"id\":383,\"name\":\"Ethereum\"}]"

        // Convert JSON String to this data class
        val appConfig = map.toJsonString(json).fromJsonString<AppConfig.Remote>(json)

        assertThat(appConfig?.requiredProperties?.all { it.isNotBlank() }).isTrue()

    }

}