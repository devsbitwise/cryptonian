<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2024 Devs Bitwise
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<com.google.android.material.card.MaterialCardView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/all_time_card"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="@dimen/space_24"
    app:cardCornerRadius="10dp">

    <androidx.appcompat.widget.LinearLayoutCompat
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical"
        android:padding="@dimen/space_24">

        <com.google.android.material.textview.MaterialTextView
            android:id="@+id/allTimeLabelToggle"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:drawableEnd="@drawable/ic_arrow_down"
            android:paddingVertical="@dimen/space_8"
            android:text="@string/all_time_high_cycle_low"
            android:textColor="?android:textColorHighlight"
            android:textSize="@dimen/textSize32"
            android:textStyle="bold" />

        <TableLayout
            android:id="@+id/allTimeViews"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:stretchColumns="1"
            android:visibility="gone">

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/ath_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight49"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/ath_date" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight50"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/time_from_ath" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight51"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/down_from_ath" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight52"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/breakeven_multiple" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight53"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/cycle_low_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight54"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/cycle_low_date" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight55"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/time_since_low" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight56"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_8">

                <com.google.android.material.textview.MaterialTextView
                    android:textSize="@dimen/textSize24"
                    android:text="@string/up_since_low" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight57"
                    android:textSize="@dimen/textSize24"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

        </TableLayout>

    </androidx.appcompat.widget.LinearLayoutCompat>

</com.google.android.material.card.MaterialCardView>