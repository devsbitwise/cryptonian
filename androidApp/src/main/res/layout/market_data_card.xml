<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2024 Devs Bitwise
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<com.google.android.material.card.MaterialCardView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/market_data_card"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="@dimen/space_14"
    app:cardCornerRadius="10dp">

    <androidx.appcompat.widget.LinearLayoutCompat
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical"
        android:padding="@dimen/space_14">

        <com.google.android.material.textview.MaterialTextView
            android:id="@+id/marketDataLabelToggle"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:drawableEnd="@drawable/ic_arrow_down"
            android:paddingVertical="@dimen/space_4"
            android:text="@string/market_data"
            android:textColor="?android:textColorHighlight"
            android:textSize="@dimen/textSize16"
            android:textStyle="bold" />

        <TableLayout
            android:id="@+id/marketDataViews"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:stretchColumns="1"
            android:visibility="gone">

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/price_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight1"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/price_btc" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight2"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/price_eth" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight3"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/real_vol" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight4"
                    android:ellipsize="marquee"
                    android:gravity="end"
                    android:singleLine="true"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/reported_vol" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight5"
                    android:ellipsize="marquee"
                    android:gravity="end"
                    android:singleLine="true"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/volume_overstatement" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight6"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/change_vs_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight7"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/change_vs_btc" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight8"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/change_vs_eth" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight9"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/low_high_1" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight10"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/low_high_24" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight11"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/last_trade" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight12"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

        </TableLayout>

    </androidx.appcompat.widget.LinearLayoutCompat>

</com.google.android.material.card.MaterialCardView>