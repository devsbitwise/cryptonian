<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2024 Devs Bitwise
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<com.google.android.material.card.MaterialCardView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/on_chain_card"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="@dimen/space_14"
    app:cardCornerRadius="10dp">

    <androidx.appcompat.widget.LinearLayoutCompat
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical"
        android:padding="@dimen/space_14">

        <com.google.android.material.textview.MaterialTextView
            android:id="@+id/onChainLabelToggle"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:drawableEnd="@drawable/ic_arrow_down"
            android:paddingVertical="@dimen/space_4"
            android:text="@string/on_chain_data"
            android:textColor="?android:textColorHighlight"
            android:textSize="@dimen/textSize16"
            android:textStyle="bold" />

        <TableLayout
            android:id="@+id/onChainViews"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:stretchColumns="1"
            android:visibility="gone">

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/transaction_vol_24h_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight27"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/adjusted_transaction_volume_24h_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight28"
                    android:ellipsize="marquee"
                    android:gravity="end"
                    android:singleLine="true"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/adjusted_nvt_ratio" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight29"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/fees_24h_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight30"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/median_transaction_volume_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight31"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/median_fee_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight32"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/active_addresses" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight33"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/transactions_24h" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight34"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/payments_24h" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight35"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/new_issuance_24h_usd" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight36"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/average_difficulty" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight37"
                    android:ellipsize="marquee"
                    android:gravity="end"
                    android:singleLine="true"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/kilobytes_added_24h" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight38"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

            <TableRow android:paddingTop="@dimen/space_4">

                <com.google.android.material.textview.MaterialTextView android:text="@string/blocks_24h" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/fieldRight39"
                    android:gravity="end"
                    android:text="@string/empty" />

            </TableRow>

        </TableLayout>

    </androidx.appcompat.widget.LinearLayoutCompat>

</com.google.android.material.card.MaterialCardView>