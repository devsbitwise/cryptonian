/*
 * Copyright (c) 2023 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.data.data_source.components

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.devsbitwise.android.common.extensions.createChannel
import com.devsbitwise.android.common.extensions.hasNotificationPermission
import com.devsbitwise.android.common.extensions.isNotificationChannelEnabled
import com.devsbitwise.android.firebase.extensions.dumpRemoteMessage
import com.devsbitwise.cryptonian.BuildConfig
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.data.data_source.remote.responses.notifications.CommonNotification
import com.devsbitwise.kommon.extensions.fromJsonString
import com.devsbitwise.kommon.extensions.toJsonString
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.serialization.json.Json
import org.koin.android.ext.android.inject


class FCMService : FirebaseMessagingService() {

    companion object {
        const val TAG = "CloudMessaging"
        const val NOTIFICATION_ACTION_DATA = "Notification Action Data"
    }

    private val json by inject<Json>()

    private val glide by inject<RequestManager>()

    private val ledColor = Color.WHITE

    private val vibrationPattern = longArrayOf(1_000L, 1_000L, 1_000L, 1_000L, 1_000L)

    private lateinit var notificationManager: NotificationManagerCompat

    private var flags = PendingIntent.FLAG_UPDATE_CURRENT

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "FCM device token: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        remoteMessage.dumpRemoteMessage()

        remoteMessage.data.toJsonString(json)?.let {

            val dataMsgPayload: CommonNotification? = it.fromJsonString(json)

            dataMsgPayload?.let { dataPayload ->

                notificationManager = NotificationManagerCompat.from(this).apply {
                    createChannel(
                        channelName = dataPayload.channel,
                        ledColor = ledColor,
                        vibratePattern = vibrationPattern,
                        soundUri = Uri.Builder()
                            .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                            .authority(packageName)
                            .appendPath("${R.raw.marbles}")
                            .build(),
                        audioAttr = AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .build()
                    )
                }

                if (hasNotificationPermission(this).not() && notificationManager.isNotificationChannelEnabled(this, dataPayload.channel).not()) {
                    Log.d(TAG, "onMessageReceived: ${getString(R.string.notification_error)}")
                    return
                }

                // Environment check
                if (BuildConfig.BUILD_TYPE.equals(dataPayload.environment, ignoreCase = true).not()) {
                    return
                }

                val sentTime = remoteMessage.sentTime

                if (dataPayload.bigImage.isNullOrBlank()) {
                    showNotification(sentTime, null, dataPayload)
                }
                else {

                    glide
                        .asBitmap()
                        .load(dataPayload.bigImage)
                        .listener(object : RequestListener<Bitmap> {

                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any?,
                                target: Target<Bitmap>,
                                isFirstResource: Boolean
                            ): Boolean {
                                showNotification(sentTime, null, dataPayload)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Bitmap,
                                model: Any,
                                target: Target<Bitmap>?,
                                dataSource: DataSource,
                                isFirstResource: Boolean
                            ): Boolean {
                                showNotification(sentTime, resource, dataPayload)
                                return false
                            }

                        }).submit()

                }

            } ?: run {
                Log.wtf(TAG, "onMessageReceived: ${getString(R.string.unknown_error)}")
            }

        } ?: run {
            Log.wtf(TAG, "onMessageReceived: ${getString(R.string.unknown_error)}")
        }

    }

    @SuppressLint("MissingPermission") // Permission checking should already perform before calling this function
    private fun showNotification(sentTime: Long, bitmap: Bitmap?, msgData: CommonNotification) {

        val notificationID = sentTime.toInt()
        val notificationTitle = msgData.title
        val notificationDescription = msgData.description
        val notificationAction = msgData.action
        val notificationActionData = msgData.actionData

        val notificationBuilder = NotificationCompat.Builder(this, msgData.channel)
            .setSmallIcon(R.drawable.ic_rss_feed)
            .setContentTitle(notificationTitle)
            .setContentText(notificationDescription)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setCategory(NotificationCompat.CATEGORY_EVENT)
            .setLights(ledColor, 500, 2_000) // Set custom notification light on older Android version
            .setVibrate(vibrationPattern) // Set custom notification vibration on older Android version
            .setSound(
                Uri.Builder()
                    .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                    .authority(packageName)
                    .appendPath("${R.raw.marbles}")
                    .build()
            ) // Set custom notification sound on older Android version
            .setOnlyAlertOnce(true)
            .setAutoCancel(true)

        // Remote image is optional
        if (bitmap == null) {

            notificationBuilder.setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(notificationDescription)
            )

        } else {

            // Fix overload resolution ambiguity
            val emptyBitmap: Bitmap? = null

            notificationBuilder
                .setLargeIcon(bitmap)
                .setStyle(
                    NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap)
                        .setBigContentTitle(notificationTitle)
                        .setSummaryText(notificationDescription)
                        .bigLargeIcon(emptyBitmap)
                )

        }

        setNotificationAction(notificationBuilder, notificationAction, notificationActionData, notificationID)

        notificationManager.notify(
            notificationID.toString(),
            notificationID,
            notificationBuilder.build()
        )

    }

    private fun setNotificationAction(
        notificationBuilder: NotificationCompat.Builder,
        notificationAction: String,
        notificationActionData: String?,
        notificationID: Int
    ) {

        // Required starting Android 12 (S) https://developer.android.com/about/versions/12/behavior-changes-12#pending-intent-mutability
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            flags = flags or PendingIntent.FLAG_IMMUTABLE
        }

        var notificationIntent = Intent(notificationAction).apply {
            putExtra(NOTIFICATION_ACTION_DATA, notificationAction)
        }

        if (notificationAction == Intent.ACTION_VIEW) {
            notificationIntent = Intent(Intent.ACTION_VIEW, Uri.parse(notificationActionData))
        }

        // Starting Android 9 (P) calling startActivity() from outside of an Activity context requires the FLAG_ACTIVITY_NEW_TASK flag
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        // Launch Activity via getActivity
        val pendingIntent = PendingIntent.getActivity(
            this,
            notificationID,
            notificationIntent,
            flags or PendingIntent.FLAG_ONE_SHOT
        )

        notificationBuilder.setContentIntent(pendingIntent)

    }

}