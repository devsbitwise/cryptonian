/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.data.data_source.remote.responses.notifications

import androidx.annotation.Keep
import com.devsbitwise.android.common.BuildConfig
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_STRING
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class CommonNotification(
    @SerialName("title")
    val title: String,
    @SerialName("description")
    val description: String,
    @SerialName("channel")
    val channel: String,
    @SerialName("action")
    val action: String = DEFAULT_VALUE_STRING,
    @SerialName("actionData")
    val actionData: String? = DEFAULT_VALUE_STRING,
    @SerialName("bigImage")
    val bigImage: String? = DEFAULT_VALUE_STRING,
    @SerialName("environment")
    val environment: String = BuildConfig.BUILD_TYPE
)