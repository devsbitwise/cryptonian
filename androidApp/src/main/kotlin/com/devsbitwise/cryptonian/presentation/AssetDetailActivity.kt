/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.collectFlow
import com.devsbitwise.android.common.extensions.createBitmapFromView
import com.devsbitwise.android.common.extensions.formatFractional
import com.devsbitwise.android.common.extensions.getParcelable
import com.devsbitwise.android.common.extensions.hasImageStoragePermission
import com.devsbitwise.android.common.extensions.isNegative
import com.devsbitwise.android.common.extensions.openExternalIntent
import com.devsbitwise.android.common.extensions.openSettingsIntent
import com.devsbitwise.android.common.extensions.requestImageStoragePermission
import com.devsbitwise.android.common.extensions.shareImageIntent
import com.devsbitwise.android.common.extensions.showToast
import com.devsbitwise.android.common.presentation.states.BitmapToFileState
import com.devsbitwise.android.common.presentation.ui.activity.BaseActivity
import com.devsbitwise.android.common.presentation.ui.adapters.FragmentAdapter
import com.devsbitwise.android.firebase.extensions.recordException
import com.devsbitwise.android.material.extensions.materialDialog
import com.devsbitwise.android.material.extensions.snackBar
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.ActivityAssetDetailBinding
import com.devsbitwise.cryptonian.databinding.AmountConverterBinding
import com.devsbitwise.cryptonian.presentation.events.activity.AssetDetailActivityEvent
import com.devsbitwise.cryptonian.presentation.ui.assets.ExchangeFragment
import com.devsbitwise.cryptonian.presentation.ui.assets.MetricsFragment
import com.devsbitwise.cryptonian.presentation.ui.assets.OverviewFragment
import com.devsbitwise.cryptonian.presentation.ui.assets.ProfileFragment
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.cryptonian.presentation.utils.AppUtils
import com.devsbitwise.cryptonian.presentation.viewmodels.shared.AssetDetailSharedViewModel
import com.devsbitwise.messari.domain.models.MessariAssetDataDomain
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class AssetDetailActivity : BaseActivity<ActivityAssetDetailBinding, ViewModel>(),
    OverviewFragment.OverviewListener,
    ProfileFragment.ProfileListener,
    MetricsFragment.MetricsListener,
    ExchangeFragment.ExchangeListener,
    Toolbar.OnMenuItemClickListener {

    companion object {
        const val ARG_ASSET = "ARG_ASSET"
    }

    private lateinit var parentView: View
    private lateinit var viewPager: ViewPager2
    private lateinit var appBarLayout: AppBarLayout
    private lateinit var rateTxtV: MaterialTextView
    private lateinit var priceTxtV: MaterialTextView

    private lateinit var alertDialog: AlertDialog

    private val _getAsset: MessariAssetDataDomain by lazy {
        getParcelable(ARG_ASSET)!!
    }

    private val permissionResultLauncher = getPermission()

    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (viewPager.currentItem != 0) {
                viewPager.setCurrentItem(0, true)
            }
            else {
                remove()
                onBackPressedDispatcher.onBackPressed()
            }
        }
    }

    private val glide by inject<RequestManager>()

    override val viewModel by viewModel<AssetDetailSharedViewModel>()

    override fun bindingInflater(): (LayoutInflater) -> ActivityAssetDetailBinding =
        ActivityAssetDetailBinding::inflate

    override fun initViews(binding: ActivityAssetDetailBinding) {

        binding.apply {

            appBarLayout = appBar

            toolbar.setNavigationOnClickListener {
                backPressedCallback.handleOnBackPressed()
            }

            toolbar.setOnMenuItemClickListener(this@AssetDetailActivity)

            parentView = containerView

            viewPager = viewPagerContainer

            priceTxtV = priceUsd
            rateTxtV = percentageRateTxt

            val tabLayout = tabLayout

            glide
                .load(
                    getString(
                        R.string.icon_url,
                        Firebase.remoteConfig.getString(AppConfig.CRYPTO_IMAGE_BASE_URL),
                        _getAsset.id
                    )
                )
                .placeholder(R.mipmap.ic_launcher_round)
                .circleCrop()
                .into(assetIcon)

            val categories =
                _getAsset.profileDomain.generalDomain.overviewDomain.category?.ifBlank { getString(R.string.empty) } ?: getString(R.string.empty)
            val sectors =
                _getAsset.profileDomain.generalDomain.overviewDomain.sector?.ifBlank { getString(R.string.empty) } ?: getString(R.string.empty)

            toolbar.title = _getAsset.name
            toolbar.subtitle = _getAsset.symbol?.ifBlank { getString(R.string.empty) } ?: getString(R.string.empty)
            priceTxtV.text = getString(
                R.string.us_dollars,
                _getAsset.metricsDomain.marketDataDomain.priceUsd.formatFractional()
            )
            descriptionTxt.text = _getAsset.profileDomain.generalDomain.overviewDomain.tagline
            sectorTxt.text = getString(R.string.sector, sectors)
            categoryTxt.text = getString(R.string.category, categories)
            rateTxtV.text = getString(
                R.string.percent,
                _getAsset.metricsDomain.marketDataDomain.percentChangeUsdLast24Hours.formatFractional()
            )

            AppUtils.displayPercentChange(
                rateTxtV,
                _getAsset.metricsDomain.marketDataDomain.percentChangeUsdLast24Hours
            )

            if (_getAsset.metricsDomain.marketDataDomain.percentChangeUsdLast24Hours.isNegative()) {
                priceTxtV.setTextColor(Color.RED)
            } else {
                priceTxtV.setTextColor(Color.GREEN)
            }

            if (descriptionTxt.text.isBlank()) {
                descriptionTxt.visibility = View.GONE
            }

            val fragmentList = mutableListOf<Pair<String, Fragment>>(
                Pair(getString(R.string.overview), OverviewFragment.newInstance()),
                Pair(getString(R.string.profile), ProfileFragment.newInstance()),
                Pair(getString(R.string.exchange), ExchangeFragment.newInstance()),
                Pair(getString(R.string.metrics), MetricsFragment.newInstance())
            )

            val adapter = FragmentAdapter(fragmentList, this@AssetDetailActivity)

            viewPager.adapter = adapter
            // Retain every offscreen Fragments since its ViewModel is being cleared after configuration change if not visible on the screen
            viewPager.offscreenPageLimit = fragmentList.size.minus(1)
            // Disabling swipe gesture due to conflict with CandleStickChart in OverviewFragment
            viewPager.isUserInputEnabled = false

            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = adapter.getFragmentTabName(position)
            }.attach()

            val customView = AmountConverterBinding.inflate(layoutInflater).apply {
                amountInputLayout.hint = getString(R.string.field_amount, _getAsset.name)
                amountInputEt.apply {
                    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                    doAfterTextChanged {

                        val coins = text.toString()

                        amountTxt.text = try {

                            getString(
                                R.string.us_dollars,
                                coins.toDouble().times(
                                    (_getAsset.metricsDomain.marketDataDomain.priceUsd ?: 0.0)
                                ).formatFractional()
                            )

                        } catch (e: Exception) {
                            e.printStackTrace()
                            getString(R.string.sampleUsd)
                        }

                    }
                }
            }

            alertDialog = materialDialog(
                getString(R.string.to_usd, _getAsset.name),
                null,
                getString(R.string.navigation_close),
                { dialog, _ -> dialog.dismiss() },
                null,
                null,
                true
            ).setView(customView.root).create()

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        // Do not pass LifecycleOwner here since this Activity will take over the control immediately
        // when onResume thus overlap its Fragments back press control, this Activity is now responsible
        // for removing its own OnBackPressedCallback
        onBackPressedDispatcher.addCallback(backPressedCallback)

        with(viewModel) {
            collectFlow(bitmapToFileState, ::onBitmapStateChanged)
        }

    }

    private fun onBitmapStateChanged(state: BitmapToFileState) {

        when (state) {

            is BitmapToFileState.FetchLoading -> {
                showToast(getString(R.string.saving_image), Toast.LENGTH_SHORT)
            }

            is BitmapToFileState.FetchSuccess -> {
                openExternalIntent(
                    shareImageIntent(
                        desc = getString(
                            R.string.app_promotion,
                            getString(
                                R.string.asset_info,
                                _getAsset.name,
                                rateTxtV.text.toString(),
                                priceTxtV.text.toString()
                            ),
                            getString(R.string.app_promo)
                        ),
                        uri = state.uri
                    ).apply {
                        showToast(getString(R.string.image_saved), Toast.LENGTH_LONG)
                    }
                )?.let { e ->
                    e.localizedMessage?.let { msg -> showToast(msg, Toast.LENGTH_LONG) }
                    e.recordException()
                }
            }

            is BitmapToFileState.FetchFailed -> {
                showToast(getString(R.string.image_failed), Toast.LENGTH_LONG)
                recordException(state.msg)
            }

        }

    }

    override fun onMenuItemClick(item: MenuItem): Boolean {

        when (item.itemId) {

            R.id.action_converter -> {

                if (_getAsset.metricsDomain.marketDataDomain.priceUsd == null) {
                    snackBar(getString(R.string.not_available)).show()
                } else {
                    alertDialog.show()
                }

            }

            R.id.action_share -> {
                saveScreenshot(parentView)
            }

        }

        return true

    }

    override fun onDestroy() {
        alertDialog.dismiss()
        super.onDestroy()
    }

    override fun onPermissionResult(permissions: Map<String, Boolean>, activity: ComponentActivity) {

        with(activity) {
            // If any from the request permission fails
            if (permissions.containsValue(false)) {

                snackBar(
                    getString(R.string.permission_denied),
                    getString(R.string.settings),
                    {

                        if (isFinishing || isDestroyed) {
                            return@snackBar
                        }

                        openSettingsIntent()?.let { e ->
                            e.localizedMessage?.also { msg -> showToast(msg, Toast.LENGTH_LONG) }
                            e.recordException()
                        }

                    },
                    Snackbar.LENGTH_LONG,
                    parent = parentView
                ).show()

            } else {
                showToast(getString(R.string.permission_granted), Toast.LENGTH_SHORT)
            }
        }

    }

    private fun saveScreenshot(view: View) {

        if (hasImageStoragePermission(this)) {

            viewModel.onTriggerEvent(
                AssetDetailActivityEvent.SaveBitmap(
                    view.createBitmapFromView(R.color.mdc_colorWhite_PrimaryDark),
                    getString(R.string.app_name)
                )
            )

        } else {
            permissionResultLauncher.requestImageStoragePermission()
        }

    }

    override fun getAsset(): MessariAssetDataDomain = _getAsset

    override fun expandAppBarLayout() {
        // Set animate to false due to existing design issue
        // Reference: https://github.com/material-components/material-components-android/issues/3854
        appBarLayout.setExpanded(true, false)
    }

}