/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.assets

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.canScrollUp
import com.devsbitwise.android.common.extensions.collectFlow
import com.devsbitwise.android.common.extensions.createBitmapFromView
import com.devsbitwise.android.common.extensions.hasImageStoragePermission
import com.devsbitwise.android.common.extensions.openExternalIntent
import com.devsbitwise.android.common.extensions.openSettingsIntent
import com.devsbitwise.android.common.extensions.requestImageStoragePermission
import com.devsbitwise.android.common.extensions.resToColor
import com.devsbitwise.android.common.extensions.shareImageIntent
import com.devsbitwise.android.common.extensions.showToast
import com.devsbitwise.android.common.extensions.smoothScrollWithinCoordinator
import com.devsbitwise.android.common.presentation.states.BitmapToFileState
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.android.firebase.extensions.recordException
import com.devsbitwise.android.material.extensions.materialDialog
import com.devsbitwise.android.material.extensions.snackBar
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentExchangeListBinding
import com.devsbitwise.cryptonian.presentation.adapters.crypto.ExchangeAdapter
import com.devsbitwise.cryptonian.presentation.adapters.crypto.ExchangeMidAdapter
import com.devsbitwise.cryptonian.presentation.events.activity.AssetDetailActivityEvent
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.cryptonian.presentation.viewmodels.shared.AssetDetailSharedViewModel
import com.devsbitwise.messari.domain.models.MessariAssetDataDomain
import com.devsbitwise.messari.domain.models.MessariExchangeDataDomain
import com.devsbitwise.messari.presentation.events.exchanges.MessariExchangeEvent
import com.devsbitwise.messari.presentation.states.exchanges.MessariExchangeState
import com.devsbitwise.messari.presentation.viewmodels.MessariExchangeViewModel
import com.google.android.material.divider.MaterialDividerItemDecoration
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ExchangeFragment : BaseFragment<FragmentExchangeListBinding, MessariExchangeViewModel>() {

    companion object {
        fun newInstance() = ExchangeFragment()
    }

    private lateinit var assetId: String

    private lateinit var concatAdapter: ConcatAdapter
    private lateinit var includedExchangeAdapter: ExchangeAdapter
    private lateinit var excludedExchangeAdapter: ExchangeAdapter
    private val middleAdapter = ExchangeMidAdapter { alertDialog.show() }

    private lateinit var logTxtV: MaterialTextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private lateinit var alertDialog: AlertDialog

    private lateinit var listener: ExchangeListener

    private val permissionResultLauncher = getPermission()

    private val assetDetailSharedViewModel by activityViewModel<AssetDetailSharedViewModel>()

    private val glide by inject<RequestManager>()

    override var interceptBackPress: Boolean = true

    override val viewModel by viewModel<MessariExchangeViewModel>()

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentExchangeListBinding =
        FragmentExchangeListBinding::inflate

    override fun initViews(binding: FragmentExchangeListBinding) {

        binding.apply {

            val asset = listener.getAsset()
            assetId = asset.id.trim()

            recyclerView = rvExchanges
            logTxtV = errorLog
            swipeRefreshLayout = refreshLayout.apply {
                setColorSchemeColors(context.resToColor(R.color.mdc_colorWhite_PrimaryDark))
            }

            includedExchangeAdapter = ExchangeAdapter(
                Firebase.remoteConfig.getString(AppConfig.CRYPTO_IMAGE_BASE_URL),
                glide,
                object : ExchangeAdapter.ItemListener {

                    override fun onRequestScreenShot(view: View, description: String) {
                        saveScreenshot(view, description)
                    }

                    override fun onListUpdate(
                        previousList: List<MessariExchangeDataDomain>,
                        currentList: List<MessariExchangeDataDomain>
                    ) {
                        /* no-op */
                    }

                }
            )

            excludedExchangeAdapter = ExchangeAdapter(
                Firebase.remoteConfig.getString(AppConfig.CRYPTO_IMAGE_BASE_URL),
                glide,
                object : ExchangeAdapter.ItemListener {

                    override fun onRequestScreenShot(view: View, description: String) {
                        saveScreenshot(view, description)
                    }

                    override fun onListUpdate(
                        previousList: List<MessariExchangeDataDomain>,
                        currentList: List<MessariExchangeDataDomain>
                    ) {
                        if (currentList.isEmpty()) {
                            concatAdapter.removeAdapter(middleAdapter)
                        }
                        else {
                            concatAdapter.addAdapter(1, middleAdapter)
                        }
                    }

                }
            )

            concatAdapter = ConcatAdapter(includedExchangeAdapter, excludedExchangeAdapter)

            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = concatAdapter

            recyclerView.addItemDecoration(
                MaterialDividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )

            alertDialog = requireContext().materialDialog(
                getString(R.string.vwap_detail),
                getString(R.string.vwap),
                getString(R.string.navigation_close),
                { dialog: DialogInterface, _: Int -> dialog.dismiss() },
                null,
                null,
                false
            ).create()

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        with(assetDetailSharedViewModel) {
            viewLifecycleOwner.collectFlow(bitmapToFileState, ::onBitmapStateChanged)
        }

        with(viewModel) {

            viewLifecycleOwner.collectFlow(messariExchangeState, ::onExchangeStateChanged)

            // Recover the the UI state during configuration change.
            // We cannot use the onSavedInstanceState to save and recover the UI loading state because if system-initiated process death occur
            // the UI will be in loading state forever. We only want UI states to survive a configuration change using ViewModel.
            swipeRefreshLayout.isRefreshing = isFetchOngoing

            if (includeExchangesList.isEmpty() && excludeExchangesList.isEmpty()) {

                // Prevent repetitive initial fetch which will cause duplicate items.
                // This could happen when the response is slow and user keep switching between this and other fragments.
                if (isFetchOngoing) {
                    return
                }

                // Initial fetch
                fetchData()

            }
            else {
                // Recover data after configuration change
                includedExchangeAdapter.submitList(includeExchangesList)
                excludedExchangeAdapter.submitList(excludeExchangesList)
            }

        }

        swipeRefreshLayout.setOnRefreshListener {
            // Reset or retry if initial fetch failed
            fetchData()
        }

    }

    private fun onExchangeStateChanged(state: MessariExchangeState) {

        when (state) {

            is MessariExchangeState.FetchLoading -> {

                logTxtV.text = null

                swipeRefreshLayout.isRefreshing = true

                includedExchangeAdapter.submitList(state.includedList) // Cache
                excludedExchangeAdapter.submitList(state.excludedList) // Cache

            }

            is MessariExchangeState.FetchSuccess -> {

                swipeRefreshLayout.isRefreshing = false

                includedExchangeAdapter.submitList(state.includedList)
                excludedExchangeAdapter.submitList(state.excludedList)

                // Since this is a free limited API, data can be empty
                if (state.includedList.isEmpty() && state.excludedList.isEmpty()) {
                    logTxtV.setText(R.string.no_data)
                }

            }

            is MessariExchangeState.FetchFailed -> {

                swipeRefreshLayout.isRefreshing = false

                if (state.includedList.isNullOrEmpty() && state.excludedList.isNullOrEmpty()) {
                    // We can't rely on `onCurrentListChanged` of the adapter as we have no way of knowing
                    // what state that cause the list to become empty
                    if (includedExchangeAdapter.currentList.isEmpty() && excludedExchangeAdapter.currentList.isEmpty()) {
                        // In case the fetch failed and has no cache
                        logTxtV.setText(R.string.swipe_to_refresh)
                        return
                    }
                    activity?.snackBar(getString(R.string.no_data), parent = view)?.show()
                }
                else {
                    activity?.snackBar(getString(R.string.cache_loaded), parent = view)?.show()
                }

                // There is no point of submitting local cache data on this state
                // since it is always just the same data coming from loading state

            }

        }

    }

    private fun onBitmapStateChanged(state: BitmapToFileState) {

        with(requireContext()) {

            when (state) {

                is BitmapToFileState.FetchLoading -> {
                    showToast(getString(R.string.saving_image), Toast.LENGTH_SHORT)
                }

                is BitmapToFileState.FetchSuccess -> {
                    activity?.openExternalIntent(
                        shareImageIntent(
                            desc = getString(
                                R.string.app_promotion,
                                state.description,
                                getString(R.string.app_promo)
                            ),
                            uri = state.uri
                        ).apply {
                            showToast(getString(R.string.image_saved), Toast.LENGTH_LONG)
                        }
                    )?.let { e ->
                        e.localizedMessage?.let { msg -> showToast(msg, Toast.LENGTH_LONG) }
                        e.recordException()
                    }
                }

                is BitmapToFileState.FetchFailed -> {
                    showToast(getString(R.string.image_failed), Toast.LENGTH_LONG)
                    recordException(state.msg)
                }

            }

        }

    }

    private fun fetchData() {
        viewModel.onTriggerEvent(MessariExchangeEvent.FetchExchanges(assetId))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is ExchangeListener) {
            listener = context
        }

    }

    override fun onPause() {
        alertDialog.dismiss()
        super.onPause()
    }

    override fun onPermissionResult(permissions: Map<String, Boolean>, activity: ComponentActivity) {

        with(activity) {
            // If any from the request permission fails
            if (permissions.containsValue(false)) {

                snackBar(
                    getString(R.string.permission_denied),
                    getString(R.string.settings),
                    {

                        if (isRemoving || isDetached) {
                            return@snackBar
                        }

                        openSettingsIntent()?.let { e ->
                            e.localizedMessage?.also { msg -> showToast(msg, Toast.LENGTH_LONG) }
                            e.recordException()
                        }

                    },
                    Snackbar.LENGTH_LONG,
                    view
                ).show()

            } else {
                showToast(getString(R.string.permission_granted), Toast.LENGTH_SHORT)
            }
        }

    }

    override fun onBackPressed() {
        if (recyclerView.canScrollUp()) {
            recyclerView.smoothScrollWithinCoordinator(0)
            listener.expandAppBarLayout()
        } else {
            super.onBackPressed()
        }
    }

    private fun saveScreenshot(view: View, description: String) {

        with(view.context) {

            if (hasImageStoragePermission(this)) {

                assetDetailSharedViewModel.onTriggerEvent(
                    AssetDetailActivityEvent.SaveBitmap(
                        view.createBitmapFromView(R.color.mdc_colorWhite_PrimaryDark),
                        getString(R.string.app_name),
                        description
                    )
                )

            } else {
                permissionResultLauncher.requestImageStoragePermission()
            }

        }

    }

    interface ExchangeListener {

        fun getAsset(): MessariAssetDataDomain
        fun expandAppBarLayout()

    }

}