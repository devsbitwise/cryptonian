/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.adapters.crypto

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.formatFractional
import com.devsbitwise.android.common.extensions.formatWithUnit
import com.devsbitwise.android.common.extensions.isNegative
import com.devsbitwise.android.common.presentation.ui.adapters.FilterableListAdapter
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.AssetCardBinding
import com.devsbitwise.cryptonian.presentation.utils.AppUtils
import com.devsbitwise.cryptonian.presentation.utils.diffutils.DiffUtilExchange
import com.devsbitwise.messari.domain.models.MessariExchangeDataDomain

class ExchangeAdapter(
    private val iconLink: String,
    private val glide: RequestManager,
    private val itemListener: ItemListener
) : FilterableListAdapter<MessariExchangeDataDomain, ExchangeAdapter.ItemView>(DiffUtilExchange()) {

    inner class ItemView(itemView: AssetCardBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val exchangeName = itemView.assetName
        internal val exchangePrice = itemView.assetPrice
        private val exchangeVolume = itemView.assetMarketCap
        private val exchangePercentChange = itemView.assetPercentChange
        internal val exchangePair = itemView.intervalExchangeTxt
        private val exchangeIcon = itemView.assetIcon
        internal val exchangeShare = itemView.assetShare

        // Full update/binding
        fun bindFull(domain: MessariExchangeDataDomain) {
            
            with(itemView.context) {

                bindTextData(
                    domain.exchangeName,
                    domain.priceUsd,
                    domain.volumeLast24Hours,
                    domain.deviationFromVwapPercent,
                    domain.pair
                )

                glide
                    .load(
                        getString(
                            R.string.icon_url,
                            iconLink,
                            domain.exchangeId
                        )
                    )
                    .placeholder(R.mipmap.ic_launcher_round)
                    .circleCrop()
                    .into(exchangeIcon)
                
            }
            
        }

        // Partial update/binding
        fun bindPartial(domain: MessariExchangeDataDomain, bundle: Bundle) {
            bindTextData(
                domain.exchangeName,
                bundle.getDouble(DiffUtilExchange.ARG_EXCHANGE_PRICE),
                bundle.getDouble(DiffUtilExchange.ARG_EXCHANGE_VOLUME),
                bundle.getDouble(DiffUtilExchange.ARG_EXCHANGE_VWAP),
                domain.pair,
            )
        }

        private fun bindTextData(
            name: String?,
            priceUsd: Double?,
            volumeLast24Hours: Double?,
            deviationFromVwapPercent: Double?,
            pair: String
        ) {

            with(itemView.context) {

                exchangeName.apply {
                    text = name
                    isSelected = true
                }
                exchangePrice.text = getString(
                    R.string.us_dollars,
                    priceUsd.formatFractional()
                )
                exchangeVolume.apply {
                    text = getString(
                        R.string.vol_24h,
                        volumeLast24Hours.formatWithUnit()
                    )
                    isSelected = true
                }

                exchangePair.text = pair

                AppUtils.displayPercentChange(exchangePercentChange, deviationFromVwapPercent)

                if (deviationFromVwapPercent.isNegative()) {
                    exchangePrice.setTextColor(Color.RED)
                } else {
                    exchangePrice.setTextColor(Color.GREEN)
                }

            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView =
        ItemView(AssetCardBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        // Reference: https://forums.raywenderlich.com/t/speed-up-your-android-recyclerview-using-diffutil-raywenderlich-com/141338/8?u=archeremiya
        onBindViewHolder(holder, holder.bindingAdapterPosition, emptyList())
    }

    override fun onBindViewHolder(holder: ItemView, position: Int, payloads: List<Any>) {

        with(holder) {

            // For most cases `getBindingAdapterPosition()` is use as it provides the most up-to-date position considering pending changes.
            // It is also ideal for getting data from `getCurrentList()` as it returns the only position under this specific adapter and
            // not the entire adapters of a ConcatAdapter.
            // Reference: https://stackoverflow.com/a/63148812
            val domain = getItem(bindingAdapterPosition)

            // Upon scroll we need to rebind click listener regardless if full or partial update
            // this is to ensure that click listener is bound to correct item.
            exchangeShare.setOnClickListener {
                itemListener.onRequestScreenShot(
                    itemView,
                    it.context.getString(
                        R.string.asset_info,
                        domain.exchangeName,
                        exchangePair.text.toString(),
                        exchangePrice.text.toString()
                    )
                )
            }

            if (payloads.isEmpty() || payloads.first() !is Bundle) {
                bindFull(domain) // Full update/binding
            }
            else {
                val bundle = payloads.first() as Bundle
                bindPartial(domain, bundle) // Partial update/binding
            }

        }

    }

    override fun onFilter(list: List<MessariExchangeDataDomain>, constraint: String): List<MessariExchangeDataDomain> {
        TODO("Not yet implemented")
    }

    // Since adapter.currentList() does not immediately reflecting the actual update from filters
    // we can use this callback instead to listen and get the latest list.
    override fun onCurrentListChanged(
        previousList: List<MessariExchangeDataDomain>,
        currentList: List<MessariExchangeDataDomain>
    ) {
        super.onCurrentListChanged(previousList, currentList)
        itemListener.onListUpdate(previousList, currentList)
    }

    interface ItemListener {

        fun onRequestScreenShot(view: View, description: String)

        fun onListUpdate(
            previousList: List<MessariExchangeDataDomain>,
            currentList: List<MessariExchangeDataDomain>
        )

    }

}