/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.adapters.crypto

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.formatFractional
import com.devsbitwise.android.common.extensions.formatWithUnit
import com.devsbitwise.android.common.extensions.isNegative
import com.devsbitwise.android.common.presentation.ui.adapters.FilterableListAdapter
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.AssetCardBinding
import com.devsbitwise.cryptonian.presentation.utils.AppUtils
import com.devsbitwise.cryptonian.presentation.utils.diffutils.DiffUtilAsset
import com.devsbitwise.messari.domain.models.MessariAssetDataDomain

class AssetAdapter(
    private val iconLink: String,
    private val glide: RequestManager,
    private val itemListener: ItemListener
) : FilterableListAdapter<MessariAssetDataDomain, AssetAdapter.ItemView>(DiffUtilAsset()) {

    inner class ItemView(itemView: AssetCardBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val assetName = itemView.assetName
        private val assetPrice = itemView.assetPrice
        private val assetMarketCap = itemView.assetMarketCap
        private val assetPercentChange = itemView.assetPercentChange
        private val assetIcon = itemView.assetIcon
        private val assetShare = itemView.assetShare

        // Full update/binding
        fun bindFull(domain: MessariAssetDataDomain) {

            with(itemView.context) {

                bindTextData(
                    domain.symbol ?: domain.name,
                    domain.metricsDomain.marketDataDomain.priceUsd,
                    domain.metricsDomain.marketDomain.currentMarketcapUsd,
                    domain.metricsDomain.marketDataDomain.percentChangeUsdLast24Hours
                )

                glide
                    .load(
                        getString(
                            R.string.icon_url,
                            iconLink,
                            domain.id
                        )
                    )
                    .placeholder(R.mipmap.ic_launcher_round)
                    .circleCrop()
                    .into(assetIcon)

                // It is advisable to always use interface when working with `View.setOnClickListener`
                // in adapter to avoid outdated binding and data reference.
                // Passing position and using `adapter.currentList[position]` in the Activity/Fragment
                // is better than passing object via `getItem(position)` from here.
                // Reference: https://stackoverflow.com/q/77308368/12204620

                assetShare.setOnClickListener {
                    itemListener.onRequestScreenShot(
                        itemView,
                        it.context.getString(
                            R.string.asset_info,
                            domain.name,
                            assetPercentChange.text.toString(),
                            assetPrice.text.toString()
                        )
                    )
                }

                itemView.setOnClickListener {
                    itemListener.onItemSelected(bindingAdapterPosition)
                }

            }


        }

        // Partial update/binding
        fun bindPartial(domain: MessariAssetDataDomain, bundle: Bundle) {
            bindTextData(
                domain.symbol ?: domain.name,
                bundle.getDouble(DiffUtilAsset.ARG_ASSET_PRICE),
                bundle.getDouble(DiffUtilAsset.ARG_ASSET_MARKET_CAP),
                bundle.getDouble(DiffUtilAsset.ARG_ASSET_PERCENTAGE)
            )
        }


        private fun bindTextData(nameSymbol: String, priceUsd: Double?, mCap: Double?, percent: Double?) {

            with(itemView.context) {

                assetName.text = nameSymbol
                assetPrice.text = getString(
                    R.string.us_dollars,
                    priceUsd.formatFractional()
                )
                assetMarketCap.text = getString(
                    R.string.mcap,
                    mCap.formatWithUnit()
                )

                AppUtils.displayPercentChange(assetPercentChange, percent)

                if (percent.isNegative()) {
                    assetPrice.setTextColor(Color.RED)
                } else {
                    assetPrice.setTextColor(Color.GREEN)
                }
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView =
        ItemView(AssetCardBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        // Reference: https://forums.raywenderlich.com/t/speed-up-your-android-recyclerview-using-diffutil-raywenderlich-com/141338/8?u=archeremiya
        onBindViewHolder(holder, holder.bindingAdapterPosition, emptyList())
    }

    override fun onBindViewHolder(holder: ItemView, position: Int, payloads: List<Any>) {

        with(holder) {

            // For most cases `getBindingAdapterPosition()` is use as it provides the most up-to-date position considering pending changes.
            // It is also ideal for getting data from `getCurrentList()` as it returns the only position under this specific adapter and
            // not the entire adapters of a ConcatAdapter.
            // Reference: https://stackoverflow.com/a/63148812
            val domain = getItem(bindingAdapterPosition)

            if (payloads.isEmpty() || payloads.first() !is Bundle) {
                holder.bindFull(domain) // Full update/binding
            }
            else {
                val bundle = payloads.first() as Bundle
                holder.bindPartial(domain, bundle) // Partial update/binding
            }

        }

    }

    // Required when setHasStableIds is set to true
    override fun getItemId(position: Int): Long {
        return currentList[position].id.hashCode().toLong()
    }

    override fun onFilter(list: List<MessariAssetDataDomain>, constraint: String): List<MessariAssetDataDomain> {

        return list.filter {
            it.name.lowercase().contains(constraint.lowercase()) ||
            it.symbol?.lowercase()?.contains(constraint.lowercase()) == true
        }

    }

    // Since adapter.currentList() does not immediately reflecting the actual update from filters
    // we can use this callback instead to listen and get the latest list.
    override fun onCurrentListChanged(
        previousList: List<MessariAssetDataDomain>,
        currentList: List<MessariAssetDataDomain>
    ) {
        super.onCurrentListChanged(previousList, currentList)
        itemListener.onListUpdate(previousList, currentList)
    }

    interface ItemListener {

        fun onItemSelected(position: Int)
        fun onRequestScreenShot(view: View, description: String)
        fun onListUpdate(
            previousList: List<MessariAssetDataDomain>,
            currentList: List<MessariAssetDataDomain>
        )

    }

}