/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.assets

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModel
import com.devsbitwise.android.common.extensions.canScrollUp
import com.devsbitwise.android.common.extensions.formatFractional
import com.devsbitwise.android.common.extensions.formatTwoDecimal
import com.devsbitwise.android.common.extensions.resToDrawable
import com.devsbitwise.android.common.extensions.showOrHide
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentMetricsBinding
import com.devsbitwise.cryptonian.presentation.utils.AppUtils
import com.devsbitwise.messari.domain.models.MessariAssetDataDomain
import com.google.android.material.textview.MaterialTextView
import java.util.Locale

class MetricsFragment : BaseFragment<FragmentMetricsBinding, ViewModel>() {

    companion object {
        fun newInstance() = MetricsFragment()
    }

    private lateinit var scrollView: NestedScrollView

    private lateinit var listener: MetricsListener

    override var interceptBackPress: Boolean = true

    // No usage
    override val viewModel: ViewModel? = null

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentMetricsBinding =
        FragmentMetricsBinding::inflate

    override fun initViews(binding: FragmentMetricsBinding) {

        binding.apply {

            val asset = listener.getAsset()

            scrollView = parentScrollView

            exchangeSection.apply {
                fieldRight40.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.exchangeFlowsDomain.flowInExchangeUsd.formatFractional()
                )
                fieldRight41.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.exchangeFlowsDomain.flowOutExchangeUsd.formatFractional()
                )
            }

            marketDataSection.apply {

                fieldRight1.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.marketDataDomain.priceUsd.formatFractional()
                )
                fieldRight2.text = asset.metricsDomain.marketDataDomain.priceBtc.formatFractional()
                fieldRight3.text = asset.metricsDomain.marketDataDomain.priceEth.formatFractional()
                fieldRight4.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDataDomain.realVolumeLast24Hours.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight5.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDataDomain.volumeLast24Hours.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight6.text = asset.metricsDomain.marketDataDomain.volumeLast24HoursOverstatementMultiple.formatFractional()

                fieldRight7.text = getString(
                    R.string.percent,
                    asset.metricsDomain.marketDataDomain.percentChangeUsdLast24Hours.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight7,
                    asset.metricsDomain.marketDataDomain.percentChangeUsdLast24Hours
                )

                fieldRight8.text = getString(
                    R.string.percent,
                    asset.metricsDomain.marketDataDomain.percentChangeBtcLast24Hours.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight8,
                    asset.metricsDomain.marketDataDomain.percentChangeBtcLast24Hours
                )

                fieldRight9.text = getString(
                    R.string.percent,
                    asset.metricsDomain.marketDataDomain.percentChangeEthLast24Hours.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight9,
                    asset.metricsDomain.marketDataDomain.percentChangeEthLast24Hours
                )

                fieldRight10.text = getString(
                    R.string.ohlc,
                    asset.metricsDomain.marketDataDomain.ohlcvLast1HourDomain?.low.formatFractional(),
                    asset.metricsDomain.marketDataDomain.ohlcvLast1HourDomain?.high.formatFractional()
                )
                fieldRight11.text = getString(
                    R.string.ohlc,
                    asset.metricsDomain.marketDataDomain.ohlcvLast24HourDomain?.low.formatFractional(),
                    asset.metricsDomain.marketDataDomain.ohlcvLast24HourDomain?.high.formatFractional()
                )

                asset.metricsDomain.marketDataDomain.lastTradeAt?.let {
                    fieldRight12.text = asset.metricsDomain.marketDataDomain.formattedDate
                }

                marketDataCard.setOnClickListener {
                    arrowToggle(marketDataViews.showOrHide(), marketDataLabelToggle)
                }

            }

            marketCapSection.apply {

                fieldRight13.apply {
                    text = getString(
                        R.string.percent,
                        asset.metricsDomain.marketDomain.marketcapDominancePercent.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight14.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDomain.liquidMarketcapUsd.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight15.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDomain.currentMarketcapUsd.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight16.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDomain.realizedMarketcapUsd.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight17.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDomain.yPlus10MarketcapUsd.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight18.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDomain.y2050MarketcapUsd.formatFractional()
                    )
                    isSelected = true
                }

                marketCapCard.setOnClickListener {
                    arrowToggle(marketCapViews.showOrHide(), marketCapLabelToggle)
                }

            }

            supplySection.apply {

                fieldRight19.text = asset.metricsDomain.supplyDomain.liquid.formatFractional()
                fieldRight20.text = asset.metricsDomain.supplyDomain.circulating.formatFractional()
                fieldRight21.text = asset.metricsDomain.supplyDomain.yPlus10.formatFractional()
                fieldRight22.text = asset.metricsDomain.supplyDomain.y2050.formatFractional()
                fieldRight23.text = getString(
                    R.string.percent,
                    asset.metricsDomain.supplyDomain.yPlus10IssuedPercent.formatFractional()
                )
                fieldRight24.text = getString(
                    R.string.percent,
                    asset.metricsDomain.supplyDomain.y2050IssuedPercent.formatFractional()
                )
                fieldRight25.text = getString(
                    R.string.percent,
                    asset.metricsDomain.supplyDomain.annualInflationPercent.formatFractional()
                )
                fieldRight26.text = asset.metricsDomain.supplyDomain.stockToFlow.formatFractional()

                supplyCard.setOnClickListener {
                    arrowToggle(supplyViews.showOrHide(), supplyLabelToggle)
                }

            }

            onChainSection.apply {

                fieldRight27.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.blockchainStats24HoursDomain.transactionVolume.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight28.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.blockchainStats24HoursDomain.adjustedTransactionVolume.formatFractional()
                    )
                    isSelected = true
                }
                fieldRight29.text = asset.metricsDomain.onChainDataDomain.adjustedNvt.formatFractional()
                fieldRight30.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.onChainDataDomain.totalFeesLast24HoursUsd.formatFractional()
                )
                fieldRight31.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.onChainDataDomain.medianTransferValueUsd.formatFractional()
                )
                fieldRight32.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.onChainDataDomain.medianFeeUsd.formatFractional()
                )

                asset.metricsDomain.blockchainStats24HoursDomain.countOfActiveAddresses?.let {
                    fieldRight33.text = String.format(
                        Locale.getDefault(),
                        "%,d", it
                    )
                }

                asset.metricsDomain.blockchainStats24HoursDomain.countOfTx?.let {
                    fieldRight34.text = String.format(
                        Locale.getDefault(),
                        "%,d", it
                    )
                }

                asset.metricsDomain.blockchainStats24HoursDomain.countOfPayments?.let {
                    fieldRight35.text = String.format(
                        Locale.getDefault(),
                        "%,d", it
                    )
                }

                fieldRight36.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.blockchainStats24HoursDomain.newIssuance.formatFractional()
                )
                fieldRight37.apply {
                    text = asset.metricsDomain.blockchainStats24HoursDomain.averageDifficulty.formatFractional()
                    isSelected = true
                }
                fieldRight38.text =
                    asset.metricsDomain.blockchainStats24HoursDomain.kilobytesAdded.formatFractional()

                asset.metricsDomain.blockchainStats24HoursDomain.countOfBlocksAdded?.let {
                    fieldRight39.text = String.format(
                        Locale.getDefault(),
                        "%,d", it
                    )
                }

                onChainCard.setOnClickListener {
                    arrowToggle(onChainViews.showOrHide(), onChainLabelToggle)
                }

            }

            miningSection.apply {

                fieldRight42.text =
                    asset.metricsDomain.miningStatsDomain.networkHashRate?.ifBlank { getString(R.string.empty) } ?: getString(R.string.empty)
                fieldRight43.text = getString(
                    R.string.percent,
                    asset.metricsDomain.miningStatsDomain.availableOnNicehashPercent.formatFractional()
                )
                fieldRight44.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.miningStatsDomain.hourAttackCost.formatFractional()
                )
                fieldRight45.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.miningStatsDomain.hoursAttackCost.formatFractional()
                )
                fieldRight46.text = asset.metricsDomain.miningStatsDomain.attackAppeal.formatFractional()
                fieldRight47.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.miningStatsDomain.miningRevenueUsd.formatFractional()
                )
                fieldRight48.text = asset.metricsDomain.miningStatsDomain.miningRevenueNative.formatFractional()

                miningCard.setOnClickListener {
                    arrowToggle(miningViews.showOrHide(), miningLabelToggle)
                }

            }

            allTimeSection.apply {

                fieldRight49.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.allTimeHighDomain.price.formatFractional()
                )
                fieldRight50.text = asset.metricsDomain.allTimeHighDomain.formattedDate
                fieldRight51.text = getString(
                    R.string.days,
                    asset.metricsDomain.allTimeHighDomain.daysSince.toString()
                )
                fieldRight52.text = getString(
                    R.string.percent,
                    getString(
                        R.string.arrow_down,
                        asset.metricsDomain.allTimeHighDomain.percentDown.formatTwoDecimal(true)
                    )
                )

                fieldRight53.text = asset.metricsDomain.allTimeHighDomain.breakEvenMultiple.formatFractional()

                fieldRight54.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.cycleLowDomain.price.formatFractional()
                )
                fieldRight55.text = asset.metricsDomain.cycleLowDomain.formattedDate
                fieldRight56.text = getString(R.string.days, asset.metricsDomain.cycleLowDomain.daysSince.toString())
                fieldRight57.text = getString(
                    R.string.percent,
                    getString(
                        R.string.arrow_up,
                        asset.metricsDomain.cycleLowDomain.percentUp.formatTwoDecimal(true)
                    )
                )

                fieldRight52.setTextColor(Color.RED)
                fieldRight57.setTextColor(Color.GREEN)

                allTimeCard.setOnClickListener {
                    arrowToggle(allTimeViews.showOrHide(), allTimeLabelToggle)
                }

            }

            shortRoiSection.apply {

                fieldRight58.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeLast1Week.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight58,
                    asset.metricsDomain.roiDataDomain.percentChangeLast1Week
                )
                fieldRight59.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeLast1Month.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight59,
                    asset.metricsDomain.roiDataDomain.percentChangeLast1Month
                )
                fieldRight60.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeLast3Months.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight60,
                    asset.metricsDomain.roiDataDomain.percentChangeLast3Months
                )
                fieldRight61.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeLast1Year.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight61,
                    asset.metricsDomain.roiDataDomain.percentChangeLast1Year
                )
                fieldRight62.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeBtcLast1Week.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight62,
                    asset.metricsDomain.roiDataDomain.percentChangeBtcLast1Week
                )
                fieldRight63.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeBtcLast1Month.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight63,
                    asset.metricsDomain.roiDataDomain.percentChangeBtcLast1Month
                )
                fieldRight64.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeBtcLast3Months.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight64,
                    asset.metricsDomain.roiDataDomain.percentChangeBtcLast3Months
                )
                fieldRight65.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeBtcLast1Year.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight65,
                    asset.metricsDomain.roiDataDomain.percentChangeBtcLast1Year
                )
                fieldRight66.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeEthLast1Week.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight66,
                    asset.metricsDomain.roiDataDomain.percentChangeEthLast1Week
                )
                fieldRight67.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeEthLast1Month.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight67,
                    asset.metricsDomain.roiDataDomain.percentChangeEthLast1Month
                )
                fieldRight68.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeEthLast3Months.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight68,
                    asset.metricsDomain.roiDataDomain.percentChangeEthLast3Months
                )
                fieldRight69.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeEthLast1Year.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight69,
                    asset.metricsDomain.roiDataDomain.percentChangeEthLast1Year
                )
                fieldRight70.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeMonthToDate.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight70,
                    asset.metricsDomain.roiDataDomain.percentChangeMonthToDate
                )
                fieldRight71.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeQuarterToDate.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight71,
                    asset.metricsDomain.roiDataDomain.percentChangeQuarterToDate
                )
                fieldRight72.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiDataDomain.percentChangeYearToDate.formatFractional()
                )
                AppUtils.displayPercentChange(
                    fieldRight72,
                    asset.metricsDomain.roiDataDomain.percentChangeYearToDate
                )

                shortRoiCard.setOnClickListener {
                    arrowToggle(shortRoiViews.showOrHide(), shortRoiLabelToggle)
                }

            }

            roiSection.apply {

                roiYear1.text = getString(R.string.year_roi, "2020")
                roiYear2.text = getString(R.string.year_roi, "2019")
                roiYear3.text = getString(R.string.year_roi, "2018")
                roiYear4.text = getString(R.string.year_roi, "2017")
                roiYear5.text = getString(R.string.year_roi, "2016")
                roiYear6.text = getString(R.string.year_roi, "2015")
                roiYear7.text = getString(R.string.year_roi, "2014")
                roiYear8.text = getString(R.string.year_roi, "2013")
                roiYear9.text = getString(R.string.year_roi, "2012")
                roiYear10.text = getString(R.string.year_roi, "2011")

                roiValue1.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2020.formatFractional()
                )
                roiValue2.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2019.formatFractional()
                )
                roiValue3.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2018.formatFractional()
                )
                roiValue4.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2017.formatFractional()
                )
                roiValue5.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2016.formatFractional()
                )
                roiValue6.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2015.formatFractional()
                )
                roiValue7.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2014.formatFractional()
                )
                roiValue8.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2013.formatFractional()
                )
                roiValue9.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2012.formatFractional()
                )
                roiValue10.text = getString(
                    R.string.percent,
                    asset.metricsDomain.roiByYearDomain.usdPercent2011.formatFractional()
                )

                roiCard.setOnClickListener {
                    arrowToggle(roiViews.showOrHide(), roiLabelToggle)
                }

            }

            riskSection.apply {

                fieldRight73.text =
                    asset.metricsDomain.riskMetricsDomain.volatilityStatsDomain.volatilityLast30Days.formatFractional()
                fieldRight74.text =
                    asset.metricsDomain.riskMetricsDomain.volatilityStatsDomain.volatilityLast90Days.formatFractional()
                fieldRight75.text =
                    asset.metricsDomain.riskMetricsDomain.volatilityStatsDomain.volatilityLast1Year.formatFractional()
                fieldRight76.text =
                    asset.metricsDomain.riskMetricsDomain.volatilityStatsDomain.volatilityLast3Years.formatFractional()
                fieldRight77.text =
                    asset.metricsDomain.riskMetricsDomain.sharpeRatiosDomain.last30Days.formatFractional()
                fieldRight78.text =
                    asset.metricsDomain.riskMetricsDomain.sharpeRatiosDomain.last90Days.formatFractional()
                fieldRight79.text =
                    asset.metricsDomain.riskMetricsDomain.sharpeRatiosDomain.last1Year.formatFractional()
                fieldRight80.text =
                    asset.metricsDomain.riskMetricsDomain.sharpeRatiosDomain.last3Years.formatFractional()

                riskCard.setOnClickListener {
                    arrowToggle(riskViews.showOrHide(), riskLabelToggle)
                }

            }

            socialSection.apply {

                asset.metricsDomain.redditDomain.subscribers?.let {
                    fieldRight81.text = String.format(
                        Locale.getDefault(),
                        "%,d", it
                    )
                }

                asset.metricsDomain.redditDomain.activeUserCount?.let {
                    fieldRight82.text = String.format(
                        Locale.getDefault(),
                        "%,d", it
                    )
                }

            }

            miscSection.apply {

                fieldRight83.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.miscDataDomain.btcCurrentNormalizedSupplyPriceUsd.formatFractional()
                )
                fieldRight84.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.miscDataDomain.btcY2050NormalizedSupplyPriceUsd.formatFractional()
                )
                fieldRight85.text = asset.metricsDomain.miscDataDomain.vladimirClubCost.formatFractional()

            }

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {
        /* no-op */
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is MetricsListener) {
            listener = context
        }

    }

    override fun onBackPressed() {
        if (scrollView.canScrollUp()) {
            scrollView.smoothScrollTo(0, 0)
            listener.expandAppBarLayout()
        } else {
            super.onBackPressed()
        }
    }

    private fun arrowToggle(isVisible: Int, textView: MaterialTextView) {

        textView.setCompoundDrawablesWithIntrinsicBounds(
            null,
            null,
            if (isVisible == View.VISIBLE) {
                context?.resToDrawable(R.drawable.ic_arrow_up)
            } else {
                context?.resToDrawable(R.drawable.ic_arrow_down)
            },
            null
        )

    }

    interface MetricsListener {
        
        fun getAsset(): MessariAssetDataDomain
        fun expandAppBarLayout()
        
    }
    
}