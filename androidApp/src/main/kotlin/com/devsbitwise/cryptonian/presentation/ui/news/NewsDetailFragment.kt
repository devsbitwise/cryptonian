/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.news

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.admob.utils.AdMobUtils
import com.devsbitwise.android.common.extensions.getHtmlSpanned
import com.devsbitwise.android.common.extensions.getParcelable
import com.devsbitwise.android.common.extensions.getParcelableArray
import com.devsbitwise.android.common.extensions.openExternalIntent
import com.devsbitwise.android.common.extensions.resToColor
import com.devsbitwise.android.common.extensions.resToDrawable
import com.devsbitwise.android.common.extensions.showToast
import com.devsbitwise.android.common.extensions.strippedHtmlScriptTags
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentNewsDetailBinding
import com.devsbitwise.cryptonian.presentation.NewsDetailActivity
import com.devsbitwise.cryptonian.presentation.adapters.news.NewsRelatedAdapter
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.wordpress.domain.models.WordPressPostDataDomain
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.LoadAdError
import com.google.firebase.Firebase
import com.google.firebase.remoteconfig.remoteConfig
import org.koin.android.ext.android.inject
import org.sufficientlysecure.htmltextview.ClickableTableSpan
import org.sufficientlysecure.htmltextview.DrawTableLinkSpan
import org.sufficientlysecure.htmltextview.HtmlImageGetter

class NewsDetailFragment : BaseFragment<FragmentNewsDetailBinding, ViewModel>() {

    private lateinit var adView: AdView
    private lateinit var adsContainer: FrameLayout
    private lateinit var recyclerView: RecyclerView

    private lateinit var adapter: NewsRelatedAdapter

    private lateinit var globalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener

    private val glide by inject<RequestManager>()

    override val interceptBackPress: Boolean = false

    // No usage
    override val viewModel: ViewModel? = null

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentNewsDetailBinding =
        FragmentNewsDetailBinding::inflate

    override fun initViews(binding: FragmentNewsDetailBinding) {

        binding.apply {

            toolbar.setNavigationOnClickListener {
                onBackPressed()
            }

            val intentDomain = getParcelable<WordPressPostDataDomain>(NewsDetailActivity.ARG_NEWS)
                ?: requireActivity().getParcelable<WordPressPostDataDomain>(NewsDetailActivity.ARG_NEWS)!!

            val intentDomainList = getParcelableArray<WordPressPostDataDomain>(NewsDetailActivity.ARG_NEWS_LIST)
                ?: requireActivity().getParcelableArray<WordPressPostDataDomain>(NewsDetailActivity.ARG_NEWS_LIST)!!

            glide
                .load(intentDomain.featuredMediaUrl)
                .placeholder(context?.resToDrawable(R.drawable.logo_dynamic_placeholder))
                .centerCrop()
                .into(featuredImage)

            recyclerView = feedRecycler
            adsContainer = adFrame

            adapter = NewsRelatedAdapter(glide) { position ->

                val domain = adapter.currentList[position]

                val domainList = adapter.currentList.toMutableList().also {
                    // Removing this item from the list before navigating to next destination
                    it.removeAt(position)
                }.toList()

                findNavController().navigate(
                    R.id.action_news_detail_self,
                    bundleOf(
                        NewsDetailActivity.ARG_NEWS to domain,
                        NewsDetailActivity.ARG_NEWS_LIST to domainList.toTypedArray()
                    )
                )

            }.apply {
                setHasStableIds(true)
            }

            feedRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            feedRecycler.setHasFixedSize(true)
            feedRecycler.adapter = adapter

            collapsingToolbar.title = intentDomain.titleDomain.rendered.getHtmlSpanned()

            val tableLinkSpan = DrawTableLinkSpan().apply {
                textColor = requireContext().resToColor(R.color.mdc_colorAccentDark_Accent)
                tableLinkText = getString(R.string.table)
            }

            descriptionHtmlTxtV.apply {

                // The HTML table(s) are individually passed through to the ClickableTableSpan implementation
                // presumably for a WebView Activity.
                setClickableTableSpan(ClickableTableSpanImpl(intentDomain))

                setDrawTableLinkSpan(tableLinkSpan)

                // Best to use indentation that matches screen density.
                val metrics = resources.displayMetrics

                setListIndentPx(metrics.density * 10)

                setOnClickATagListener { _, _, href ->
                    openInWebView(href)
                    true
                }

                setOnClickImageTagListener { _, src ->
                    openInWebView(src)
                    true
                }

                setHtml(
                    intentDomain.contentDomain.rendered.strippedHtmlScriptTags(),
                    HtmlImageGetter(lifecycleScope, resources, this, R.drawable.error_no_connection)
                )

            }

            visitWeb.setOnClickListener { openInWebView(intentDomain.link) }

            labelTxt.text = getString(R.string.related_content, getString(R.string.news))

            if (intentDomainList.isEmpty()) {
                labelTxt.visibility = View.GONE
                return
            }

            adapter.submitList(intentDomainList.toList())

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        adView = AdMobUtils.initAndLoadBannerAd(
            requireActivity(),
            Firebase.remoteConfig.getString(AppConfig.BANNER_UNIT),
            adsContainer,
            object : AdListener() {

                override fun onAdLoaded() {

                    val previousPaddingHorizontal = recyclerView.paddingStart

                    // Get the correct and updated dimension of banner ad after being fully loaded
                    globalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
                        // Add padding to avoid banner ad overlapping RecyclerView
                        recyclerView.setPadding(
                            previousPaddingHorizontal,
                            0,
                            previousPaddingHorizontal,
                            adsContainer.height
                        )
                        // Remove this listener to avoid memory leak
                        adView.viewTreeObserver.removeOnGlobalLayoutListener(globalLayoutListener)
                    }

                    adView.viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)

                }

                override fun onAdFailedToLoad(e: LoadAdError) {
                    Log.i("BANNER AD", e.message)
                }

            }
        )

    }

    override fun onPause() {
        if (::adView.isInitialized) {
            adView.pause()
        }
        super.onPause()
    }

    override fun onResume() {
        if (::adView.isInitialized) {
            adView.resume()
        }
        super.onResume()
    }

    override fun onDestroyView() {
        if (::adView.isInitialized) {
            adView.destroy()
        }
        super.onDestroyView()
    }

    private fun openInWebView(href: String?) {

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(href))

        context?.openExternalIntent(intent)?.let { e ->
            e.localizedMessage?.let { msg -> context?.showToast(msg, Toast.LENGTH_LONG) }
        }

    }

    inner class ClickableTableSpanImpl(private val intentDomain: WordPressPostDataDomain) : ClickableTableSpan() {

        override fun newInstance(): ClickableTableSpan {
            // Do not convert this to anonymous object with `return this` as it will not create new instance
            // so in case where multiple table tags exist, only the last table tag will be clickable.
            return ClickableTableSpanImpl(intentDomain)
        }

        override fun onClick(widget: View) {
            findNavController().navigate(
                R.id.action_news_detail_to_news_detail_table,
                bundleOf(
                    NewsDetailActivity.ARG_TITLE to intentDomain.titleDomain.rendered,
                    NewsDetailActivity.ARG_DATE to intentDomain.formattedDate,
                    NewsDetailActivity.ARG_TABLE_HTML to tableHtml
                )
            )
        }

    }

}