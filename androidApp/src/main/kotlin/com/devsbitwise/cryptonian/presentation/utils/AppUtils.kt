/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.utils

import android.graphics.Color
import com.devsbitwise.android.common.extensions.formatTwoDecimal
import com.devsbitwise.android.common.extensions.resToColor
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.presentation.custom_views.AppChartMarker
import com.github.mikephil.charting.charts.CandleStickChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.google.android.material.textview.MaterialTextView
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.format
import kotlinx.datetime.format.MonthNames
import kotlinx.datetime.format.char
import kotlinx.datetime.toLocalDateTime

/**
 * Project specific utility class
 * */
object AppUtils {

    /**
     * Display percentage value with arrow up or down symbol and color representing the movement
     * */
    fun displayPercentChange(textView: MaterialTextView, number: Number?) {

        var nonNullNumber = number

        if (nonNullNumber == null) {
            nonNullNumber = 0
        }

        with(textView.context) {

            when (nonNullNumber.toDouble().compareTo(0.00)) {

                -1 -> {
                    textView.setTextColor(Color.RED)
                    textView.text = getString(
                        R.string.percent,
                        getString(
                            R.string.arrow_down,
                            nonNullNumber.formatTwoDecimal(false)
                        )
                    )
                }

                0 -> {
                    textView.setTextColor(Color.GRAY)
                    textView.text = getString(
                        R.string.percent,
                        getString(
                            R.string.arrow_up_down,
                            nonNullNumber.formatTwoDecimal(false)
                        )
                    )
                }

                1 -> {
                    textView.setTextColor(Color.GREEN)
                    textView.text = getString(
                        R.string.percent,
                        getString(
                            R.string.arrow_up,
                            nonNullNumber.formatTwoDecimal(false)
                        )
                    )
                }

            }

        }

    }

    /**
     * Apply style on LineChart
     * */
    fun styleLineChart(chart: LineChart) {

        with(chart.context) {

            /*START OF GRAPH STYLE*/

            // Enable scaling and dragging
            chart.isDragEnabled = true
            chart.setScaleEnabled(true)

            val desc = Description()
            desc.text = getString(R.string.realized_marketcap)
            desc.textSize = resources.getInteger(R.integer.lineChartLabelSize).toFloat()
            desc.textColor = resToColor(R.color.mdc_colorAccent)

            chart.description = desc

            // Force pinch zoom along both axis
            chart.setPinchZoom(true)

            // Enable and show the marker when a point is selected
            chart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                override fun onValueSelected(e: Entry, h: Highlight) {
                    chart.setDrawMarkers(true)
                }

                override fun onNothingSelected() {
                    /* no-op */
                }
            })

            // X-Axis Style
            chart.xAxis.apply {
                granularity = 1f // Minimum axis-step (interval) is 1
                labelRotationAngle = 315f
                textSize = resources.getInteger(R.integer.lineChartAxisSize).toFloat()
                setDrawGridLines(false)
                setDrawAxisLine(true)
                setAvoidFirstLastClipping(true)
                position = XAxis.XAxisPosition.BOTTOM

                textColor = resToColor(R.color.mdc_colorPrimaryDark_White)
            }

            // Y-Axis Style (Right)
            chart.axisRight.apply {
                granularity = 1f // Minimum axis-step (interval) is 1
                textSize = resources.getInteger(R.integer.lineChartAxisSize).toFloat()
                setDrawGridLines(true)
                textColor = resToColor(R.color.mdc_colorPrimaryDark_White)
            }

            // Y-Axis Style (Left)
            chart.axisLeft.apply {
                isEnabled = false
                setDrawGridLines(false)
                setDrawAxisLine(false)
            }

            // Legend Style per LineDataSet
            chart.legend.apply {
                form = Legend.LegendForm.CIRCLE
                textSize = resources.getInteger(R.integer.lineChartLabelSize).toFloat()
                textColor = resToColor(R.color.mdc_colorPrimaryDark_White)
                verticalAlignment = Legend.LegendVerticalAlignment.TOP
                horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
                orientation = Legend.LegendOrientation.HORIZONTAL
                isWordWrapEnabled = true
                setDrawInside(false)
            }

            // Create a custom MarkerView (extend MarkerView) and specify the layout to use for it
            val markerView = AppChartMarker(this, R.layout.graph_markerview)
            markerView.chartView = chart // For bounds control

            chart.marker = markerView // Set the marker to the chart

            chart.extraBottomOffset = resources.getInteger(R.integer.chartBottomSpace).toFloat()

            /*END OF GRAPH STYLE*/

        }

    }

    /**
     * Apply style on CandleStickChart
     * */
    fun styleCandleStickChart(chart: CandleStickChart) {

        with(chart.context) {

            /*START OF GRAPH STYLE*/

            chart.setDrawBorders(true)
            chart.setBorderColor(Color.DKGRAY)

            chart.description.isEnabled = false

            // If more than 60 entries are displayed in the chart, no values will be drawn
            // chart.setMaxVisibleValueCount(60)

            // Scaling can now only be done on x- and y-axis separately
            chart.setPinchZoom(false)

            // Enable and show the marker when a point is selected
            chart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                override fun onValueSelected(e: Entry, h: Highlight) {
                    chart.setDrawMarkers(true)
                }

                override fun onNothingSelected() {
                    /* no-op */
                }
            })

            // X-Axis Style
            chart.xAxis.apply {
                granularity = 1f // Minimum axis-step (interval) is 1
                labelRotationAngle = 315f
                textSize = resources.getInteger(R.integer.candleStickChartAxisSize).toFloat()
                setDrawGridLines(true)
                setDrawAxisLine(true)
                setAvoidFirstLastClipping(true)
                position = XAxis.XAxisPosition.BOTTOM

                textColor = resToColor(R.color.mdc_colorPrimaryDark_White)
            }

            // Y-Axis Style (Right)
            chart.axisRight.apply {
                granularity = 1f // Minimum axis-step (interval) is 1
                textSize = resources.getInteger(R.integer.candleStickChartAxisSize).toFloat()
                setDrawGridLines(true)
                textColor = resToColor(R.color.mdc_colorPrimaryDark_White)
            }

            // Y-Axis Style (Left)
            chart.axisLeft.apply {
                isEnabled = false
                setDrawGridLines(false)
                setDrawAxisLine(false)
            }

            chart.legend.isEnabled = false

            // Create a custom MarkerView (extend MarkerView) and specify the layout to use for it
            val markerView = AppChartMarker(this, R.layout.graph_markerview)
            markerView.chartView = chart // For bounds control

            chart.marker = markerView // Set the marker to the chart

            chart.extraBottomOffset = resources.getInteger(R.integer.chartBottomSpace).toFloat()

            /*END OF GRAPH STYLE*/

        }

    }

    // Epoch = (numberOfDay * 24 * 60 * 60 * 1000)
    // 24 represents the number of hours in a day
    // 60 represents the number of minutes in an hour
    // 60 represents the number of seconds in a minute
    // 1000 represents the number of milliseconds in a second
    private const val SCALE_TIMESTAMP = 86400000f

    /**
     *  Return a scaled down version of the timestamp.
     *
     *  We do this since the whole Epoch milliseconds can be too large for float
     *  which is the chosen data type for entries by MPAndroidChart, causing it to loss precision and get weird behavior.
     *  Reference: https://github.com/PhilJay/MPAndroidChart/issues/4282
     * */
    fun getScaledDownTimeStamp(timestamp: Long): Float {
        return timestamp.div(SCALE_TIMESTAMP)
    }

    private fun getScaledUpTimeStamp(timestamp: Float): Long {
        return timestamp.times(SCALE_TIMESTAMP).toLong()
    }

    fun getMarkerFormattedDate(time: Float): String {
        return Instant.fromEpochMilliseconds(getScaledUpTimeStamp(time))
            .toLocalDateTime(TimeZone.currentSystemDefault())
            .format(
                LocalDateTime.Format { // MM-dd-yyyy
                    monthNumber()
                    char('-')
                    dayOfMonth()
                    char('-')
                    year()
                }
            )
    }

    fun getXAxisFormattedDate(time: Float): String {
        return Instant.fromEpochMilliseconds(getScaledUpTimeStamp(time))
            .toLocalDateTime(TimeZone.currentSystemDefault())
            .format(
                LocalDateTime.Format { // dd MMM
                    dayOfMonth()
                    char(' ')
                    monthName(MonthNames.ENGLISH_ABBREVIATED)
                }
            )
    }

}