/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.adapters.crypto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.formatFractional
import com.devsbitwise.android.common.presentation.ui.adapters.FilterableListAdapter
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.AssetMinCardBinding
import com.devsbitwise.cryptonian.presentation.utils.diffutils.DiffUtilAssetMin
import com.devsbitwise.messari.domain.models.MessariAssetMinDataDomain
import com.google.android.material.card.MaterialCardView

class AssetMinAdapter(
    private val iconLink: String,
    private val glide: RequestManager,
    private val itemListener: ItemListener
) : FilterableListAdapter<MessariAssetMinDataDomain, AssetMinAdapter.ItemView>(DiffUtilAssetMin()) {

    inner class ItemView(itemView: AssetMinCardBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val cardView = itemView.cardRoot
        private val assetName = itemView.assetName
        private val assetSymbol = itemView.assetSymbol
        private val assetPrice = itemView.assetPrice
        private val assetIcon = itemView.assetIcon

        // Full update/binding
        fun bindFull(domain: MessariAssetMinDataDomain) {

            with(itemView.context) {

                bindTextData(
                    domain.name,
                    domain.symbol,
                    domain.metricsMinDomain.marketDataMinDomain.priceUsd,
                    domain.isSelected
                )

                glide
                    .load(
                        getString(
                            R.string.icon_url,
                            iconLink,
                            domain.id
                        )
                    )
                    .placeholder(R.mipmap.ic_launcher_round)
                    .circleCrop()
                    .into(assetIcon)

                cardView.setOnClickListener {

                    // It is advisable to always use interface when working with `View.setOnClickListener`
                    // in adapter to avoid outdated binding and data reference.
                    // Passing position and using `adapter.currentList[position]` in the Activity/Fragment
                    // is better than passing object via `getItem(position)` from here.
                    // Reference: https://stackoverflow.com/q/77308368/12204620
                    itemListener.onItemSelected(bindingAdapterPosition, cardView)

                }

            }

        }

        // Partial update/binding
        fun bindPartial(domain: MessariAssetMinDataDomain, bundle: Bundle) {
            bindTextData(
                domain.name,
                domain.symbol,
                bundle.getDouble(DiffUtilAssetMin.ARG_MARKET_PRICE),
                bundle.getBoolean(DiffUtilAssetMin.ARG_IS_SELECTED)
            )
        }

        private fun bindTextData(name: String, symbol: String?, price: Double?, isChecked: Boolean) {

            with(itemView.context) {
                assetName.text = name
                assetSymbol.text = symbol ?: getString(R.string.empty)
                assetPrice.apply {
                    text = getString(R.string.us_dollars, price.formatFractional())
                    isSelected = true
                }
                cardView.isChecked = isChecked
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView =
        ItemView(AssetMinCardBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        // Reference: https://forums.raywenderlich.com/t/speed-up-your-android-recyclerview-using-diffutil-raywenderlich-com/141338/8?u=archeremiya
        onBindViewHolder(holder, holder.bindingAdapterPosition, emptyList())
    }

    override fun onBindViewHolder(holder: ItemView, position: Int, payloads: List<Any>) {

        with(holder) {

            // For most cases `getBindingAdapterPosition()` is use as it provides the most up-to-date position considering pending changes.
            // It is also ideal for getting data from `getCurrentList()` as it returns the only position under this specific adapter and
            // not the entire adapters of a ConcatAdapter.
            // Reference: https://stackoverflow.com/a/63148812
            val domain = getItem(bindingAdapterPosition)

            if (payloads.isEmpty() || payloads.first() !is Bundle) {
                bindFull(domain) // Full update/binding
            }
            else {
                val bundle = payloads.first() as Bundle
                bindPartial(domain, bundle) // Partial update/binding
            }

        }

    }

    // Required when setHasStableIds is set to true
    override fun getItemId(position: Int): Long {
        return currentList[position].id.hashCode().toLong()
    }

    override fun onFilter(
        list: List<MessariAssetMinDataDomain>,
        constraint: String
    ): List<MessariAssetMinDataDomain> {

        return list.filter {
            it.name.lowercase().contains(constraint.lowercase()) ||
            it.symbol?.lowercase()?.contains(constraint.lowercase()) == true ||
            it.isSelected
        }

    }

    // Since adapter.currentList() does not immediately reflecting the actual update from filters
    // we can use this callback instead to listen and get the latest list.
    override fun onCurrentListChanged(
        previousList: List<MessariAssetMinDataDomain>,
        currentList: List<MessariAssetMinDataDomain>
    ) {
        super.onCurrentListChanged(previousList, currentList)
        itemListener.onListUpdate(previousList, currentList)
    }

    interface ItemListener {

        fun onItemSelected(position: Int, cardView: MaterialCardView)
        fun onListUpdate(
            previousList: List<MessariAssetMinDataDomain>,
            currentList: List<MessariAssetMinDataDomain>
        )

    }

}