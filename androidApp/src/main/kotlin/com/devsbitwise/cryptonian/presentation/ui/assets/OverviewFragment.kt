/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.assets

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.widget.NestedScrollView
import com.devsbitwise.android.common.extensions.canScrollUp
import com.devsbitwise.android.common.extensions.collectFlow
import com.devsbitwise.android.common.extensions.formatFractional
import com.devsbitwise.android.common.extensions.formatWithUnit
import com.devsbitwise.android.common.extensions.resToColor
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_STRING
import com.devsbitwise.android.material.extensions.materialDatePicker
import com.devsbitwise.android.material.extensions.safeShow
import com.devsbitwise.android.material.extensions.snackBar
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentOverviewBinding
import com.devsbitwise.cryptonian.presentation.utils.AppUtils
import com.devsbitwise.messari.domain.models.MessariAssetDataDomain
import com.devsbitwise.messari.domain.models.MessariPriceDataDomain
import com.devsbitwise.messari.presentation.events.price.MessariPriceEvent
import com.devsbitwise.messari.presentation.states.price.MessariPriceState
import com.devsbitwise.messari.presentation.viewmodels.MessariPriceViewModel
import com.github.mikephil.charting.charts.CandleStickChart
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.CandleData
import com.github.mikephil.charting.data.CandleDataSet
import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.google.android.material.chip.Chip
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textview.MaterialTextView
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.format
import kotlinx.datetime.toLocalDateTime
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named


class OverviewFragment : BaseFragment<FragmentOverviewBinding, MessariPriceViewModel>() {

    companion object {
        fun newInstance() = OverviewFragment()
    }

    private lateinit var assetId: String

    private lateinit var chart: CandleStickChart

    private lateinit var scrollView: NestedScrollView
    private lateinit var logTxtV: MaterialTextView
    private lateinit var progressBar: ProgressBar
    private lateinit var fromDateChip: Chip
    private lateinit var toDateChip: Chip

    private lateinit var datePickerFrom: MaterialDatePicker<Long>
    private lateinit var datePickerTo: MaterialDatePicker<Long>

    private lateinit var listener: OverviewListener

    private val dateFormatter = LocalDateTime.Format { date(LocalDate.Formats.ISO) }

    private val fromDateConstraints by inject<Pair<CalendarConstraints, Long>>(qualifier = named("FromMaterialDatePickerConstraints"))

    private val toDateConstraints by inject<Pair<CalendarConstraints, Long>>(qualifier = named("ToMaterialDatePickerConstraints"))

    override var interceptBackPress: Boolean = true

    override val viewModel by viewModel<MessariPriceViewModel>()

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentOverviewBinding =
        FragmentOverviewBinding::inflate

    override fun initViews(binding: FragmentOverviewBinding) {

        binding.apply {

            with(requireContext()) {

                val asset = listener.getAsset()
                assetId = asset.id.trim()

                val card1 = stat1
                val card2 = stat2

                logTxtV = errorLog
                chart = candleChart
                progressBar = progressChart
                scrollView = parentScrollView

                toDateChip = toDate
                fromDateChip = fromDate

                // Apply styling to CandleStickChart
                AppUtils.styleCandleStickChart(chart)

                datePickerFrom = materialDatePicker(calendarConstraints = fromDateConstraints.first, fromDateConstraints.second).apply {

                    fromDateChip.text = selection?.let { Instant.fromEpochMilliseconds(it).toLocalDateTime(TimeZone.currentSystemDefault()).format(dateFormatter) }

                    addOnPositiveButtonClickListener {

                        fromDateChip.text = Instant.fromEpochMilliseconds(it).toLocalDateTime(TimeZone.currentSystemDefault()).format(dateFormatter)

                        fetchData()

                    }

                }

                datePickerTo = materialDatePicker(calendarConstraints = toDateConstraints.first, toDateConstraints.second).apply {

                    toDateChip.text = selection?.let { Instant.fromEpochMilliseconds(it).toLocalDateTime(TimeZone.currentSystemDefault()).format(dateFormatter) }

                    addOnPositiveButtonClickListener {

                        toDateChip.text = Instant.fromEpochMilliseconds(it).toLocalDateTime(TimeZone.currentSystemDefault()).format(dateFormatter)

                        fetchData()

                    }

                }

                fromDateChip.setOnClickListener {
                    datePickerFrom.safeShow(parentFragmentManager, null)
                }

                toDateChip.setOnClickListener {
                    datePickerTo.safeShow(parentFragmentManager, null)
                }

                card1.labelTxt.text = getString(R.string.key_metrics)
                card2.labelTxt.text = getString(R.string.price_metrics)

                card1.fieldLeft1.setText(R.string.price)
                card1.fieldRight1.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.marketDataDomain.priceUsd.formatFractional()
                )

                card1.fieldLeft2.setText(R.string.change_24h)
                AppUtils.displayPercentChange(
                    card1.fieldRight2,
                    asset.metricsDomain.marketDataDomain.percentChangeUsdLast24Hours
                )

                card1.fieldLeft3.setText(R.string.real_vol)
                card1.fieldRight3.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDataDomain.realVolumeLast24Hours.formatFractional()
                    )
                    isSelected = true
                }

                card1.fieldLeft4.setText(R.string.liquid_marketcap)
                card1.fieldRight4.apply {
                    text = getString(
                        R.string.us_dollars,
                        asset.metricsDomain.marketDomain.liquidMarketcapUsd.formatFractional()
                    )
                    isSelected = true
                }

                card1.fieldLeft5.setText(R.string.liquid_supply)
                card1.fieldRight5.text =
                    asset.metricsDomain.supplyDomain.liquid.formatFractional()

                card1.fieldLeft6.setText(R.string.max_supply)
                card1.fieldRight6.text =
                    asset.profileDomain.economicsDomain.consensusAndEmissionDomain.supply.maxSupply.formatFractional()

                card2.fieldLeft1.setText(R.string.low_high_1)
                card2.fieldRight1.text = getString(
                    R.string.ohlc,
                    asset.metricsDomain.marketDataDomain.ohlcvLast1HourDomain?.low.formatFractional(),
                    asset.metricsDomain.marketDataDomain.ohlcvLast1HourDomain?.high.formatFractional()
                )

                card2.fieldLeft2.setText(R.string.low_high_24)
                card2.fieldRight2.text = getString(
                    R.string.ohlc,
                    asset.metricsDomain.marketDataDomain.ohlcvLast24HourDomain?.low.formatFractional(),
                    asset.metricsDomain.marketDataDomain.ohlcvLast24HourDomain?.high.formatFractional()
                )

                card2.fieldLeft3.setText(R.string.ath_usd)
                card2.fieldRight3.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.allTimeHighDomain.price.formatFractional()
                )

                card2.fieldLeft4.setText(R.string.ath_date)

                asset.metricsDomain.allTimeHighDomain.at?.let {
                    card2.fieldRight4.text = asset.metricsDomain.allTimeHighDomain.formattedDate
                }

                card2.fieldLeft5.setText(R.string.cycle_low_usd)
                card2.fieldRight5.text = getString(
                    R.string.us_dollars,
                    asset.metricsDomain.cycleLowDomain.price.formatFractional()
                )

                card2.fieldLeft6.setText(R.string.cycle_low_date)

                asset.metricsDomain.cycleLowDomain.at?.let {
                    card2.fieldRight6.text = asset.metricsDomain.cycleLowDomain.formattedDate
                }

            }

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        with(viewModel) {

            viewLifecycleOwner.collectFlow(messariPriceState, ::onPriceStateChanged)

            // Recover the the UI state during configuration change.
            // We cannot use the onSavedInstanceState to save and recover the UI loading state because if system-initiated process death occur
            // the UI will be in loading state forever. We only want UI states to survive a configuration change using ViewModel.
            if (isFetchOngoing) {
                chart.visibility = View.INVISIBLE
                logTxtV.text = null
                logTxtV.visibility = View.INVISIBLE
                progressBar.visibility = View.VISIBLE
            }

            if (priceList.isEmpty()) {

                // Prevent repetitive initial fetch which will cause duplicate items.
                // This could happen when the response is slow and user keep switching between this and other fragments.
                if (isFetchOngoing) {
                    return
                }

                // Initial fetch
                fetchData()

            }
            else {
                progressBar.visibility = View.GONE
                displayGraph(getCandleStickEntries(priceList))
            }

        }

        logTxtV.setOnClickListener {
            if (chart.data == null || chart.data.dataSets.isEmpty()) {
                // Retry if fetch failed
                fetchData()
            }
        }

    }

    private fun onPriceStateChanged(state: MessariPriceState) {

        when (state) {

            is MessariPriceState.FetchLoading -> {

                chart.visibility = View.INVISIBLE
                logTxtV.text = null
                logTxtV.visibility = View.INVISIBLE
                progressBar.visibility = View.VISIBLE

                chart.apply {
                    // Disable showing marker when invalidating the data,
                    // this avoid NPE as the data might no longer be available
                    setDrawMarkers(false)
                    resetTracking()
                    clear()
                }

                // To avoid the visual look of flickering on candlestick graph
                // when success state (remote) or failed state (local) returns immediately after this state,
                // we only supply cache data during failed state

            }

            is MessariPriceState.FetchSuccess -> {

                progressBar.visibility = View.GONE

                // Since the availability of data is based on a given date range,
                // it can be empty but such scenario is valid for this case
                if (state.priceList.isEmpty()) {
                    logTxtV.setText(R.string.no_data)
                    logTxtV.visibility = View.VISIBLE
                    return
                }

                displayGraph(getCandleStickEntries(state.priceList))

            }

            is MessariPriceState.FetchFailed -> {

                progressBar.visibility = View.GONE

                if (state.priceList.isNullOrEmpty()) {
                    logTxtV.setText(R.string.tap_to_retry)
                    logTxtV.visibility = View.VISIBLE
                    return
                }
                else {
                    activity?.snackBar(getString(R.string.cache_loaded), parent = view)?.show()
                }

                displayGraph(getCandleStickEntries(state.priceList ?: emptyList())) // Cache

            }

        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is OverviewListener) {
            listener = context
        }

    }

    private fun fetchData() {

        viewModel.onTriggerEvent(
            MessariPriceEvent.FetchPrices(
                assetId,
                fromDateChip.text.toString(),
                toDateChip.text.toString()
            )
        )

    }

    private fun getCandleStickEntries(priceList: List<MessariPriceDataDomain>): List<CandleEntry> {

        val candleEntries = mutableListOf<CandleEntry>()

        for (dataDomain in priceList) {
            candleEntries.add(
                // Sadly we may lose precision here due to usage of float
                // which is the chosen data type for entries by MPAndroidChart
                CandleEntry(
                    AppUtils.getScaledDownTimeStamp(dataDomain.timestamp),
                    dataDomain.high.toFloat(),
                    dataDomain.low.toFloat(),
                    dataDomain.open.toFloat(),
                    dataDomain.close.toFloat(),
                    dataDomain.volume
                )
            )
        }

        return candleEntries.toList()

    }

    private fun displayGraph(candleEntryList: List<CandleEntry>) {

        chart.visibility = View.VISIBLE

        with(requireContext()) {

            // Always set and plot the axis first before populating the chart data
            // to avoid axis label display issue and data inconsistency that can cause exception
            chart.xAxis.valueFormatter = object : ValueFormatter() {

                override fun getFormattedValue(value: Float): String {
                    return try {
                        AppUtils.getXAxisFormattedDate(value)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        DEFAULT_VALUE_STRING
                    }
                }

            }

            // Always set and plot the axis first before populating the chart data
            // to avoid axis label display issue and data inconsistency that can cause exception
            chart.axisRight.valueFormatter = object : ValueFormatter() {

                override fun getFormattedValue(value: Float): String {
                    return try {
                        getString(R.string.us_dollars, value.formatWithUnit())
                    } catch (e: Exception) {
                        e.printStackTrace()
                        DEFAULT_VALUE_STRING
                    }
                }

            }

            val candleDataSet = CandleDataSet(candleEntryList, getString(R.string.stock_price))

            candleDataSet.setDrawValues(false) //Display values on each point
            candleDataSet.setDrawIcons(false)
            candleDataSet.axisDependency = YAxis.AxisDependency.LEFT
            candleDataSet.shadowColor = resToColor(R.color.mdc_colorPrimaryDark_White)
            candleDataSet.shadowWidth = 0.7f
            candleDataSet.decreasingColor = Color.RED
            candleDataSet.decreasingPaintStyle = Paint.Style.FILL
            candleDataSet.increasingColor = Color.rgb(122, 242, 84)
            candleDataSet.increasingPaintStyle = Paint.Style.FILL
            candleDataSet.neutralColor = Color.BLUE
            candleDataSet.highLightColor = Color.rgb(244, 117, 117)

            val data = CandleData(candleDataSet)

            chart.data = data
            chart.notifyDataSetChanged()

            chart.fitScreen()

            chart.animateX(1500)

        }

    }

    override fun onPause() {
        if (datePickerFrom.isAdded) {
            datePickerFrom.dismiss()
        }
        if (datePickerTo.isAdded) {
            datePickerTo.dismiss()
        }
        super.onPause()
    }

    override fun onBackPressed() {
        if (scrollView.canScrollUp()) {
            scrollView.smoothScrollTo(0, 0)
            listener.expandAppBarLayout()
        } else {
            super.onBackPressed()
        }
    }

    interface OverviewListener {

        fun getAsset(): MessariAssetDataDomain
        fun expandAppBarLayout()

    }

}