/*
 * Copyright (c) 2023 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("UNUSED_PARAMETER")

package com.devsbitwise.cryptonian.presentation.custom_views

import android.content.Context
import android.util.AttributeSet
import com.devsbitwise.android.common.extensions.formatFractional
import com.devsbitwise.android.firebase.extensions.recordException
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.presentation.utils.AppUtils
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.google.android.material.textview.MaterialTextView

class AppChartMarker : MarkerView {

    private val textView: MaterialTextView

    constructor(context: Context, attrs: AttributeSet?, layoutResource: Int) : super(context, layoutResource) {
        textView = findViewById(R.id.tvContent)
    }

    constructor(context: Context, layoutResource: Int) : super(context, layoutResource) {
        textView = findViewById(R.id.tvContent)
    }

    /**
     * @param entry has property named data which is an Object
     * Runs every time the MarkerView is redrawn, can be used to update the content
     * */
    override fun refreshContent(entry: Entry, highlight: Highlight) {

        try {

            var content: String

            when (entry) {

                is CandleEntry -> {

                    content = "Date: ${AppUtils.getMarkerFormattedDate(entry.x)}"

                    content += "\nClose: ${context.getString(
                        R.string.us_dollars,
                        entry.close.formatFractional()
                    )}"

                    content += "\nOpen: ${
                        context.getString(
                            R.string.us_dollars,
                            entry.open.formatFractional()
                        )
                    }"

                    content += "\nHigh: ${
                        context.getString(
                            R.string.us_dollars,
                            entry.high.formatFractional()
                        )
                    }"

                    content += "\nLow: ${
                        context.getString(
                            R.string.us_dollars,
                            entry.low.formatFractional()
                        )
                    }"

                    content += "\nVolume: ${
                        context.getString(
                            R.string.us_dollars,
                            (entry.data as Double).formatFractional()
                        )
                    }"

                }

                else -> {

                    val extraData = entry.data as Pair<*, *>
                    content = extraData.first as String
                    content += "\nDate: ${AppUtils.getMarkerFormattedDate(entry.x)}"
                    content += "\nRealized: ${
                        context.getString(
                            R.string.us_dollars,
                            (extraData.second as Double).formatFractional()
                        )
                    }"

                }

            }

            textView.text = content

        }
        catch (e: Exception) {
            e.printStackTrace()
            e.recordException()
        }

        super.refreshContent(entry, highlight)

    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height - 10).toFloat())
    }

}