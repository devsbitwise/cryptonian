/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.viewmodels.shared

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsbitwise.android.common.domain.RequestStatus
import com.devsbitwise.android.common.domain.use_cases.SaveBitmapUseCase
import com.devsbitwise.android.common.presentation.states.BitmapToFileState
import com.devsbitwise.cryptonian.presentation.events.activity.MainActivityEvent
import com.devsbitwise.cryptonian.presentation.states.activity.MainActivityState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class MainSharedViewModel(private val saveBitmapUseCase: SaveBitmapUseCase) : ViewModel() {

    private val _bitmapToFileState = Channel<BitmapToFileState>()

    val bitmapToFileState = _bitmapToFileState.receiveAsFlow()

    private val _mainActivityState = MutableSharedFlow<MainActivityState>()

    val mainActivityState = _mainActivityState.asSharedFlow()

    fun onTriggerEvent(events: MainActivityEvent) {

        viewModelScope.launch {

            when (events) {
                is MainActivityEvent.UpdateAssetFilter -> { setAssetFilter(events.assetNameSymbol) }
                is MainActivityEvent.UpdateAssetLimit -> { setAssetLimit(events.limit) }
                is MainActivityEvent.SaveBitmap -> {
                    storeBitmap(
                        events.bitmap,
                        events.folderName,
                        events.description
                    )
                }
            }

        }

    }

    private suspend fun setAssetFilter(assetName: String) {

        _mainActivityState.emit(MainActivityState.AssetFilterChanged(assetName))

    }

    private suspend fun setAssetLimit(limit: Int) {

        _mainActivityState.emit(MainActivityState.AssetLimitChanged(limit))

    }

    private suspend fun storeBitmap(bitmap: Bitmap, folderName: String, description: String) {

        saveBitmapUseCase(bitmap, folderName).onEach {

            when (it) {

                is RequestStatus.Loading -> {
                    _bitmapToFileState.send(BitmapToFileState.FetchLoading(it.data))
                }

                is RequestStatus.Success -> {
                    _bitmapToFileState.send(BitmapToFileState.FetchSuccess(it.data, description))
                }

                is RequestStatus.Canceled -> {
                    /* no-op */
                }

                is RequestStatus.Failed -> {
                    _bitmapToFileState.send(BitmapToFileState.FetchFailed(it.message, it.data))
                }

            }

        }.collect()

    }

}