/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.utils.diffutils

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.devsbitwise.messari.domain.models.MessariExchangeDataDomain

class DiffUtilExchange : DiffUtil.ItemCallback<MessariExchangeDataDomain>() {

    // DiffUtil uses this test to help discover if an item was added, removed, or moved.
    // Use attribute(s) that represent object's identity.
    override fun areItemsTheSame(oldItem: MessariExchangeDataDomain, newItem: MessariExchangeDataDomain): Boolean {
        return oldItem.id == newItem.id
    }

    // Check whether oldItem and newItem contain the same data; that is, whether they are equal.
    // If there are differences between oldItem and newItem, this code tells DiffUtil that the item has been updated.
    // Note: If you are using data class and trying to detect changes based on properties outside primary constructor,
    // you may need to do additional checking since the default generated `equals` only uses properties inside primary constructor.
    override fun areContentsTheSame(oldItem: MessariExchangeDataDomain, newItem: MessariExchangeDataDomain): Boolean {
        // Checking all the data for changes
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: MessariExchangeDataDomain, newItem: MessariExchangeDataDomain): Any? {

        if (oldItem.id == newItem.id) {
            return if (oldItem.priceUsd == newItem.priceUsd &&
                oldItem.volumeLast24Hours == newItem.volumeLast24Hours &&
                oldItem.deviationFromVwapPercent == newItem.deviationFromVwapPercent) {
                super.getChangePayload(oldItem, newItem)
            } else {
                // Add object's attribute(s) that has changed using this payload
                Bundle().apply {
                    newItem.let { exchange ->
                        putDouble(ARG_EXCHANGE_PRICE, exchange.priceUsd ?: 0.0)
                        putDouble(ARG_EXCHANGE_VOLUME, exchange.volumeLast24Hours ?: 0.0)
                        putDouble(ARG_EXCHANGE_VWAP, exchange.deviationFromVwapPercent ?: 0.0)
                    }
                }
            }
        }

        return super.getChangePayload(oldItem, newItem)

    }

    companion object {
        const val ARG_EXCHANGE_PRICE = "arg.exchange.price"
        const val ARG_EXCHANGE_VOLUME = "arg.exchange.volume"
        const val ARG_EXCHANGE_VWAP = "arg.exchange.vwap"
    }

}