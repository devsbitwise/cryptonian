/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.adapters.videos

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_STRING
import com.devsbitwise.cryptonian.databinding.VideosChannelsChipBinding
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.google.android.material.chip.Chip

class VideosChannelsAdapter(
    private val channelList: List<AppConfig.Remote.VideoChannel>,
    private val itemListener: ItemListener,
    private val selectedChannelId: String = DEFAULT_VALUE_STRING
) : RecyclerView.Adapter<VideosChannelsAdapter.ItemView>() {

    inner class ItemView(itemView: VideosChannelsChipBinding) : RecyclerView.ViewHolder(itemView.root) {

        private val chipGroup = itemView.chipGroupGeneric

        init {
            with(itemView.root.context) {

                // Create Chip programmatically
                for ((index, data) in channelList.withIndex()) {

                    val chip = Chip(this).apply {

                        id = index
                        isCheckable = true

                        // Set the channel id on this View tag
                        tag = data.id

                        text = data.name

                        // First chip will be set check at initial render
                        if (index == 0 && selectedChannelId.isBlank()) {
                            isChecked = true
                        } else if (selectedChannelId == tag.toString()) {
                            // Recover last selected chip
                            isChecked = true
                        }

                    }

                    chipGroup.addView(chip)

                }

                chipGroup.setOnCheckedStateChangeListener { group, _ ->
                    val channelId = group.findViewById<Chip>(group.checkedChipId).tag.toString()
                    itemListener.onChipSelected(channelId)
                }

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView {
        return ItemView(VideosChannelsChipBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        /* no-op */
    }

    override fun getItemCount(): Int = 1

    fun interface ItemListener {

        fun onChipSelected(channelId: String)

    }

}