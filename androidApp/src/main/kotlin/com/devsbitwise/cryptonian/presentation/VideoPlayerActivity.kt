/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation

import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.ViewModel
import com.devsbitwise.android.common.extensions.fullScreenNoSystemBars
import com.devsbitwise.android.common.extensions.getParcelable
import com.devsbitwise.android.common.extensions.keepScreenOn
import com.devsbitwise.android.common.presentation.ui.activity.BaseActivity
import com.devsbitwise.android.material.extensions.snackBar
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.ActivityVideoPlayerBinding
import com.devsbitwise.youtube.domain.models.YouTubeVideoDataDomain
import com.google.android.material.snackbar.Snackbar
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener


class VideoPlayerActivity : BaseActivity<ActivityVideoPlayerBinding, ViewModel>() {

    companion object {
        const val ARG_VIDEO = "ARG_VIDEO"
    }

    override val viewModel: ViewModel? = null

    override fun bindingInflater(): (LayoutInflater) -> ActivityVideoPlayerBinding =
        ActivityVideoPlayerBinding::inflate

    override fun initViews(binding: ActivityVideoPlayerBinding) {

        window.apply {
            keepScreenOn()
            fullScreenNoSystemBars()
        }

        val snackBar = snackBar(
            getString(R.string.unknown_error),
            getString(R.string.navigation_close),
            { onBackPressedDispatcher.onBackPressed() },
            Snackbar.LENGTH_INDEFINITE
        )

        binding.youtubePlayerView.apply {

            lifecycle.addObserver(this)

            addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {

                override fun onReady(youTubePlayer: YouTubePlayer) {
                    super.onReady(youTubePlayer)

                    val videoId = getParcelable<YouTubeVideoDataDomain>(ARG_VIDEO)

                    videoId?.let {
                        youTubePlayer.loadVideo(it.videoId, 0f)
                    } ?: run {
                        snackBar.show()
                    }

                }

                override fun onError(youTubePlayer: YouTubePlayer, error: PlayerConstants.PlayerError) {
                    super.onError(youTubePlayer, error)
                    snackBar.show()
                }

            })

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {
        /* no-op */
    }

}