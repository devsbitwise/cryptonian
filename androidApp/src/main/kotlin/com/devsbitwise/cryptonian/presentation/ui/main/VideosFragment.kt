/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.main

import android.content.Context
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.canScrollDown
import com.devsbitwise.android.common.extensions.canScrollUp
import com.devsbitwise.android.common.extensions.collectFlow
import com.devsbitwise.android.common.extensions.resToColor
import com.devsbitwise.android.common.extensions.smoothScrollWithinCoordinator
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.android.material.extensions.snackBar
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentVideosListBinding
import com.devsbitwise.cryptonian.presentation.VideoPlayerActivity
import com.devsbitwise.cryptonian.presentation.adapters.videos.VideosAdapter
import com.devsbitwise.cryptonian.presentation.adapters.videos.VideosChannelsAdapter
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.kommon.extensions.fromJsonString
import com.devsbitwise.youtube.presentation.events.videos.YouTubeVideosEvent
import com.devsbitwise.youtube.presentation.states.videos.YouTubeVideosState
import com.devsbitwise.youtube.presentation.viewmodels.YouTubeVideosViewModel
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.Firebase
import com.google.firebase.remoteconfig.remoteConfig
import kotlinx.serialization.json.Json
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class VideosFragment : BaseFragment<FragmentVideosListBinding, YouTubeVideosViewModel>() {

    private lateinit var videosAdapter: VideosAdapter

    private lateinit var logTxtV: MaterialTextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private lateinit var channels: List<AppConfig.Remote.VideoChannel>

    private lateinit var listener: VideosListener

    private val json by inject<Json>()

    private val glide by inject<RequestManager>()

    override var interceptBackPress: Boolean = true

    override val viewModel by viewModel<YouTubeVideosViewModel>()

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentVideosListBinding =
        FragmentVideosListBinding::inflate

    override fun initViews(binding: FragmentVideosListBinding) {

        binding.apply {

            channels = Firebase.remoteConfig.getString(AppConfig.VIDEO_CHANNELS)
                .fromJsonString<List<AppConfig.Remote.VideoChannel>>(json) ?: emptyList()

            recyclerView = rvVideos
            logTxtV = errorLog
            swipeRefreshLayout = refreshLayout.apply {
                setColorSchemeColors(context.resToColor(R.color.mdc_colorWhite_PrimaryDark))
            }

            videosAdapter = VideosAdapter(glide, object : VideosAdapter.ItemListener {

                override fun onItemSelected(position: Int) {
                    val domain = videosAdapter.currentList[position]
                    findNavController().navigate(
                        R.id.action_menu_videos_to_video_player_activity,
                        bundleOf(VideoPlayerActivity.ARG_VIDEO to domain)
                    )
                }

                // ListAdapter can be slow when updating its `currentList` after performing `submitList`
                // and we don't want to scroll prematurely if it's initial items,
                // we can listen for updates here to know when it's the right time
                override fun onListUpdate(previousListSize: Int, currentListSize: Int) {
                    // Only start scrolling slightly after initial items are rendered
                    if (previousListSize != 0 && currentListSize != 0) {
                        recyclerView.smoothScrollBy(0, 200)
                    }
                }

            })

            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = ConcatAdapter(
                VideosChannelsAdapter(
                    channels,
                    { channelId -> fetchData(channelId, true) },
                    viewModel.channelId
                ),
                videosAdapter
            )

            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                var lastFetchTimestamp: Long = 0

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                    // Populating RecyclerView with initial items can call `onScrolled`,
                    // we only want to start supporting pagination after initial items are rendered
                    if (recyclerView.canScrollDown().not() && videosAdapter.currentList.isNotEmpty() && refreshLayout.isRefreshing.not()) {
                        // Throttling where `onScrolled` was called repeatedly in a short period of time
                        if (SystemClock.elapsedRealtime() - lastFetchTimestamp < 1000L) {
                            return
                        } else {
                            fetchData(viewModel.channelId, false)
                        }
                        lastFetchTimestamp = SystemClock.elapsedRealtime()
                    }

                }

            })

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        with(viewModel) {

            viewLifecycleOwner.collectFlow(youTubeVideosState, ::onVideosStateChanged)

            // Recover the the UI state during configuration change.
            // We cannot use the onSavedInstanceState to save and recover the UI loading state because if system-initiated process death occur
            // the UI will be in loading state forever. We only want UI states to survive a configuration change using ViewModel.
            swipeRefreshLayout.isRefreshing = isFetchOngoing

            if (youTubeVideoList.isEmpty()) {

                // Prevent repetitive initial fetch which will cause duplicate items.
                // This could happen when the response is slow and user keep switching between this and other fragments.
                if (isFetchOngoing) {
                    return
                }

                // Initial fetch
                fetchData(
                    channelId.ifBlank { channels.first().id },
                    true
                )

            }
            else {
                // Recover data after configuration change
                videosAdapter.submitList(youTubeVideoList)
            }

        }

        swipeRefreshLayout.setOnRefreshListener {
            // Reset or retry if initial fetch failed
            fetchData(viewModel.channelId, true)
        }

    }

    private fun onVideosStateChanged(state: YouTubeVideosState) {

        when (state) {

            is YouTubeVideosState.FetchLoading -> {

                logTxtV.text = null

                swipeRefreshLayout.isRefreshing = true

                // Cache in this scenario is always empty for every page that was never cached before
                // and submitting it to adapter will empty the RecyclerView as
                // DiffUtil will assume all items are removed if empty data was submitted

            }

            is YouTubeVideosState.FetchSuccess -> {

                swipeRefreshLayout.isRefreshing = false

                if (state.videoList.isEmpty()) {
                    // In case the fetch was successful but with empty response
                    // and the adapter was previously empty as well
                    if (videosAdapter.currentList.isEmpty()) {
                        logTxtV.setText(R.string.no_data)
                    }
                    return
                }

                videosAdapter.submitList(state.videoList)

            }

            is YouTubeVideosState.FetchFailed -> {

                swipeRefreshLayout.isRefreshing = false

                if (state.videoList.isNullOrEmpty()) {
                    // We can't rely on `onCurrentListChanged` of the adapter as we have no way of knowing
                    // what state that cause the list to become empty and switching between channels
                    // with both empty response will not trigger it after `submitList`
                    if (videosAdapter.currentList.isEmpty()) {
                        // In case the fetch failed and has no cache
                        logTxtV.setText(R.string.swipe_to_refresh)
                    }
                    else {
                        activity?.snackBar(getString(R.string.no_data), parent = view)?.show()
                    }
                    return
                }
                else {
                    activity?.snackBar(getString(R.string.cache_loaded), parent = view)?.show()
                }

                videosAdapter.submitList(state.videoList) // Cache

            }

        }

    }

    private fun fetchData(channelId: String, isInitial: Boolean) {

        if (isInitial) {
            viewModel.onTriggerEvent(YouTubeVideosEvent.ClearYouTubeVideosList)
            // Passing `emptyList()` is slower, sometimes causing to skip
            // the callback of `onCurrentListChanged` so we pass null
            videosAdapter.submitList(null)
        }

        viewModel.onTriggerEvent(
            YouTubeVideosEvent.FetchYouTubeVideos(
                Firebase.remoteConfig.getString(AppConfig.VIDEO_KEY),
                channelId,
                viewModel.requestPageToken
            )
        )

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is VideosListener) {
            listener = context
        }

    }

    override fun onBackPressed() {
        if (recyclerView.canScrollUp()) {
            recyclerView.smoothScrollWithinCoordinator(0)
            listener.expandAppBarLayout()
        } else {
            super.onBackPressed()
        }
    }

    fun interface VideosListener {

        fun expandAppBarLayout()

    }

}