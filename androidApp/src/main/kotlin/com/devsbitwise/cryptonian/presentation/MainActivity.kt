/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SearchView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.devsbitwise.android.admob.utils.AdMobUtils
import com.devsbitwise.android.common.domain.repositories.UserPreferences
import com.devsbitwise.android.common.extensions.activityForResultIntentSender
import com.devsbitwise.android.common.extensions.checkUpdates
import com.devsbitwise.android.common.extensions.hasNotificationPermission
import com.devsbitwise.android.common.extensions.openExternalIntent
import com.devsbitwise.android.common.extensions.requestNotificationPermission
import com.devsbitwise.android.common.extensions.showToast
import com.devsbitwise.android.common.presentation.ui.activity.BaseActivity
import com.devsbitwise.android.common.utils.DAY_NIGHT_THEME
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_STRING
import com.devsbitwise.android.firebase.extensions.recordException
import com.devsbitwise.android.material.extensions.materialDialog
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.ActivityMainBinding
import com.devsbitwise.cryptonian.presentation.events.activity.MainActivityEvent
import com.devsbitwise.cryptonian.presentation.ui.main.AssetsFragment
import com.devsbitwise.cryptonian.presentation.ui.main.MarketCapFragment
import com.devsbitwise.cryptonian.presentation.ui.main.NewsFragment
import com.devsbitwise.cryptonian.presentation.ui.main.VideosFragment
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.cryptonian.presentation.viewmodels.shared.MainSharedViewModel
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.InstallStatus
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity<ActivityMainBinding, MainSharedViewModel>(),
    AssetsFragment.AssetListener,
    NewsFragment.NewsListener,
    VideosFragment.VideosListener,
    MarketCapFragment.MarketCapListener,
    NavController.OnDestinationChangedListener {

    private var isAdFetched = false
    private var isAppUpdateOngoing = false

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    private lateinit var searchView: SearchView
    private lateinit var toolBar: MaterialToolbar
    private lateinit var appBarLayout: AppBarLayout
    private var drawerLayoutView: DrawerLayout? = null

    private lateinit var assetLimitDialog: AlertDialog
    private lateinit var inAppUpdateDialog: AlertDialog

    private val permissionResultLauncher = getPermission()

    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            // For drawers that is open from right, use GravityCompat.END
            if (drawerLayoutView?.isDrawerOpen(GravityCompat.START) == true) {
                drawerLayoutView?.closeDrawer(GravityCompat.START)
            }
            else {
                remove()
                finish()
            }
        }
    }

    private val installStateUpdatedListener = InstallStateUpdatedListener { state ->

        when (state.installStatus()) {

            InstallStatus.DOWNLOADED -> {

                if (isDestroyed.not() && isFinishing.not()) {
                    inAppUpdateDialog.show()
                }

            }

            InstallStatus.INSTALLED -> {
                unregisterInAppUpdateListener()
            }

            InstallStatus.CANCELED, InstallStatus.FAILED, InstallStatus.UNKNOWN -> {
                recordException("In-App Update Failed Status Code: ${state.installErrorCode()}")
                unregisterInAppUpdateListener()
            }

            InstallStatus.DOWNLOADING,
            InstallStatus.INSTALLING,
            InstallStatus.PENDING -> {
                /* no-op */
            }

            else -> {
                recordException("In-App Update Unknown Status Code: ${state.installErrorCode()}")
                unregisterInAppUpdateListener()
            }

        }

    }

    private lateinit var inAppUpdateResultLauncher: ActivityResultLauncher<IntentSenderRequest>

    private val appUpdateManager by inject<AppUpdateManager>()

    private val userPreferences by inject<UserPreferences>()

    override val viewModel by viewModel<MainSharedViewModel>()

    override fun bindingInflater(): (LayoutInflater) -> ActivityMainBinding =
        ActivityMainBinding::inflate

    override fun initViews(binding: ActivityMainBinding) {

        // In case where Activity was recreated, reinitialize
        inAppUpdateResultLauncher = activityForResultIntentSender { result, _ ->

            when (result.resultCode) {
                // In flexible flow, the user accepted the request to update.
                // In immediate flow, the user accepted and the update succeeded (which, in practice, your app never should never receive because it already updated).
                Activity.RESULT_OK -> isAppUpdateOngoing = true

                // In flexible flow, the user denied the request to update.
                // In immediate flow, the user denied or canceled the update.
                Activity.RESULT_CANCELED -> unregisterInAppUpdateListener()

                // In flexible flow, something failed during the request for user confirmation. For example, the user terminates the app before responding to the request.
                // In immediate flow, the flow failed either during the user confirmation, the download, or the installation.
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {
                    recordException("In-App Update Unknown Failure")
                    showToast(getString(R.string.unknown_error))
                }
            }

        }

        binding.apply {

            toolBar = toolbar
            appBarLayout = appBar
            setSupportActionBar(toolBar)

            drawerLayoutView = drawerLayout

            val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_content_main) as NavHostFragment

            navController = navHostFragment.navController
            navController.addOnDestinationChangedListener(this@MainActivity)

            // For Tablet devices (Portrait or Landscape)
            navView?.let {

                // Passing each menu ID as a set of Ids because each menu should be considered as top level destinations.
                appBarConfiguration = AppBarConfiguration(
                    setOf(R.id.menu_asset, R.id.menu_marketcap, R.id.menu_news, R.id.menu_videos),
                    drawerLayoutView
                )

                // Attach NavController to this NavigationView
                it.setupWithNavController(navController)

            }

            // For Mobile devices (Landscape)
            navRail?.let {

                // Passing each menu ID as a set of Ids because each menu should be considered as top level destinations.
                appBarConfiguration = AppBarConfiguration(
                    setOf(R.id.menu_asset, R.id.menu_marketcap, R.id.menu_news, R.id.menu_videos)
                )

                // Attach NavController to this NavigationRailView
                it.setupWithNavController(navController)

            }

            // For Mobile devices (Portrait)
            bottomNavView?.let {

                // Passing each menu ID as a set of Ids because each menu should be considered as top level destinations.
                appBarConfiguration = AppBarConfiguration(
                    setOf(R.id.menu_asset, R.id.menu_marketcap, R.id.menu_news, R.id.menu_videos)
                )

                // Attach NavController to this BottomNavigationView
                it.setupWithNavController(navController)

            }

            // Automatically update ActionBar/Toolbar title
            setupActionBarWithNavController(navController, appBarConfiguration)

            assetLimitDialog = materialDialog(
                getString(R.string.asset_limit_title),
                null, // When using single choice item
                null,
                null,
                getString(R.string.navigation_close),
                { dialog, _ ->
                    // Respond to neutral button press
                    dialog.dismiss()
                },
                false

            )   // Single-choice items (initialized with checked item)
                .setSingleChoiceItems(
                    resources.getStringArray(R.array.asset_limit),
                    userPreferences.prefInt(getString(R.string.asset_limit_key))
                ) { dialog, index ->
                    // Respond to selected item by updating asset limit
                    userPreferences.prefInt(getString(R.string.asset_limit_key), index)
                    viewModel.onTriggerEvent(MainActivityEvent.UpdateAssetLimit(index))
                    dialog.dismiss()
                }.create()

            inAppUpdateDialog = materialDialog(
                getString(R.string.update_success),
                null,
                getString(R.string.install_update),
                { dialog, _ ->
                    appUpdateManager.completeUpdate() // Install updates
                    dialog.dismiss()
                },
                null,
                null,
                false
            ).create()

        }

        if (hasNotificationPermission(this).not()) {
            permissionResultLauncher.requestNotificationPermission()
        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        // Do not pass LifecycleOwner here since this Activity will take over the control immediately
        // when in onResume thus overlap its Fragments back press control, this Activity is now responsible
        // for removing its own OnBackPressedCallback
        onBackPressedDispatcher.addCallback(backPressedCallback)

        // Recover the the UI state during configuration change or system-initiated process death
        isAdFetched = savedInstanceState?.getBoolean(AppConfig.IS_AD_FETCHED, false) == true
        isAppUpdateOngoing = savedInstanceState?.getBoolean(AppConfig.IS_APP_UPDATE_ONGOING, false) == true

        if (isAdFetched.not()) {

            isAdFetched = true

            AdMobUtils.initAndLoadInterstitialAd(
                this,
                Firebase.remoteConfig.getString(AppConfig.INTERSTITIAL_UNIT),
                object : InterstitialAdLoadCallback() {
                    override fun onAdLoaded(interstitialAd: InterstitialAd) {

                        interstitialAd.fullScreenContentCallback = object : FullScreenContentCallback() {
                            override fun onAdShowedFullScreenContent() {
                                Log.i("INTERSTITIAL AD", "The ad was shown.")
                            }
                        }

                        interstitialAd.show(this@MainActivity)

                    }

                    override fun onAdFailedToLoad(e: LoadAdError) {
                        Log.i("INTERSTITIAL AD", e.message)
                    }
                }
            )

        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        // Store the UI state that can survive even a System-initiated system-initiated process death
        outState.putBoolean(AppConfig.IS_AD_FETCHED, isAdFetched)
        outState.putBoolean(AppConfig.IS_APP_UPDATE_ONGOING, isAppUpdateOngoing)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_main_overflow, menu)

        val isAppNight = AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES

        val systemNightMode = (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK)

        val isDeviceNight = systemNightMode == Configuration.UI_MODE_NIGHT_YES

        // Set CheckBox state based on Day/Night mode
        val themeOption = menu.findItem(R.id.action_theme)
        themeOption.isChecked = isAppNight || isDeviceNight

        val searchIcon = menu.findItem(R.id.action_search)
        searchIcon.isVisible = false
        searchIcon.isEnabled = false

        searchView = (searchIcon.actionView as SearchView).apply {

            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS

            // Associate searchable configuration with the SearchView
            val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
            setSearchableInfo(searchManager.getSearchableInfo(componentName))

            queryHint = getString(R.string.search_hint)

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {

                override fun onQueryTextSubmit(query: String): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    viewModel.onTriggerEvent(MainActivityEvent.UpdateAssetFilter(newText))
                    return false
                }

            })

        }

        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            R.id.action_filter -> {
                if (searchView.isIconified) {
                    assetLimitDialog.show()
                }
                else {
                    showToast(getString(R.string.require_search_close), Toast.LENGTH_LONG)
                }
            }

            R.id.action_privacy -> openInWebView(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy_policy_url))))

            R.id.action_terms -> openInWebView(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.terms_and_conditions_url))))

            // Update Day/Night mode status
            R.id.action_theme -> {

                // To avoid the inconsistency with Splash Screen API on some older version of Android
                // follow the device Dark Theme settings starting Android 10 (Q) and above
                // Reference: https://issuetracker.google.com/issues/240533989
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val intent = Intent(Settings.ACTION_DISPLAY_SETTINGS)
                    openInWebView(intent)
                }
                else {

                    item.isChecked = !item.isChecked

                    val theme = when (item.isChecked) {
                        true -> AppCompatDelegate.MODE_NIGHT_YES
                        else -> AppCompatDelegate.MODE_NIGHT_NO
                    }

                    AppCompatDelegate.setDefaultNightMode(theme) // For Android 12 (S) and above use UiModeManager instead of AppCompatDelegate

                    // Save settings configuration in SharedPreferences
                    userPreferences.prefInt(DAY_NIGHT_THEME, theme)

                }

            }

        }

        return super.onOptionsItemSelected(item)

    }

    override fun onSupportNavigateUp(): Boolean {
        return navigateUp(navController, appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onStart() {
        super.onStart()
        if (isAppUpdateOngoing.not()) {
            appUpdateManager.checkUpdates(inAppUpdateResultLauncher, installStateUpdatedListener)
        }
    }

    override fun onDestroy() {
        assetLimitDialog.dismiss()
        inAppUpdateDialog.dismiss()
        backPressedCallback.remove()
        super.onDestroy()
    }

    override fun expandAppBarLayout() {
        // Set animate to false due to existing design issue
        // Reference: https://github.com/material-components/material-components-android/issues/3854
        appBarLayout.setExpanded(true, false)
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        toolBar.subtitle = when (destination.id) {
            R.id.menu_asset, R.id.menu_marketcap  -> getString(R.string.powered_messari)
            R.id.menu_news -> getString(R.string.powered_wordpress)
            R.id.menu_videos -> getString(R.string.powered_youtube)
            else -> DEFAULT_VALUE_STRING
        }
    }

    private fun unregisterInAppUpdateListener() {
        appUpdateManager.unregisterListener(installStateUpdatedListener)
    }

    private fun openInWebView(intent: Intent) {
        openExternalIntent(intent)?.let { e ->
            e.localizedMessage?.let { msg -> showToast(msg, Toast.LENGTH_LONG) }
            e.recordException()
        }
    }

}