/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.news

import android.graphics.Color
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import com.devsbitwise.android.common.extensions.getHtmlSpanned
import com.devsbitwise.android.common.extensions.resToColor
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentNewsDetailTableBinding
import com.devsbitwise.cryptonian.presentation.NewsDetailActivity

class NewsDetailTableFragment : BaseFragment<FragmentNewsDetailTableBinding, ViewModel>() {

    override val interceptBackPress: Boolean = false

    // No usage
    override val viewModel: ViewModel? = null

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentNewsDetailTableBinding =
        FragmentNewsDetailTableBinding::inflate

    override fun initViews(binding: FragmentNewsDetailTableBinding) {

        val data = arguments?.getString(NewsDetailActivity.ARG_TABLE_HTML)

        binding.apply {

            toolbar.apply {
                title = (arguments?.getString(NewsDetailActivity.ARG_TITLE) ?: getString(R.string.app_name)).getHtmlSpanned()
                subtitle = arguments?.getString(NewsDetailActivity.ARG_DATE)
                setNavigationOnClickListener {
                    onBackPressed()
                }
            }

            webView.apply {

                val bgColorHex = requireContext().resToColor(R.color.mdc_colorWhite_PrimaryDark)
                val textColorHex = requireContext().resToColor(R.color.mdc_colorPrimaryDark_White)

                setBackgroundColor(bgColorHex)

                val redBg = Color.red(bgColorHex)
                val greenBg = Color.green(bgColorHex)
                val blueBg = Color.blue(bgColorHex)
                val alphaBg = Color.alpha(bgColorHex)

                val redTxt = Color.red(textColorHex)
                val greenTxt = Color.green(textColorHex)
                val blueTxt = Color.blue(textColorHex)
                val alphaTxt = Color.alpha(textColorHex)

                val tableHtml = """
                
                    <style> 
                        table { 
                            font-family: arial, sans-serif; 
                            border-collapse: collapse; 
                            width: 100%; 
                            color: rgba($redTxt, $greenTxt, $blueTxt, $alphaTxt) 
                        }
                        
                        td, th { 
                            border: 1px solid #dddddd; 
                            text-align: left; padding: 8px; 
                        }
                        
                        body { 
                            background-color: rgba($redBg, $greenBg, $blueBg, $alphaBg) 
                        }
                    </style> 
                    
                    <body>
                        $data
                    </body>
                        
                """

                // Recommended way to render HTML
                // Reference: https://stackoverflow.com/a/54614344
                val base64 = Base64.encodeToString(tableHtml.toByteArray(), Base64.DEFAULT)
                loadData(base64, "text/html; charset=UTF-8", "base64")

            }

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {
        /* no-op */
    }

}