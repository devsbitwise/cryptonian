/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.adapters.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.getHtmlSpanned
import com.devsbitwise.android.common.extensions.resToDrawable
import com.devsbitwise.android.common.extensions.strippedHtmlUnsupportedTags
import com.devsbitwise.android.common.presentation.ui.adapters.FilterableListAdapter
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.NewsRelatedItemCardBinding
import com.devsbitwise.cryptonian.presentation.utils.diffutils.DiffUtilNews
import com.devsbitwise.wordpress.domain.models.WordPressPostDataDomain
import com.google.android.material.textview.MaterialTextView

class NewsRelatedAdapter(
    private val glide: RequestManager,
    private val itemListener: ItemListener
) : FilterableListAdapter<WordPressPostDataDomain, NewsRelatedAdapter.ItemView>(DiffUtilNews()) {

    inner class ItemView(itemView: NewsRelatedItemCardBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val titleTxtV: MaterialTextView = itemView.postTitle
        private val dateTxtV: MaterialTextView = itemView.postTime
        private val descriptionTxtV: MaterialTextView = itemView.postDescription
        private val featuredImage: AppCompatImageView = itemView.postImage

        // Full update/binding
        fun bindFull(domain: WordPressPostDataDomain) {

            with(domain) {

                bindTextData(titleDomain.rendered, formattedDate, contentDomain.rendered)

                glide
                    .load(featuredMediaUrl)
                    .placeholder(itemView.context.resToDrawable(R.drawable.logo_dynamic_placeholder))
                    .centerCrop()
                    .into(featuredImage)

                // It is advisable to always use interface when working with `View.setOnClickListener`
                // in adapter to avoid outdated binding and data reference.
                // Passing position and using `adapter.currentList[position]` in the Activity/Fragment
                // is better than passing object via `getItem(position)` from here.
                // Reference: https://stackoverflow.com/q/77308368/12204620
                itemView.setOnClickListener {
                    itemListener.onItemSelected(bindingAdapterPosition)
                }

            }

        }

        // Partial update/binding
        fun bindPartial(bundle: Bundle) {
            bindTextData(
                bundle.getString(DiffUtilNews.ARG_NEWS_TITLE)!!,
                bundle.getString(DiffUtilNews.ARG_NEWS_DATE)!!,
                bundle.getString(DiffUtilNews.ARG_NEWS_CONTENT)!!
            )
        }

        private fun bindTextData(title: String, date: String, description: String) {

            titleTxtV.text = title.getHtmlSpanned()
            dateTxtV.text = date
            descriptionTxtV.text = description.strippedHtmlUnsupportedTags()

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView {
        return ItemView(NewsRelatedItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        // Reference: https://forums.raywenderlich.com/t/speed-up-your-android-recyclerview-using-diffutil-raywenderlich-com/141338/8?u=archeremiya
        onBindViewHolder(holder, holder.bindingAdapterPosition, emptyList())
    }

    override fun onBindViewHolder(holder: ItemView, position: Int, payloads: List<Any>) {

        holder.apply {

            // For most cases `getBindingAdapterPosition()` is use as it provides the most up-to-date position considering pending changes.
            // It is also ideal for getting data from `getCurrentList()` as it returns the only position under this specific adapter and
            // not the entire adapters of a ConcatAdapter.
            // Reference: https://stackoverflow.com/a/63148812
            val domain = getItem(bindingAdapterPosition)

            if (payloads.isEmpty() || payloads.first() !is Bundle) {
                holder.bindFull(domain) // Full update/binding
            }
            else {
                val bundle = payloads.first() as Bundle
                holder.bindPartial(bundle) // Partial update/binding
            }

        }

    }

    // Required when setHasStableIds is set to true
    override fun getItemId(position: Int): Long {
        return currentList[position].id.toLong()
    }

    override fun onFilter(list: List<WordPressPostDataDomain>, constraint: String): List<WordPressPostDataDomain> {
        TODO("Not yet implemented")
    }

    fun interface ItemListener {

        fun onItemSelected(position: Int)

    }

}