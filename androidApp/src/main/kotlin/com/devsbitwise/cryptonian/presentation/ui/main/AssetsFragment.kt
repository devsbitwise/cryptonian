/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.main

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.core.os.bundleOf
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.admob.utils.AdMobUtils
import com.devsbitwise.android.common.domain.repositories.UserPreferences
import com.devsbitwise.android.common.extensions.canScrollUp
import com.devsbitwise.android.common.extensions.collectFlow
import com.devsbitwise.android.common.extensions.collectShared
import com.devsbitwise.android.common.extensions.createBitmapFromView
import com.devsbitwise.android.common.extensions.hasImageStoragePermission
import com.devsbitwise.android.common.extensions.openExternalIntent
import com.devsbitwise.android.common.extensions.openSettingsIntent
import com.devsbitwise.android.common.extensions.requestImageStoragePermission
import com.devsbitwise.android.common.extensions.resToColor
import com.devsbitwise.android.common.extensions.shareImageIntent
import com.devsbitwise.android.common.extensions.showToast
import com.devsbitwise.android.common.extensions.smoothScrollWithinCoordinator
import com.devsbitwise.android.common.presentation.states.BitmapToFileState
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_STRING
import com.devsbitwise.android.firebase.extensions.recordException
import com.devsbitwise.android.material.extensions.snackBar
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentAssetsListBinding
import com.devsbitwise.cryptonian.presentation.AssetDetailActivity
import com.devsbitwise.cryptonian.presentation.adapters.crypto.AssetAdapter
import com.devsbitwise.cryptonian.presentation.events.activity.MainActivityEvent
import com.devsbitwise.cryptonian.presentation.states.activity.MainActivityState
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.cryptonian.presentation.viewmodels.shared.MainSharedViewModel
import com.devsbitwise.messari.domain.models.MessariAssetDataDomain
import com.devsbitwise.messari.presentation.events.assets.MessariAssetEvent
import com.devsbitwise.messari.presentation.states.assets.MessariAssetState
import com.devsbitwise.messari.presentation.viewmodels.MessariAssetViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.LoadAdError
import com.google.android.material.divider.MaterialDividerItemDecoration
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class AssetsFragment : BaseFragment<FragmentAssetsListBinding, MessariAssetViewModel>() {

    private var filterConstraint: String = DEFAULT_VALUE_STRING

    private lateinit var adView: AdView
    private lateinit var adsContainer: FrameLayout
    private lateinit var logTxtV: MaterialTextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private val userPreferredAssetLimit
        get() = resources.getStringArray(R.array.asset_limit)[userPreferences.prefInt(getString(R.string.asset_limit_key))]

    private lateinit var assetAdapter: AssetAdapter

    private lateinit var listener: AssetListener

    private val permissionResultLauncher = getPermission()

    private val mainSharedViewModel by activityViewModel<MainSharedViewModel>()

    private val userPreferences by inject<UserPreferences>()

    private val glide by inject<RequestManager>()

    override var interceptBackPress: Boolean = true

    override val viewModel by viewModel<MessariAssetViewModel>()

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentAssetsListBinding =
        FragmentAssetsListBinding::inflate

    override fun initViews(binding: FragmentAssetsListBinding) {

        binding.apply {

            recyclerView = rvAssets
            adsContainer = adFrame
            logTxtV = errorLog
            swipeRefreshLayout = refreshLayout.apply {
                setColorSchemeColors(context.resToColor(R.color.mdc_colorWhite_PrimaryDark))
            }

            with(requireContext()) {

                assetAdapter = AssetAdapter(
                    Firebase.remoteConfig.getString(AppConfig.CRYPTO_IMAGE_BASE_URL),
                    glide,
                    object : AssetAdapter.ItemListener {

                    override fun onItemSelected(position: Int) {

                        val domain = assetAdapter.currentList[position]

                        findNavController().navigate(
                            R.id.action_menu_asset_to_asset_detail_activity,
                            bundleOf(AssetDetailActivity.ARG_ASSET to domain)
                        )

                    }

                    override fun onRequestScreenShot(view: View, description: String) {
                        saveScreenshot(view, description)
                    }

                    override fun onListUpdate(
                        previousList: List<MessariAssetDataDomain>,
                        currentList: List<MessariAssetDataDomain>
                    ) {

                        if (currentList.isEmpty()) {
                            if (filterConstraint.isNotBlank()) {
                                logTxtV.setText(R.string.no_data)
                            }
                        }
                        else {
                            logTxtV.text = null
                        }

                    }

                }).apply {
                    setHasStableIds(true)
                }

                recyclerView.setHasFixedSize(true)
                recyclerView.adapter = assetAdapter

                recyclerView.addItemDecoration(
                    MaterialDividerItemDecoration(
                        this,
                        DividerItemDecoration.VERTICAL
                    )
                )

            }

        }

        activity?.addMenuProvider(object : MenuProvider {

            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                val searchIcon = menu.findItem(R.id.action_search)
                searchIcon.isVisible = true
                searchIcon.isEnabled = true
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return false // Let the host Activity or Jetpack component handles the event
            }

        }, viewLifecycleOwner, Lifecycle.State.RESUMED)

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        with(mainSharedViewModel) {
            viewLifecycleOwner.collectShared(mainActivityState, ::onMainActivityStateChanged)
            viewLifecycleOwner.collectFlow(bitmapToFileState, ::onBitmapStateChanged)
        }

        with(viewModel) {

            viewLifecycleOwner.collectFlow(messariAssetState, ::onAssetStateChanged)

            // Recover the the UI state during configuration change.
            // We cannot use the onSavedInstanceState to save and recover the UI loading state because if system-initiated process death occur
            // the UI will be in loading state forever. We only want UI states to survive a configuration change using ViewModel.
            swipeRefreshLayout.isRefreshing = isFetchOngoing

            if (assetList.isEmpty()) {

                // Prevent repetitive initial fetch which will cause duplicate items.
                // This could happen when the response is slow and user keep switching between this and other fragments.
                if (isFetchOngoing) {
                    return
                }

                // Initial fetch
                fetchData()

            }
            else {
                // Recover data after configuration change
                assetAdapter.submitList(assetList)
            }

        }

        swipeRefreshLayout.setOnRefreshListener {
            // Reset or retry if initial fetch failed
            fetchData()
        }

        viewLifecycleOwner.lifecycleScope.launch {
            // Auto fetch for every 10 seconds when this Fragment is visible to user
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                while (true) {
                    if (viewModel.assetList.isNotEmpty()) {
                        fetchData()
                    }
                    delay(10_000L)
                }
            }
        }

        adView = AdMobUtils.initAndLoadBannerAd(
            requireActivity(),
            Firebase.remoteConfig.getString(AppConfig.BANNER_UNIT),
            adsContainer,
            object : AdListener() {
                override fun onAdFailedToLoad(e: LoadAdError) {
                    Log.i("BANNER AD", e.message)
                }
            }
        )

    }

    private fun onAssetStateChanged(state: MessariAssetState) {

        when (state) {

            is MessariAssetState.FetchLoading -> {

                // Prevent updating the UI while the user is searching to avoid display conflict,
                // this can happen when a late response arrived but the user is already searching
                filterConstraint.ifBlank {

                    logTxtV.text = null

                    if (assetAdapter.currentList.isEmpty()) {
                        swipeRefreshLayout.isRefreshing = true

                        // One time only since if the user preferred asset limit changed to a much lower count,
                        // the return set of items are different in this state compare to the actual response from API on success state
                        // and that will cause the RecyclerView to show a constant removal of items if submitted in its adapter
                        assetAdapter.submitList(state.assetList) // Cache
                    }

                }

            }

            is MessariAssetState.FetchSuccess -> {

                swipeRefreshLayout.isRefreshing = false

                // Since this is a free limited API, data can be empty
                // DiffUtil will assume items are removed if empty data was submitted
                // This can also happen when reaching the end of page during pagination
                // and yet the API still decided to return an HTTP response code 2xx
                if (state.assetList.isEmpty()) {
                    // In case the fetch was successful but with empty response
                    // and the adapter was previously empty as well
                    if (assetAdapter.currentList.isEmpty()) {
                        logTxtV.setText(R.string.no_data)
                    }
                    return
                }

                // Prevent updating the RecyclerView while the user is searching to avoid display conflict,
                // this can happen when a late response arrived but the user is already searching
                filterConstraint.ifBlank {
                    assetAdapter.submitList(state.assetList)
                }

            }

            is MessariAssetState.FetchFailed -> {

                swipeRefreshLayout.isRefreshing = false

                if (state.assetList.isNullOrEmpty()) {
                    // We can't rely on `onCurrentListChanged` of the adapter as we have no way of knowing
                    // what state that cause the list to become empty
                    if (assetAdapter.currentList.isEmpty()) {
                        // In case the fetch failed and has no cache
                        logTxtV.setText(R.string.swipe_to_refresh)
                        return
                    }
                    activity?.snackBar(getString(R.string.no_data), parent = view)?.show()
                }
                else {
                    activity?.snackBar(getString(R.string.cache_loaded), parent = view)?.show()
                }

                // There is no point of submitting local cache data on this state
                // since it is always just the same data coming from loading state

            }

        }

    }

    private fun onMainActivityStateChanged(state: MainActivityState) {

        when (state) {

            is MainActivityState.AssetFilterChanged -> {
                if (assetAdapter.currentList.isNotEmpty()) {
                    filterConstraint = state.assetNameSymbol
                    assetAdapter.filter.filter(filterConstraint)
                }
            }

            is MainActivityState.AssetLimitChanged -> {
                // Since loading animation only shows when adapter is not yet populated
                // call this manually to notify the user that the number of asset is about to change
                swipeRefreshLayout.isRefreshing = true
                fetchData()
            }

        }

    }

    private fun onBitmapStateChanged(state: BitmapToFileState) {

        with(requireContext()) {

            when (state) {

                is BitmapToFileState.FetchLoading -> {
                    showToast(getString(R.string.saving_image), Toast.LENGTH_SHORT)
                }

                is BitmapToFileState.FetchSuccess -> {
                    openExternalIntent(
                        shareImageIntent(
                            desc = getString(
                                R.string.app_promotion,
                                state.description,
                                getString(R.string.app_promo)
                            ),
                            uri = state.uri
                        ).apply {
                            showToast(getString(R.string.image_saved), Toast.LENGTH_LONG)
                        }
                    )?.let { e ->
                        e.localizedMessage?.let { msg -> showToast(msg, Toast.LENGTH_LONG) }
                        e.recordException()
                    }
                }

                is BitmapToFileState.FetchFailed -> {
                    showToast(getString(R.string.image_failed), Toast.LENGTH_LONG)
                    recordException(state.msg)
                }

            }

        }

    }

    private fun fetchData() {

        viewModel.onTriggerEvent(MessariAssetEvent.FetchAssets(userPreferredAssetLimit))

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is AssetListener) {
            listener = context
        }

    }

    override fun onPause() {
        if (::adView.isInitialized) {
            adView.pause()
        }
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (::adView.isInitialized) {
            adView.resume()
        }
    }

    override fun onDestroyView() {
        if (::adView.isInitialized) {
            adView.destroy()
        }
        super.onDestroyView()
    }

    override fun onBackPressed() {
        if (recyclerView.canScrollUp()) {
            recyclerView.smoothScrollWithinCoordinator(0)
            listener.expandAppBarLayout()
        } else {
            super.onBackPressed()
        }
    }

    override fun onPermissionResult(permissions: Map<String, Boolean>, activity: ComponentActivity) {

        with(activity) {
            // If any from the request permission fails
            if (permissions.containsValue(false)) {

                snackBar(
                    getString(R.string.permission_denied),
                    getString(R.string.settings),
                    {

                        if (isRemoving || isDetached) {
                            return@snackBar
                        }

                        openSettingsIntent()?.let { e ->
                            e.localizedMessage?.also { msg -> showToast(msg, Toast.LENGTH_LONG) }
                            e.recordException()
                        }

                    },
                    Snackbar.LENGTH_LONG,
                    parent = recyclerView
                ).show()

            } else {
                showToast(getString(R.string.permission_granted), Toast.LENGTH_SHORT)
            }
        }

    }

    private fun saveScreenshot(view: View, description: String) {

        with(view.context) {

            if (hasImageStoragePermission(this)) {

                mainSharedViewModel.onTriggerEvent(
                    MainActivityEvent.SaveBitmap(
                        view.createBitmapFromView(R.color.mdc_colorWhite_PrimaryDark),
                        getString(R.string.app_name),
                        description
                    )
                )

            } else {
                permissionResultLauncher.requestImageStoragePermission()
            }

        }

    }

    fun interface AssetListener {

        fun expandAppBarLayout()

    }

}
