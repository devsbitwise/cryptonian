/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.main

import android.content.Context
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.canScrollDown
import com.devsbitwise.android.common.extensions.canScrollUp
import com.devsbitwise.android.common.extensions.collectFlow
import com.devsbitwise.android.common.extensions.resToColor
import com.devsbitwise.android.common.extensions.smoothScrollWithinCoordinator
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.android.material.extensions.snackBar
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentNewsListBinding
import com.devsbitwise.cryptonian.presentation.NewsDetailActivity
import com.devsbitwise.cryptonian.presentation.adapters.news.NewsAdapter
import com.devsbitwise.cryptonian.presentation.adapters.news.NewsCategoriesAdapter
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.kommon.extensions.fromJsonString
import com.devsbitwise.wordpress.presentation.events.posts.WordPressPostsEvent
import com.devsbitwise.wordpress.presentation.states.posts.WordPressPostsState
import com.devsbitwise.wordpress.presentation.viewmodels.WordPressPostsViewModel
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.Firebase
import com.google.firebase.remoteconfig.remoteConfig
import kotlinx.serialization.json.Json
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class NewsFragment : BaseFragment<FragmentNewsListBinding, WordPressPostsViewModel>() {

    private lateinit var newsAdapter: NewsAdapter

    private lateinit var logTxtV: MaterialTextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private lateinit var categories: List<AppConfig.Remote.NewsCategory>

    private lateinit var listener: NewsListener

    private val json by inject<Json>()

    private val glide by inject<RequestManager>()

    override var interceptBackPress: Boolean = true

    override val viewModel by viewModel<WordPressPostsViewModel>()

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentNewsListBinding =
        FragmentNewsListBinding::inflate

    override fun initViews(binding: FragmentNewsListBinding) {

        binding.apply {

            categories = Firebase.remoteConfig.getString(AppConfig.NEWS_CATEGORIES)
                .fromJsonString<List<AppConfig.Remote.NewsCategory>>(json) ?: emptyList()

            recyclerView = rvNews
            logTxtV = errorLog
            swipeRefreshLayout = refreshLayout.apply {
                setColorSchemeColors(context.resToColor(R.color.mdc_colorWhite_PrimaryDark))
            }

            newsAdapter = NewsAdapter(glide, object : NewsAdapter.ItemListener {

                override fun onItemSelected(position: Int) {

                    val domain = newsAdapter.currentList[position]

                    val domainList = newsAdapter.currentList.toMutableList().also {

                        // The number of items to remove starting from first item (index 0)
                        val itemsToRemove = position.plus(1)

                        it.subList(0, itemsToRemove).clear()

                    }.take(5)

                    findNavController().navigate(
                        R.id.action_menu_news_to_news_detail_activity,
                        bundleOf(
                            NewsDetailActivity.ARG_NEWS to domain,
                            NewsDetailActivity.ARG_NEWS_LIST to domainList.toTypedArray()
                        )
                    )

                }

                // ListAdapter can be slow when updating its `currentList` after performing `submitList`
                // and we don't want to scroll prematurely if it's initial items,
                // we can listen for updates here to know when it's the right time
                override fun onListUpdate(previousListSize: Int, currentListSize: Int) {
                    // Only start scrolling slightly after initial items are rendered
                    if (previousListSize != 0 && currentListSize != 0) {
                        recyclerView.smoothScrollBy(0, 200)
                    }
                }

            })

            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = ConcatAdapter(
                NewsCategoriesAdapter(
                    categories,
                    { categoryId -> fetchData(categoryId, true) },
                    viewModel.categoryId
                ),
                newsAdapter
            )

            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                var lastFetchTimestamp: Long = 0

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                    // Populating RecyclerView with initial items can call `onScrolled`,
                    // we only want to start supporting pagination after initial items are rendered
                    if (recyclerView.canScrollDown().not() && newsAdapter.currentList.isNotEmpty() && refreshLayout.isRefreshing.not()) {
                        // Throttling where `onScrolled` was called repeatedly in a short period of time
                        if (SystemClock.elapsedRealtime() - lastFetchTimestamp < 1000L) {
                            return
                        } else {
                            fetchData(viewModel.categoryId, false)
                        }
                        lastFetchTimestamp = SystemClock.elapsedRealtime()
                    }

                }

            })

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        with(viewModel) {

            viewLifecycleOwner.collectFlow(wordPressPostsState, ::onNewsStateChanged)

            // Recover the the UI state during configuration change.
            // We cannot use the onSavedInstanceState to save and recover the UI loading state because if system-initiated process death occur
            // the UI will be in loading state forever. We only want UI states to survive a configuration change using ViewModel.
            swipeRefreshLayout.isRefreshing = isFetchOngoing

            if (wordPressPostList.isEmpty()) {

                // Prevent repetitive initial fetch which will cause duplicate items.
                // This could happen when the response is slow and user keep switching between this and other fragments.
                if (isFetchOngoing) {
                    return
                }

                // Initial fetch
                fetchData(
                    categoryId.ifBlank { categories.first().id.toString() },
                    true
                )

            }
            else {
                // Recover data after configuration change
                newsAdapter.submitList(wordPressPostList)
            }

        }

        swipeRefreshLayout.setOnRefreshListener {
            // Reset or retry if initial fetch failed
            fetchData(viewModel.categoryId, true)
        }

    }

    private fun onNewsStateChanged(state: WordPressPostsState) {

        when (state) {

            is WordPressPostsState.FetchLoading -> {

                logTxtV.text = null

                swipeRefreshLayout.isRefreshing = true

                // Cache in this scenario is always empty for every page that was never cached before
                // and submitting it to adapter will empty the RecyclerView as
                // DiffUtil will assume all items are removed if empty data was submitted

            }

            is WordPressPostsState.FetchSuccess -> {

                swipeRefreshLayout.isRefreshing = false

                if (state.postList.isEmpty()) {
                    // In case the fetch was successful but with empty response
                    // and the adapter was previously empty as well
                    if (newsAdapter.currentList.isEmpty()) {
                        logTxtV.setText(R.string.no_data)
                    }
                    return
                }

                newsAdapter.submitList(state.postList)

            }

            is WordPressPostsState.FetchFailed -> {

                swipeRefreshLayout.isRefreshing = false

                if (state.postList.isNullOrEmpty()) {
                    // We can't rely on `onCurrentListChanged` of the adapter as we have no way of knowing
                    // what state that cause the list to become empty and switching between categories
                    // with both empty response will not trigger it after `submitList`
                    if (newsAdapter.currentList.isEmpty()) {
                        // In case the fetch failed and has no cache
                        logTxtV.setText(R.string.swipe_to_refresh)
                    }
                    else {
                        activity?.snackBar(getString(R.string.no_data), parent = view)?.show()
                    }
                    return
                }
                else {
                    activity?.snackBar(getString(R.string.cache_loaded), parent = view)?.show()
                }

                newsAdapter.submitList(state.postList) // Cache

            }

        }

    }

    private fun fetchData(categoryId: String, isInitial: Boolean) {

        if (isInitial) {
            viewModel.onTriggerEvent(WordPressPostsEvent.ClearWordPressPostsList)
            // Passing `emptyList()` is slower, sometimes causing to skip
            // the callback of `onCurrentListChanged` so we pass null
            newsAdapter.submitList(null)
        }

        viewModel.onTriggerEvent(WordPressPostsEvent.FetchPosts(categoryId, viewModel.requestPageNo))

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is NewsListener) {
            listener = context
        }

    }

    override fun onBackPressed() {
        if (recyclerView.canScrollUp()) {
            recyclerView.smoothScrollWithinCoordinator(0)
            listener.expandAppBarLayout()
        } else {
            super.onBackPressed()
        }
    }

    fun interface NewsListener {

        fun expandAppBarLayout()

    }

}