/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.assets

import android.content.Context
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModel
import com.devsbitwise.admob.databinding.AdItemBinding
import com.devsbitwise.android.admob.utils.AdMobUtils
import com.devsbitwise.android.common.extensions.canScrollUp
import com.devsbitwise.android.common.extensions.getHtmlSpanned
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.android.firebase.extensions.recordException
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentProfileBinding
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.messari.domain.models.MessariAssetDataDomain
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig

// TODO: There is a scrolling behavior issue on NestedScrollView for landscape orientation https://issuetracker.google.com/issues/345493180
class ProfileFragment : BaseFragment<FragmentProfileBinding, ViewModel>() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    private lateinit var listener: ProfileListener

    private lateinit var adLoader: AdLoader
    private lateinit var nativeAd: NativeAd
    private lateinit var nativeAdView: NativeAdView
    private lateinit var adItemBinding: AdItemBinding

    private lateinit var scrollView: NestedScrollView

    override var interceptBackPress: Boolean = true

    // No usage
    override val viewModel: ViewModel? = null

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentProfileBinding =
        FragmentProfileBinding::inflate

    override fun initViews(binding: FragmentProfileBinding) {

        binding.apply {

            adItemBinding = binding.adLayout

            val asset = listener.getAsset()

            labelTxt.text = getString(R.string.about_asset, asset.name)
            scrollView = parentScrollView

            asset.profileDomain.generalDomain.overviewDomain.strippedDetail.let {
                if (it.isNotBlank()) {
                    overviewTxt.text = it.getHtmlSpanned()
                }
            }

            asset.profileDomain.generalDomain.backgroundDomain.strippedDetail.let {
                if (it.isNotBlank()) {
                    historyTxt.text = it.getHtmlSpanned()
                }
            }

            overviewTxt.movementMethod = LinkMovementMethod.getInstance()
            historyTxt.movementMethod = LinkMovementMethod.getInstance()

            nativeAdView = AdMobUtils.bindCustomViewToNativeAdView(
                adItemBinding = adItemBinding,
                headLineColor = R.color.mdc_colorAccent,
                btnColor = R.color.mdc_colorPrimaryDark_Accent,
                mainBgColor = R.color.mdc_colorWhiteDark_Primary,
                contentBgColor = R.color.mdc_colorWhite_PrimaryDark
            )

        }

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        adLoader = AdMobUtils.initAndLoadNativeAd(
            requireActivity(),
            Firebase.remoteConfig.getString(AppConfig.NATIVE_UNIT),
            { ad ->
                nativeAd = ad
                // Avoid memory leak when ads occurs after this screen is no longer valid
                if (isRemoving || isDetached) {
                    nativeAd.destroy()
                }
            }, // Just using Kotlin SAM here
            object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    try {
                        if (adLoader.isLoading.not()) {
                            AdMobUtils.renderNativeAdContents(nativeAdView, nativeAd)
                            adItemBinding.root.visibility = View.VISIBLE
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        e.recordException()
                    }
                }
            }
        )

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is ProfileListener) {
            listener = context
        }

    }

    override fun onDestroyView() {
        if (::nativeAd.isInitialized) {
            nativeAd.destroy()
        }
        if (::nativeAdView.isInitialized) {
            nativeAdView.destroy()
        }
        super.onDestroyView()
    }

    override fun onBackPressed() {
        if (scrollView.canScrollUp()) {
            scrollView.smoothScrollTo(0, 0)
            listener.expandAppBarLayout()
        } else {
            super.onBackPressed()
        }
    }

    interface ProfileListener {

        fun getAsset(): MessariAssetDataDomain
        fun expandAppBarLayout()

    }

}