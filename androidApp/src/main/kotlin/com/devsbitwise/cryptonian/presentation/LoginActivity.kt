/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AlertDialog
import androidx.core.splashscreen.SplashScreen
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.splashscreen.SplashScreenViewProvider
import androidx.lifecycle.ViewModel
import com.devsbitwise.android.common.extensions.activityForResult
import com.devsbitwise.android.common.extensions.fullScreenNoSystemBars
import com.devsbitwise.android.common.extensions.hideKeyboard
import com.devsbitwise.android.common.extensions.keepScreenOn
import com.devsbitwise.android.common.extensions.showToast
import com.devsbitwise.android.common.presentation.ui.activity.BaseActivity
import com.devsbitwise.android.firebase.extensions.recordException
import com.devsbitwise.android.firebase.utils.PhoneAuthUtils
import com.devsbitwise.android.firebase.utils.RemoteConfigUtils
import com.devsbitwise.android.material.extensions.materialDialog
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.ActivitySplashBinding
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.kommon.extensions.fromJsonString
import com.devsbitwise.kommon.extensions.toJsonString
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import kotlinx.serialization.json.Json
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity<ActivitySplashBinding, ViewModel>(),
    RemoteConfigUtils.RemoteConfigCallback, PhoneAuthUtils.PhoneAuthCallback {

    private var appConfig: AppConfig.Remote? = null

    private val json by inject<Json>()

    private lateinit var alertDialog: AlertDialog

    private lateinit var splashScreen: SplashScreen

    private var splashScreenViewProvider: SplashScreenViewProvider? = null

    private lateinit var activityResultLauncher: ActivityResultLauncher<Intent>

    // No usage
    override val viewModel: ViewModel? = null

    override fun bindingInflater(): (LayoutInflater) -> ActivitySplashBinding =
        ActivitySplashBinding::inflate

    override fun installSplash() {

        splashScreen = installSplashScreen().apply {

            // This may not get called due to some bug on Android 12 (S) and 13 (T)
            // Reference: https://issuetracker.google.com/issues/197906327
            setOnExitAnimationListener {

                // Override and do nothing so the splash screen dismissal
                // is now a responsibility of consumer

                splashScreenViewProvider = it

            }

        }

    }

    override fun initViews(binding: ActivitySplashBinding) {

        window.apply {
            keepScreenOn()
            fullScreenNoSystemBars()
        }

        // In case where Activity was recreated, reinitialize
        activityResultLauncher = activityForResult { result, _ ->
            PhoneAuthUtils.processAuthResult(result, this)
        }

        // Initial fetch for remote config.
        RemoteConfigUtils.fetch(remoteConfigCallback = this)

        alertDialog = materialDialog(
            null,
            getString(R.string.no_internet),
            getString(R.string.retry),
            { dialog, _ ->
                dialog.dismiss()
                RemoteConfigUtils.fetch(remoteConfigCallback = this) // Retry fetching remote config.
            },
            getString(R.string.exit),
            { dialog, _ ->
                dialog.dismiss()
                finish()
            },
            false
        ).create()

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {
        /* no-op */
    }

    private fun applyRemoteConfigs(remoteConfig: FirebaseRemoteConfig) {

        // Convert Firebase Remote Config values to String
        val map = remoteConfig.all.mapValues { it.value.asString() }

        appConfig = map.toJsonString(json).fromJsonString<AppConfig.Remote>(json)

    }

    private fun redirectToMainActivity() {
        // Sometimes the keyboard in OTP input field do not dismiss, hide it here
        hideKeyboard()
        splashScreenViewProvider?.remove()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onDestroy() {
        alertDialog.dismiss()
        super.onDestroy()
    }

    override fun onFRCSuccess(remoteConfig: FirebaseRemoteConfig) {
        // Apply the latest configuration
        applyRemoteConfigs(remoteConfig)
    }

    override fun onFRCCanceled() {
        /* no-op */
    }

    override fun onFRCFailure(exception: Exception, remoteConfig: FirebaseRemoteConfig) {
        // Try to apply old configuration if available
        applyRemoteConfigs(remoteConfig)
    }

    override fun onFRCComplete(task: Task<Boolean>) {

        val hasRequiredProperties = appConfig?.requiredProperties?.all { it.isNotBlank() }

        // Show dialog if remote config properties is not properly initialize
        if (hasRequiredProperties?.not() == true) {
            alertDialog.show()
        }
        else {

            // Launch OTP Activity
            if (Firebase.auth.currentUser == null) {
                activityResultLauncher.launch(PhoneAuthUtils.getAuthIntent(R.style.Theme_Cryptonian_FirebaseAuthUI))
            }
            else {
                redirectToMainActivity() // Already logged in
            }

        }

    }

    override fun onFPASuccess(user: FirebaseUser) {
        redirectToMainActivity()
    }

    override fun onFPACanceled() {
        finish()
    }

    override fun onFPAError(fuiError: Int) {
        showToast(getString(fuiError), Toast.LENGTH_LONG)
        recordException(getString(fuiError))
    }

}