/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.adapters.videos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.extensions.getHtmlSpanned
import com.devsbitwise.android.common.extensions.resToDrawable
import com.devsbitwise.android.common.presentation.ui.adapters.FilterableListAdapter
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.presentation.utils.diffutils.DiffUtilVideo
import com.devsbitwise.youtube.domain.models.YouTubeVideoDataDomain
import com.google.android.material.textview.MaterialTextView

class VideosAdapter(
    private val glide: RequestManager,
    private val itemListener: ItemListener
) : FilterableListAdapter<YouTubeVideoDataDomain, VideosAdapter.ItemView>(DiffUtilVideo()) {

    companion object {
        private const val HEADER: Int = 0
        private const val ITEM: Int = 1
    }

    inner class ItemView(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val titleTxtV: MaterialTextView = itemView.findViewById(R.id.postTitle)
        private val dateTxtV: MaterialTextView = itemView.findViewById(R.id.postTime)
        private val featuredImage: AppCompatImageView = itemView.findViewById(R.id.postImage)

        private val relatedVideosTxt: MaterialTextView? = itemView.findViewById(R.id.labelHead)

        // Full update/binding
        fun bindFull(domain: YouTubeVideoDataDomain) {

            with(domain) {

                bindTextData(snippetDomain.title, snippetDomain.formattedDate)

                glide
                    .load(snippetDomain.thumbnailsDomain.fullSizeThumbnail)
                    .placeholder(itemView.context.resToDrawable(R.drawable.logo_dynamic_placeholder))
                    .centerCrop()
                    .into(featuredImage)

                // It is advisable to always use interface when working with `View.setOnClickListener`
                // in adapter to avoid outdated binding and data reference.
                // Passing position and using `adapter.currentList[position]` in the Activity/Fragment
                // is better than passing object via `getItem(position)` from here.
                // Reference: https://stackoverflow.com/q/77308368/12204620
                itemView.setOnClickListener {
                    itemListener.onItemSelected(bindingAdapterPosition)
                }

            }

        }

        // Partial update/binding
        fun bindPartial(bundle: Bundle) {
            bindTextData(
                bundle.getString(DiffUtilVideo.ARG_VIDEO_TITLE)!!,
                bundle.getString(DiffUtilVideo.ARG_VIDEO_DATE)!!
            )
        }

        private fun bindTextData(title: String, date: String) {

            titleTxtV.text = title.getHtmlSpanned()
            dateTxtV.text = date

            if (itemCount > 1) {
                relatedVideosTxt?.text = itemView.context.getString(
                    R.string.related_content, itemView.context.getString(R.string.videos)
                )
            } else {
                relatedVideosTxt?.text = null // No need to show if there is only one item in the list
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView =
        when (viewType) {
            HEADER -> ItemView(
                LayoutInflater.from(parent.context).inflate(R.layout.videos_header_item_card, parent, false)
            )
            else -> ItemView(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.videos_item_card, parent, false)
            )
        }

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        onBindViewHolder(holder, holder.bindingAdapterPosition, emptyList())
    }

    override fun onBindViewHolder(holder: ItemView, position: Int, payloads: List<Any>) {

        with(holder) {

            // For most cases `getBindingAdapterPosition()` is use as it provides the most up-to-date position considering pending changes.
            // It is also ideal for getting data from `getCurrentList()` as it returns the only position under this specific adapter and
            // not the entire adapters of a ConcatAdapter.
            // Reference: https://stackoverflow.com/a/63148812
            val domain = getItem(bindingAdapterPosition)

            if (payloads.isEmpty() || payloads.first() !is Bundle) {
                holder.bindFull(domain) // Full update/binding
            }
            else {
                val bundle = payloads.first() as Bundle
                holder.bindPartial(bundle) // Partial update/binding
            }

        }

    }

    override fun getItemViewType(position: Int): Int = when (position) {
        0 -> HEADER
        else -> ITEM
    }

    override fun onFilter(list: List<YouTubeVideoDataDomain>, constraint: String): List<YouTubeVideoDataDomain> {
        TODO("Not yet implemented")
    }

    // Since `adapter.currentList()` does not immediately reflect the updates after `submitList`
    // we can use this callback instead to listen and get the latest list.
    override fun onCurrentListChanged(
        previousList: List<YouTubeVideoDataDomain>,
        currentList: List<YouTubeVideoDataDomain>
    ) {
        super.onCurrentListChanged(previousList, currentList)
        itemListener.onListUpdate(previousList.size, currentList.size)
    }

    interface ItemListener {

        fun onItemSelected(position: Int)
        fun onListUpdate(previousListSize: Int, currentListSize: Int)

    }

}