/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.presentation.ui.main

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import com.bumptech.glide.RequestManager
import com.devsbitwise.android.common.domain.repositories.UserPreferences
import com.devsbitwise.android.common.extensions.canScrollUp
import com.devsbitwise.android.common.extensions.collectFlow
import com.devsbitwise.android.common.extensions.collectShared
import com.devsbitwise.android.common.extensions.formatWithUnit
import com.devsbitwise.android.common.extensions.showToast
import com.devsbitwise.android.common.extensions.smoothScrollWithinCoordinator
import com.devsbitwise.android.common.presentation.ui.custom_views.GridRecyclerView
import com.devsbitwise.android.common.presentation.ui.fragment.BaseFragment
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_STRING
import com.devsbitwise.android.material.extensions.materialDatePicker
import com.devsbitwise.android.material.extensions.safeShow
import com.devsbitwise.android.material.extensions.snackBar
import com.devsbitwise.cryptonian.R
import com.devsbitwise.cryptonian.databinding.FragmentMarketcapBinding
import com.devsbitwise.cryptonian.presentation.adapters.crypto.AssetMinAdapter
import com.devsbitwise.cryptonian.presentation.states.activity.MainActivityState
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.cryptonian.presentation.utils.AppUtils
import com.devsbitwise.cryptonian.presentation.viewmodels.shared.MainSharedViewModel
import com.devsbitwise.messari.domain.models.MessariAssetMinDataDomain
import com.devsbitwise.messari.domain.models.MessariMarketCapDataDomain
import com.devsbitwise.messari.presentation.events.marketcap.MessariMarketCapEvent
import com.devsbitwise.messari.presentation.states.marketcap.MessariAssetMinState
import com.devsbitwise.messari.presentation.states.marketcap.MessariMarketCapState
import com.devsbitwise.messari.presentation.viewmodels.MessariMarketCapViewModel
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.android.material.card.MaterialCardView
import com.google.android.material.chip.Chip
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.format
import kotlinx.datetime.toLocalDateTime
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named


class MarketCapFragment : BaseFragment<FragmentMarketcapBinding, MessariMarketCapViewModel>() {

    private var isReadyForSearch = false

    private var filterConstraint: String = DEFAULT_VALUE_STRING

    private lateinit var recyclerView: GridRecyclerView
    private lateinit var logTxtV: MaterialTextView
    private lateinit var progressBar: ProgressBar
    private lateinit var fromDateChip: Chip
    private lateinit var toDateChip: Chip
    private lateinit var chart: LineChart

    private lateinit var datePickerFrom: MaterialDatePicker<Long>
    private lateinit var datePickerTo: MaterialDatePicker<Long>

    private val userPreferredAssetLimit
        get() = resources.getStringArray(R.array.asset_limit)[userPreferences.prefInt(getString(R.string.asset_limit_key))]

    private lateinit var assetMinAdapter: AssetMinAdapter

    private lateinit var listener: MarketCapListener

    private val mainSharedViewModel by activityViewModel<MainSharedViewModel>()

    private val userPreferences by inject<UserPreferences>()

    private val glide by inject<RequestManager>()

    private val dateFormatter = LocalDateTime.Format { date(LocalDate.Formats.ISO) }

    private val fromDateConstraints by inject<Pair<CalendarConstraints, Long>>(qualifier = named("FromMaterialDatePickerConstraints"))

    private val toDateConstraints by inject<Pair<CalendarConstraints, Long>>(qualifier = named("ToMaterialDatePickerConstraints"))

    override var interceptBackPress: Boolean = true

    override val viewModel by viewModel<MessariMarketCapViewModel>()

    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentMarketcapBinding =
        FragmentMarketcapBinding::inflate

    override fun initViews(binding: FragmentMarketcapBinding) {

        binding.apply {

            with(requireContext()) {

                chart = lineChart
                logTxtV = errorLog
                recyclerView = gridList
                progressBar = progressChart

                fromDateChip = fromDate
                toDateChip = toDate

                assetMinAdapter = AssetMinAdapter(
                    Firebase.remoteConfig.getString(AppConfig.CRYPTO_IMAGE_BASE_URL),
                    glide,
                    object : AssetMinAdapter.ItemListener {

                    override fun onItemSelected(position: Int, cardView: MaterialCardView) {

                        val domain = assetMinAdapter.currentList[position]

                        if (domain.name.equals(MessariAssetMinDataDomain.DEFAULT_ASSET, true)) {
                            return
                        }

                        val selectedSize = assetMinAdapter.currentList.filter { it.isSelected }.size

                        // To avoid abuse usage, limit the selectable asset market cap
                        // the default asset BTC is counted
                        if (selectedSize >= 6 && domain.isSelected.not()) {
                            showToast(getString(R.string.max_size_reach), Toast.LENGTH_LONG)
                            return
                        }

                        domain.isSelected = !cardView.isChecked
                        cardView.isChecked = domain.isSelected

                        fetchAssetsMarketCap(assetMinAdapter.currentList)

                    }

                    override fun onListUpdate(
                        previousList: List<MessariAssetMinDataDomain>,
                        currentList: List<MessariAssetMinDataDomain>
                    ) {
                        recyclerView.startLayoutAnimation()
                    }

                }).apply {
                    setHasStableIds(true)
                }

                recyclerView.layoutAnimation = AnimationUtils.loadLayoutAnimation(this, R.anim.grid_layout)
                recyclerView.setHasFixedSize(true)
                recyclerView.adapter = assetMinAdapter

                // Apply styling to LineChart
                AppUtils.styleLineChart(chart)

                datePickerFrom = materialDatePicker(calendarConstraints = fromDateConstraints.first, fromDateConstraints.second).apply {

                    fromDateChip.text = selection?.let { Instant.fromEpochMilliseconds(it).toLocalDateTime(TimeZone.currentSystemDefault()).format(dateFormatter) }

                    addOnPositiveButtonClickListener {

                        fromDateChip.text = Instant.fromEpochMilliseconds(it).toLocalDateTime(TimeZone.currentSystemDefault()).format(dateFormatter)

                        fetchAssetsMarketCap(assetMinAdapter.currentList)

                    }

                }

                datePickerTo = materialDatePicker(calendarConstraints = toDateConstraints.first, toDateConstraints.second).apply {

                    toDateChip.text = selection?.let { Instant.fromEpochMilliseconds(it).toLocalDateTime(TimeZone.currentSystemDefault()).format(dateFormatter) }

                    addOnPositiveButtonClickListener {

                        toDateChip.text = Instant.fromEpochMilliseconds(it).toLocalDateTime(TimeZone.currentSystemDefault()).format(dateFormatter)

                        fetchAssetsMarketCap(assetMinAdapter.currentList)

                    }

                }

                fromDateChip.setOnClickListener {
                    datePickerFrom.safeShow(parentFragmentManager, null)
                }

                toDateChip.setOnClickListener {
                    datePickerTo.safeShow(parentFragmentManager, null)
                }

            }

        }

        activity?.addMenuProvider(object : MenuProvider {

            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                val searchIcon = menu.findItem(R.id.action_search)
                searchIcon.isVisible = true
                searchIcon.isEnabled = true
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return false // Let the host Activity or Jetpack component handles the event
            }

        }, viewLifecycleOwner, Lifecycle.State.RESUMED)

    }

    override fun subscribeUI(savedInstanceState: Bundle?) {

        with(mainSharedViewModel) {
            viewLifecycleOwner.collectShared(mainActivityState, ::onMainActivityStateChanged)
        }

        with(viewModel) {

            viewLifecycleOwner.collectFlow(messariAssetMinState, ::onMarketCapAssetStateChanged)

            viewLifecycleOwner.collectFlow(messariMarketCapState, ::onMarketCapStateChanged)

            // Recover the the UI state during configuration change.
            // We cannot use the onSavedInstanceState to save and recover the UI loading state because if system-initiated process death occur
            // the UI will be in loading state forever. We only want UI states to survive a configuration change using ViewModel.
            if (isFetchOngoing) {
                chart.visibility = View.INVISIBLE
                logTxtV.text = null
                logTxtV.visibility = View.INVISIBLE
                progressBar.visibility = View.VISIBLE
            }

            // If the user preferred asset limit is not equal to the current list size
            // it means the action to update the limit count was performed when this screen is not visible to user
            // hence not captured, this condition ensures to fetch with latest user preferred asset limit count
            if (assetList.isEmpty() || assetList.size != userPreferredAssetLimit.toInt()) {

                // Prevent repetitive initial fetch which will cause duplicate items.
                // This could happen when the response is slow and user keep switching between this and other fragments.
                if (isFetchOngoing) {
                    return
                }

                // Initial or update fetch
                fetchAssets()

            }
            else {
                // Recover data after configuration change
                assetMinAdapter.submitList(assetList)
                isReadyForSearch = true
                fetchAssetsMarketCap(assetList)
            }

        }

        logTxtV.setOnClickListener {
            if (assetMinAdapter.currentList.isEmpty()) {
                // Retry if asset fetch failed
                fetchAssets()
            } else {
                // Retry if market cap fetch failed
                fetchAssetsMarketCap(assetMinAdapter.currentList)
            }
        }

    }

    private fun onMarketCapAssetStateChanged(state: MessariAssetMinState) {

        when (state) {

            is MessariAssetMinState.FetchLoading -> {

                chart.visibility = View.INVISIBLE
                logTxtV.text = null
                logTxtV.visibility = View.INVISIBLE
                progressBar.visibility = View.VISIBLE

                // To avoid the visual look of flickering on grid list
                // when success state (remote) or failed state (local) returns immediately after this state,
                // we only supply cache data during failed state

            }

            is MessariAssetMinState.FetchSuccess -> {

                progressBar.visibility = View.GONE

                // Since this is a free limited API, data can be empty
                // DiffUtil will assume items are removed if empty data was submitted
                // This can also happen when reaching the end of page during pagination
                // and yet the API still decided to return an HTTP response code 2xx
                if (state.assetList.isEmpty()) {
                    // In case the fetch was successful but with empty response
                    // and the adapter was previously empty as well
                    if (assetMinAdapter.currentList.isEmpty()) {
                        logTxtV.setText(R.string.no_data)
                        logTxtV.visibility = View.VISIBLE
                    }
                    return
                }

                isReadyForSearch = true

                // Prevent updating the UI and data while the user is searching to avoid display conflict,
                // this can happen when a late response arrived but the user is already searching
                filterConstraint.ifBlank {
                    assetMinAdapter.submitList(state.assetList)
                    fetchAssetsMarketCap(state.assetList)
                    return
                }

                fetchAssetsMarketCap(assetMinAdapter.currentList) // Use old list while user is in search mode

            }

            is MessariAssetMinState.FetchFailed -> {

                progressBar.visibility = View.GONE

                if (state.assetList.isNullOrEmpty()) {
                    if (assetMinAdapter.currentList.isEmpty()) {
                        // In case the fetch failed and has no cache
                        logTxtV.setText(R.string.tap_to_retry)
                        logTxtV.visibility = View.VISIBLE
                    }
                    return
                }
                else {
                    activity?.snackBar(getString(R.string.cache_loaded), parent = view)?.show()
                }

                isReadyForSearch = true

                // Prevent updating the UI and data while the user is searching to avoid display conflict,
                // this can happen when a late response arrived but the user is already searching
                filterConstraint.ifBlank {
                    assetMinAdapter.submitList(state.assetList) // Cache
                    fetchAssetsMarketCap(state.assetList ?: emptyList())
                    return
                }

                fetchAssetsMarketCap(assetMinAdapter.currentList) // Use old list while user is in search mode

            }

        }

    }

    private fun onMarketCapStateChanged(state: MessariMarketCapState) {

        with(requireContext()) {

            when (state) {

                is MessariMarketCapState.FetchMarketCapLoading -> {

                    chart.visibility = View.INVISIBLE
                    logTxtV.text = null
                    logTxtV.visibility = View.INVISIBLE
                    progressBar.visibility = View.VISIBLE

                    chart.apply {
                        // Disable showing marker when invalidating the data,
                        // this avoid NPE as the data might no longer be available
                        setDrawMarkers(false)
                        resetTracking()
                        clear()
                    }

                    // To avoid the visual look of flickering on line graph
                    // when success state (remote) or failed state (local) returns immediately after this state,
                    // we only supply cache data during failed state

                }

                is MessariMarketCapState.FetchMarketCapSuccess -> {

                    progressBar.visibility = View.GONE

                    // Since the availability of data is based on a given date range,
                    // it can be empty but such scenario is valid for this case.
                    // If it has no data even with default selected asset (BTC)
                    if (state.marketCapList.all { it.first.isEmpty() }) {
                        logTxtV.setText(R.string.no_data)
                        logTxtV.visibility = View.VISIBLE
                        return
                    }

                    displayGraph(addLineData(state.marketCapList))

                }

                is MessariMarketCapState.FetchMarketCapNoData -> {

                    if (state.isFromLoadState) {
                        return
                    }

                    showToast(
                        getString(R.string.no_market_data, state.assetNameList.joinToString()),
                        Toast.LENGTH_LONG
                    )

                }

                is MessariMarketCapState.FetchMarketCapFailed -> {

                    progressBar.visibility = View.GONE

                    // If it has no data even with default selected asset (BTC)
                    if (state.marketCapList.all { it.first.isEmpty() }) {
                        logTxtV.setText(R.string.tap_to_retry)
                        logTxtV.visibility = View.VISIBLE
                        return
                    }
                    else {
                        activity?.snackBar(getString(R.string.cache_loaded), parent = view)?.show()
                    }

                    displayGraph(addLineData(state.marketCapList)) // Cache

                }

            }

        }

    }

    private fun onMainActivityStateChanged(state: MainActivityState) {

        when (state) {

            is MainActivityState.AssetFilterChanged -> {
                if (isReadyForSearch) {
                    filterConstraint = state.assetNameSymbol
                    assetMinAdapter.filter.filter(filterConstraint)
                }
            }

            is MainActivityState.AssetLimitChanged -> {
                fetchAssets()
            }

        }

    }

    private fun fetchAssets() {

        viewModel.onTriggerEvent(MessariMarketCapEvent.FetchMarketCapAssets(userPreferredAssetLimit))

    }

    private fun fetchAssetsMarketCap(assetList: List<MessariAssetMinDataDomain>) {

        viewModel.onTriggerEvent(
            MessariMarketCapEvent.FetchMarketCapData(
                fromDateChip.text.toString(),
                toDateChip.text.toString(),
                assetList.filter { it.isSelected } // Only the selected asset
            )
        )

    }

    private fun addLineData(marketCapList: List<Pair<List<MessariMarketCapDataDomain>, String>>): List<LineDataSet> {

        val lineDataSetList = mutableListOf<LineDataSet>()

        marketCapList.forEach {

            val entryList = mutableListOf<Entry>()

            it.first.forEach { data ->

                entryList.add(
                    // Sadly we may lose precision here due to usage of float
                    // which is the chosen data type for entries by MPAndroidChart
                    // thus we use the extra Object data parameter to pass the
                    // realized value paired with asset name and cast it later
                    // so we can show correct value in our AppChartMarker
                    Entry(
                        AppUtils.getScaledDownTimeStamp(data.timestamp),
                        data.realized.toFloat(),
                        Pair(it.second, data.realized)
                    )
                )

            }

            if (entryList.isNotEmpty()) {
                lineDataSetList.add(LineDataSet(entryList, it.second))
            }

        }

        return lineDataSetList.toList()

    }

    private fun displayGraph(lineDataSetList: List<LineDataSet>) {

        chart.visibility = View.VISIBLE

        for ((index, lineDataSet) in lineDataSetList.withIndex()) {

            // Styling each set of line
            val colors = ColorTemplate.PASTEL_COLORS + ColorTemplate.MATERIAL_COLORS

            val currentColor = colors[index % colors.size]

            lineDataSet.axisDependency = YAxis.AxisDependency.LEFT
            lineDataSet.color = currentColor
            lineDataSet.highLightColor = Color.rgb(244, 117, 117)
            lineDataSet.lineWidth = 2f
            lineDataSet.circleRadius = 3f
            lineDataSet.fillAlpha = 50
            lineDataSet.setCircleColor(currentColor)
            lineDataSet.setDrawCircleHole(false)
            lineDataSet.setDrawValues(false) // Display values on each point
            lineDataSet.setDrawFilled(true)
            lineDataSet.fillColor = currentColor

            // End of loop
            if (index.plus(1) == lineDataSetList.size) {

                // Always set and plot the axis first before populating the chart data
                // to avoid axis label display issue and data inconsistency that can cause exception
                chart.xAxis.valueFormatter = object : ValueFormatter() {

                    override fun getFormattedValue(value: Float): String {
                        return try {
                            AppUtils.getXAxisFormattedDate(value)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            DEFAULT_VALUE_STRING
                        }
                    }

                }

                // Always set and plot the axis first before populating the chart data
                // to avoid axis label display issue and data inconsistency that can cause exception
                chart.axisRight.valueFormatter = object : ValueFormatter() {

                    override fun getFormattedValue(value: Float): String {
                        return try {
                            getString(R.string.us_dollars, value.formatWithUnit())
                        } catch (e: Exception) {
                            e.printStackTrace()
                            DEFAULT_VALUE_STRING
                        }
                    }

                }

                // Create a data object with the data sets
                val data = LineData(*lineDataSetList.toTypedArray())

                chart.data = data
                chart.notifyDataSetChanged()

                chart.fitScreen()

                chart.animateX(1500)

            }

        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is MarketCapListener) {
            listener = context
        }

    }

    override fun onPause() {
        if (datePickerFrom.isAdded) {
            datePickerFrom.dismiss()
        }
        if (datePickerTo.isAdded) {
            datePickerTo.dismiss()
        }
        super.onPause()
    }

    override fun onBackPressed() {
        if (recyclerView.canScrollUp()) {
            recyclerView.smoothScrollWithinCoordinator(0)
            listener.expandAppBarLayout()
        } else {
            super.onBackPressed()
        }
    }


    fun interface MarketCapListener {

        fun expandAppBarLayout()

    }

}