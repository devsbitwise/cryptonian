/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian

import android.app.Application
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import com.devsbitwise.android.common.di.androidCommonModule
import com.devsbitwise.android.common.domain.repositories.UserPreferences
import com.devsbitwise.android.common.utils.DAY_NIGHT_THEME
import com.devsbitwise.cryptonian.di.appModule
import com.devsbitwise.cryptonian.presentation.utils.AppConfig
import com.devsbitwise.kmm.shared.di.initKoin
import com.google.android.gms.ads.MobileAds
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.appcheck.ktx.appCheck
import com.google.firebase.appcheck.playintegrity.PlayIntegrityAppCheckProviderFactory
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger

class App : Application() {

    private val userPreferences by inject<UserPreferences>()

    override fun onCreate() {
        super.onCreate()

        initKoin {
            androidLogger()
            androidContext(this@App)
            modules(androidCommonModule, appModule)
        }

        val isAppNight = userPreferences.prefInt(DAY_NIGHT_THEME)

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            // Apply appropriate theme before launching any Activity
            AppCompatDelegate.setDefaultNightMode(
                when (isAppNight) {
                    AppCompatDelegate.MODE_NIGHT_NO -> AppCompatDelegate.MODE_NIGHT_NO // User set this explicitly
                    AppCompatDelegate.MODE_NIGHT_YES -> AppCompatDelegate.MODE_NIGHT_YES // User set this explicitly
                    else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                }
            ) // For Android 12 (S) and above use UiModeManager instead of AppCompatDelegate
        }

        // To avoid the inconsistency with Splash Screen API on some older version of Android
        // we will follow the device Dark Theme settings starting Android 10 (Q) and above
        // Reference: https://issuetracker.google.com/issues/240533989

        FirebaseApp.initializeApp(this)
        Firebase.analytics.setAnalyticsCollectionEnabled(BuildConfig.DEBUG.not())
        Firebase.crashlytics.isCrashlyticsCollectionEnabled = BuildConfig.DEBUG.not()
        Firebase.messaging.subscribeToTopic(AppConfig.FCM_TOPIC_ANNOUNCEMENT)

        if (BuildConfig.DEBUG.not()) {
            Firebase.appCheck.installAppCheckProviderFactory(PlayIntegrityAppCheckProviderFactory.getInstance())
        }

        // Initialize AdMob
        MobileAds.initialize(this)

    }

}