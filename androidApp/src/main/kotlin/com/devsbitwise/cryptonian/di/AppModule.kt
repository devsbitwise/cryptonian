/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.cryptonian.di

import com.devsbitwise.cryptonian.presentation.viewmodels.shared.AssetDetailSharedViewModel
import com.devsbitwise.cryptonian.presentation.viewmodels.shared.MainSharedViewModel
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import kotlinx.datetime.Clock
import kotlinx.datetime.DateTimeUnit
import kotlinx.datetime.TimeZone
import kotlinx.datetime.minus
import kotlinx.datetime.toInstant
import kotlinx.datetime.toLocalDateTime
import org.koin.core.module.dsl.viewModelOf
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {

    viewModelOf(::MainSharedViewModel)

    viewModelOf(::AssetDetailSharedViewModel)

    single(qualifier = named("FromMaterialDatePickerConstraints")) {

        val now = Clock.System.now()

        val currentDate = now.toLocalDateTime(TimeZone.currentSystemDefault())

        val currentDateInMilli = currentDate.toInstant(TimeZone.UTC).toEpochMilliseconds()
        val oldestDateInMilli = currentDate.toInstant(TimeZone.UTC).minus(currentDate.year - 2009L, DateTimeUnit.YEAR, TimeZone.UTC).toEpochMilliseconds()
        val previousMonthInMilli = currentDate.toInstant(TimeZone.UTC).minus(1, DateTimeUnit.MONTH, TimeZone.UTC).toEpochMilliseconds()

        val fromConstraints =
            CalendarConstraints.Builder()
                .setStart(oldestDateInMilli)
                .setEnd(currentDateInMilli)
                .setOpenAt(previousMonthInMilli)
                .setValidator(DateValidatorPointBackward.now()) // Disable days beyond current date
                .build()

        Pair(fromConstraints, previousMonthInMilli)

    }

    single(qualifier = named("ToMaterialDatePickerConstraints")) {

        val now = Clock.System.now()

        val currentDate = now.toLocalDateTime(TimeZone.currentSystemDefault())

        val currentDateInMilli = currentDate.toInstant(TimeZone.UTC).toEpochMilliseconds()
        val oldestDateInMilli = currentDate.toInstant(TimeZone.UTC).minus(currentDate.year - 2009L, DateTimeUnit.YEAR, TimeZone.UTC).toEpochMilliseconds()

        val toConstraints =
            CalendarConstraints.Builder()
                .setStart(oldestDateInMilli)
                .setEnd(currentDateInMilli)
                .setOpenAt(currentDateInMilli)
                .setValidator(DateValidatorPointBackward.now()) // Disable days beyond current date
                .build()

        Pair(toConstraints, currentDateInMilli)

    }

}