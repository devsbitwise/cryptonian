<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2024 Devs Bitwise
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools">

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />

    <!-- Access storage backward compatibility for SDK version below Android 10 (Q) -->
    <uses-permission
        android:name="android.permission.WRITE_EXTERNAL_STORAGE"
        android:maxSdkVersion="28"
        tools:ignore="ScopedStorage" />

    <!-- Access storage for Android 12 (S) and below -->
    <uses-permission
        android:name="android.permission.READ_EXTERNAL_STORAGE"
        android:maxSdkVersion="32" />

    <!-- Android 13 (T) -->
    <uses-permission android:name="android.permission.READ_MEDIA_IMAGES" />
    <uses-permission android:name="android.permission.POST_NOTIFICATIONS" />

    <!-- Android 14 (U) -->
    <uses-permission android:name="android.permission.READ_MEDIA_VISUAL_USER_SELECTED" />

    <application
        android:name=".App"
        android:enableOnBackInvokedCallback="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/Theme.Cryptonian"
        android:usesCleartextTraffic="false"
        tools:targetApi="tiramisu">

        <!-- Locked orientation for this activity due to bug on FirebaseUI for Phone Auth -->
        <!-- Reference: https://github.com/firebase/FirebaseUI-Android/issues/2013 -->
        <!-- Reference: https://stackoverflow.com/a/43730237/12204620 -->
        <activity
            android:name=".presentation.LoginActivity"
            android:exported="true"
            android:screenOrientation="portrait"
            android:theme="@style/Theme.Cryptonian.Starting"
            tools:ignore="DiscouragedApi,LockedOrientationActivity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
            <intent-filter>
                <!-- Custom -->
                <action android:name="com.devsbitwise.cryptonian.intent.action.VIEW_MAIN" />

                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>

        <activity
            android:name=".presentation.MainActivity"
            android:launchMode="singleTask"
            android:windowSoftInputMode="adjustPan"
            android:theme="@style/Theme.Cryptonian.NoActionBar" />

        <activity
            android:name=".presentation.AssetDetailActivity"
            android:theme="@style/Theme.Cryptonian.NoActionBar" />

        <activity
            android:name=".presentation.NewsDetailActivity"
            android:theme="@style/Theme.Cryptonian.NoActionBar.TranslucentStatusBar" />

        <activity
            android:name=".presentation.VideoPlayerActivity"
            android:theme="@style/Theme.Cryptonian.NoActionBar"
            android:configChanges="orientation|keyboardHidden|screenSize|screenLayout|smallestScreenSize" />

        <service
            android:name=".data.data_source.components.FCMService"
            android:directBootAware="true"
            android:exported="false">
            <intent-filter>
                <action android:name="com.google.firebase.MESSAGING_EVENT" />
            </intent-filter>
        </service>

        <provider
            android:name="androidx.core.content.FileProvider"
            android:authorities="${applicationId}.provider"
            android:grantUriPermissions="true"
            android:exported="false"> <!-- Inject build variables: https://developer.android.com/build/manage-manifests#inject_build_variables_into_the_manifest -->
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/provider_paths" />
        </provider>

        <meta-data
            android:name="android.webkit.WebView.EnableSafeBrowsing"
            android:value="true" />

        <meta-data
            android:name="com.google.android.gms.ads.APPLICATION_ID"
            android:value="ca-app-pub-5098344296102631~9826952540" />

        <!-- The flag below optimizes the MobileAds.initialize() initialization call -->
        <meta-data
            android:name="com.google.android.gms.ads.flag.OPTIMIZE_INITIALIZATION"
            android:value="true" />

        <!-- The flag below optimizes ad load calls for all ad formats -->
        <meta-data
            android:name="com.google.android.gms.ads.flag.OPTIMIZE_AD_LOADING"
            android:value="true" />

        <!-- TODO AdMob and Google Services bug in Gradle 8.3 https://issuetracker.google.com/issues/327696048, remove this if they fixed it on newer version -->
        <property
            android:name="android.adservices.AD_SERVICES_CONFIG"
            android:resource="@xml/gma_ad_services_config"
            tools:replace="android:resource" />

    </application>

</manifest>