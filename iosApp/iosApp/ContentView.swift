//
//  ContentView.swift
//  iosApp
//
//  Created by Jensen Louise Lim on 7/27/24.
//

import SwiftUI
import shared

struct ContentView: View {
    
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Cryptonian \(Greeting().greet())")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
