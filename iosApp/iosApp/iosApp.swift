//
//  iosAppApp.swift
//  iosApp
//
//  Created by Jensen Louise Lim on 7/27/24.
//

import SwiftUI
import shared

@main
struct iosApp: App {
    
    init() {
        InitKoinKt.doInitKoin(config: {_ in })
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
    
}
