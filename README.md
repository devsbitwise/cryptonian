# Cryptonian 🚀
[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/devsbitwise/cryptonian/master?label=v3.5.0)](https://bitbucket.org/devsbitwise/cryptonian/pipelines/results/page/1)
[![Android API](https://img.shields.io/badge/API-21%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=21)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Cryptonian demonstrates a simple [offline-first](https://developer.android.com/topic/architecture/data-layer/offline-first) modern Android development with Jetpack Libraries (Navigation, Room, Lifecycle-aware Components),
Material Design 3, Koin, Ktor, SplashScreen API, OnBackPressedCallback, and Kotlin Coroutines with Flow, Channel, StateFlow, SharedFlow.

Now migrating to **Kotlin Multiplatform Mobile (KMM/KMP)** with iOS version currently in development.

[![App Feature Graphics](https://bitbucket.org/devsbitwise/cryptonian/raw/v3/app/src/main/play/listings/en-US/graphics/feature-graphic/1.png)](https://play.google.com/store/apps/details?id=com.devsbitwise.cryptonian)


## Motivation
- Show how the old UI framework **(View XML)** works with the modern practices and frameworks.
- Organizing global styles and themes with **Material Design Component 3**.
- How to migrate native Android project to **Kotlin Multiplatform Mobile (KMM/KMP)**.

|                                               Phone                                                |                                                Tablet                                                |
|:--------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------:|
|  ![Phone Portrait](https://bitbucket.org/devsbitwise/cryptonian/raw/v3.5/docs/Phone_Portrait.png)  |  ![Tablet Portrait](https://bitbucket.org/devsbitwise/cryptonian/raw/v3.5/docs/Tablet_Portrait.png)  |
| ![Phone Landscape](https://bitbucket.org/devsbitwise/cryptonian/raw/v3.5/docs/Phone_Landscape.png) | ![Tablet Landscape](https://bitbucket.org/devsbitwise/cryptonian/raw/v3.5/docs/Tablet_Landscape.png) |


## Design Pattern and Architecture
Cryptonian uses **MVI + Clean Architecture** and is **modularized by feature and layer** which follows the [Google's official architecture guidance](https://developer.android.com/topic/architecture).

###### Data Layer
- Contains business logic **implementation details**.
- Network, Storage, Sensors, External Interface, etc.

###### Domain Layer
- Contains business logic **definitions**.
- It reveals what the system is trying to solve known as **"Screaming Architecture"**.
- Does not depend on other layers, can be reuse, and is **platform agnostic**.

###### Presentation Layer
- Visual representation/interpretation of data.
- Enables a program to accept user inputs that will eventually trigger an events.

![Architecture Diagram](https://bitbucket.org/devsbitwise/cryptonian/raw/v3.5/docs/Cryptonian_Architecture.svg)


## Modules
The project uses [Git Submodule](https://git-scm.com/book/en/master/Git-Tools-Submodules) where each module has its own Git repository. The migration to **Kotlin Multiplatform Mobile (KMM/KMP)** also introduced the [umbrella module](https://www.jetbrains.com/help/kotlin-multiplatform-dev/multiplatform-project-configuration.html#several-shared-modules) configuration.

- [**Kommon (v1.5)**](https://bitbucket.org/devsbitwise/kommon/src/main/) - contains multiplatform Kotlin libraries
- [**AndroidCommon (v2.2)**](https://bitbucket.org/devsbitwise/androidcommon/src/master/) - contains common libraries, framework, base class, extension functions, and tests for Android
- [**AndroidMaterial (v1)**](https://bitbucket.org/devsbitwise/androidmaterial/src/master/) - contains Material Design Component 3 implementation for Android
- [**AndroidAdMob (v1)**](https://bitbucket.org/devsbitwise/androidadmob/src/master/) - contains AdMob implementation for Android
- [**AndroidFirebase (v1)**](https://bitbucket.org/devsbitwise/androidfirebase/src/master/) - contains Firebase services implementation for Android
- [**HtmlTextView (v5)**](https://bitbucket.org/devsbitwise/htmltextview/src/master/) - a custom TextView that extends the support for rendering HTML tags
- [**Messari (v2.5)**](https://bitbucket.org/devsbitwise/messari/src/main/) - multiplatform Kotlin module for Messari APIs contains ViewModels, use cases, repositories, mappers, and tests
- [**WordPress (v2.5)**](https://bitbucket.org/devsbitwise/wordpress/src/main/) - multiplatform Kotlin module for WordPress APIs contains ViewModels, use cases, repositories, mappers, and tests
- [**YouTube (v2.5)**](https://bitbucket.org/devsbitwise/youtube/src/main/) - multiplatform Kotlin module for YouTube Data APIs contains ViewModels, use cases, repositories, mappers, and tests
- **shared** - an umbrella framework bundle containing all of the project's KMM/KMP compatible modules, necessary for the iOS build

![Modules Diagram](https://bitbucket.org/devsbitwise/cryptonian/raw/v3.5/docs/Cryptonian_Modules.svg)


## Test and Lint Checks Report
Since the project is modularized, I decided to merge each module's tests result and code coverage report into a single directory and HTML report. Achieving this however is easier said than done.
Thus while waiting for official solution, I used the old [Android Reporting Plugin](https://issuetracker.google.com/issues/222730176) to aggregate the result of **instrumented tests** and [Test Aggregation Results Plugin](https://github.com/gmazzo/gradle-android-test-aggregation-plugin) which is based on [Test Report Aggregation Plugin](https://docs.gradle.org/current/userguide/test_report_aggregation_plugin.html) but works on Android to aggregate the result of **unit tests**.
I used [Test Aggregation Coverage Plugin](https://github.com/gmazzo/gradle-android-test-aggregation-plugin) which is powered by JaCoCo to aggregate the **code coverage** report. 
In future updates I will include the result of automated **UI tests** from [Firebase Test Lab](https://firebase.google.com/docs/test-lab/android/continuous).

[![Build Reports](https://bitbucket.org/devsbitwise/cryptonian/raw/v3.5/docs/build_reports.svg)](https://cryptonian-lint-and-tests-reports.glitch.me)


## CI/CD Workflow
The workflow uses Docker image containing Android SDK and utilized various Gradle plugins, CLI tools, and/or Linux utility commands to execute numerous tests, generate its reports, build, upload artifacts, and deployment from test environment to staging and production.

![CI CD_Workflow](https://bitbucket.org/devsbitwise/cryptonian/raw/v3.5/docs/Cryptonian_Workflow.svg)

Below are the tools and services used to achieved the workflow.

###### Gradle Plugins
- **Android Gradle Plugin (Default)** - use to test, build, and run Android project
- **Android Reporting** - use to aggregate instrumented tests result
- **Test Aggregation Results** by gmazzo - use to aggregate unit tests result of a multi module project
- **Test Aggregation Coverage** by gmazzo - use to aggregate code coverage report produced by JaCoCo on a multi module project
- **Gradle Play Publisher** by Triple-T - use to manage and publish app releases to Play Store

###### Services
- **Bitbucket Pipelines** - an integrated CI/CD service built into Bitbucket Cloud (it runs our builds in Docker containers with support for public and private images)
- **Bitbucket Downloads** - use to store artifacts such as build reports or APK (I choose this since it is already part of the Bitbucket Cloud and available to use with very minimal required setup)
- **Firebase Test Lab** - cloud-based app testing infrastructure for automated UI testing (it enable us to test the app on a wide range of devices and configurations, both physical and virtual)
- **Glitch** - an online IDE for creating web and Node.js app that includes instant hosting, each project runs on Linux (I choose this since it is much easier than setting up Firebase CLI in the pipeline to use Firebase Hosting, it is also a fun exploration that offers more flexibility)

###### CLI
- **gcloud** - command-line tool use to manage Google Cloud project resources and services (it enable us to push APK to Firebase Test Lab)
- **guardsquare** - command-line tool use for uploading build artifacts (Debug APK and Release AAB) to AppSweep for security code analysis
- **unzip** - command-line tool use for extracting files from a ZIP archives (it enable our Glitch project to extract the content of the downloaded ZIP file)
- **zip** - command-line tool use to create ZIP archive of files and directories (it enable us to compress build reports which will be stored in Bitbucket Downloads)
- **cURL** - command-line tool use for data exchange between a device and a server (it enable us to make HTTP request to Glitch API with request body containing Linux command such as `wget` and `unzip`)
- **wget** - command-line tool use to download files (it enable our Glitch project to download the artifacts stored in Bitbucket Downloads of a public repo, other cloud storage solution will also work as long as the download url of the file is accessible for public)

###### Docker Images
- **Docker Android SDK** by mindrunner - a Docker image containing the Android SDK with emulator
- **Atlassian Default (Image 4)** - one of the default Docker image provided by Bitbucket that includes common command line tools for Linux
- **Google Cloud SDK Docker** - a Docker image for the Google Cloud SDK which includes default command line tools such as `gcloud`, `gsutil`, `bq`, etc.


## Data Source and Services

###### Messari APIs
- **Asset** - endpoint to retrieve detailed information about cryptocurrency
- **Market Cap** - endpoint to retrieve information about realized market cap of a cryptocurrency
- **Exchange** - endpoint to retrieve information about exchange market of a cryptocurrency
- **Price** - endpoint to retrieve OHLC information of a cryptocurrency

###### WordPress APIs
- **Posts** - endpoint to retrieve posts

###### YouTube Data APIs
- **Search** - endpoint to retrieve information about a YouTube video, channel, or playlist

###### Others
- **Firebase Auth** - end-to-end identity solution for easy user authentication (it enable us to add user authentication)
- **Firebase Cloud Messaging** - cross-platform messaging solution that reliably send messages at no cost (it enable us to perform push notification)
- **Firebase Cloud Function** - serverless framework solution that automatically run backend code in response to events triggered by background events (it enable us to create cron jobs that triggers daily push notification which runs on a **Node.js** with **Cloud Scheduler**)
- **Firebase In-App Messaging** - user engagement solution (it enable us to engage with active users by sending a targeted contextual messages that encourage them to use key features of the app)
- **Firebase Remote Config** - remote configuration cloud service solution (it enable us to change the behavior and appearance of client app without requiring users to download an app update)
- **Firebase Hosting** - hosting solution for static and dynamic content web app, and microservices (it enable us to create the project's [landing page](https://cryptonian-app.web.app/))


## Tests and Code Coverage
To run and generate aggregated tests result and code coverage report, execute the following Gradle task in your terminal.

- Unit Test
```shell
./gradlew testAggregatedReport
```

- Instrumented Test
```shell
./gradlew connectedAndroidTest mergeAndroidReports
```

- Code Coverage
```shell
./gradlew jacocoAggregatedReport
```


## What's Next?
The **v3** main objective is to adapt the **Kotlin Multiplatform Mobile (KMM/KMP)** setup.

- Development of iOS version, waiting for [bug](https://issuetracker.google.com/issues/338842136) fix of Kotlin where KSP fails if Room entities are in a separate module


## Changelogs

###### v3.5
- Added **iOS** build
- Added **shared** module
- Renamed **app** module to **androidApp**
- Renamed source set layout from **java** to **kotlin**
- Implemented [umbrella module](https://www.jetbrains.com/help/kotlin-multiplatform-dev/multiplatform-project-configuration.html#several-shared-modules) configuration
- Implemented [Type-safe project accessors](https://docs.gradle.org/7.0/release-notes.html) introduced since Gradle 7.0
- Updated `pluginManagement` and `dependencyResolutionManagement` repositories setup in `settings.gradle.kts`
- Removed **Kotlin Parcelize** plugin in app module (now androidApp)
- Using data class to better represent a JSON Firebase Remote Config params
- Replacing some Activity with Fragment and more utilization of Jetpack Navigation features
- Bump **Kommon** version to **v1.5**
- Bump **Messari** version to **v2.5**
- Bump **YouTube** version to **v2.5**
- Bump **WordPress** version to **v2.5**
- Bump **AndroidCommon** version to **v2.2**

###### v3.0
- Added **Kommon** module
- Migrated from **Hilt** to **Koin**
- Bump **Messari** version to **v2.0**
- Bump **YouTube** version to **v2.0**
- Bump **WordPress** version to **v2.0**
- Bump **AndroidCommon** version to **v2.0**

###### v2.8
- Migrated from **Dagger** to **Hilt**
- Bump **Messari** version to **v1.5**
- Bump **YouTube** version to **v1.5**
- Bump **WordPress** version to **v1.5**
- Bump **AndroidCommon** version to **v1.5**

###### v2.5
- Moved submodules from libs to root project directory
- Improved app stability during [System-initiated process death](https://developer.android.com/topic/libraries/architecture/saving-states#options) with **SavedInstanceState**
- Improved app navigation responsiveness for different orientations and screen sizes based on Android [guideline](https://developer.android.com/guide/topics/large-screens/navigation-for-responsive-uis#responsive_ui_navigation) and Material Design [guideline](https://m3.material.io/components/navigation-bar/guidelines#bfa65899-098c-43d5-9639-1e7f2461ca06)
- Modularized by features (**Messari**, **WordPress**, **YouTube**) and layer (presentation, domain, data) which also display the effectiveness of Clean Architecture
- Secure CI/CD workflow for public use

###### v2.0+
- MVI + Clean Architecture
- Git Submodule
- Dagger 2
- Unit Tests
- Instrumented Tests with Room DAOs
- **Coroutine** replacing **RxJava**
- **Room** replacing **Retrofit caching**
- **Kotlin Flow**, **Kotlin Channel**, **StateFlow**, **SharedFlow**, replacing **LiveData**
- **KSP** replacing **KAPT**
- **Kotlin DSL** replacing **Groovy**
- **Material Design 3** replacing **Material Design 2**
- Version Catalog
- Play In-App Update
- Themed icon support
- Responsive UI for both phone and tablet devices
- Advance CI/CD workflow

###### v1.0+
- Basic MVVM
- Retrofit 2
- RxJava
- LiveData
- AdMob
- View Binding
- Material Design 2
- List Adapter and DiffUtil
- Basic CI/CD workflow


# License
```markdown
MIT License

Copyright (c) 2021 Devs Bitwise

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```